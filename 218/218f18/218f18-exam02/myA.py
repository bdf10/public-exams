from sage.all import diagonal_matrix, identity_matrix, QQ, random_matrix, ZZ


def is_good(A):
    n = A.nrows()
    I = identity_matrix(n)
    for l in A.eigenvalues():
        E = A-l*I
        K = E.change_ring(QQ).right_kernel(basis='pivot')
        try:
            K.basis_matrix().change_ring(ZZ)
        except TypeError:
            return False
    D, J = A.jordan_form(transformation=True)
    return 0 in (J.T*J).list()


def myA(eigenvalues=(-1, 1, 2)):
    n = len(eigenvalues)
    P = random_matrix(ZZ, n, algorithm='unimodular')
    while P.list().count(0) > 1 or 0 not in (P.T*P).list():
        P = random_matrix(ZZ, n, algorithm='unimodular')
    D = diagonal_matrix(eigenvalues)
    A = P*D*P.inverse()
    if not is_good(A):
        print(A)
        return myA(eigenvalues=eigenvalues)
    return A
