from sage.all import matrix, QQ, random_matrix, ZZ


def myQR(m, n):
    if n > m:
        raise TypeError('No {} x {} matrix has full column rank'.format(m, n))
    A = random_matrix(ZZ, m, n)
    while A.rank() != n:
        A = random_matrix(ZZ, m, n)
    Q, _ = A.T.gram_schmidt()
    try:
        Q.change_ring(ZZ)
    except TypeError:
        return myQR(m, n)
    Q = matrix.column(map(lambda v: v.normalized(), Q.rows()))
    R = Q.T*A
    try:
        Q.change_ring(QQ)
        return A, Q, R
    except TypeError:
        return myQR(m, n)
    return A, Q, R
