from functools import partial
from sage.all import elementary_matrix, flatten, identity_matrix, random_matrix, ZZ, prod


class Elem(object):

    def __init__(self, E):
        self.E = E

    def _latex_(self):
        if any(map(lambda d: d != 1, self.E.diagonal())):
            try:
                i, j = [_ for _, d in enumerate(self.E.diagonal()) if d == 0]
                return 'R_{}\\leftrightarrow R_{}'.format(i+1, j+1)
            except ValueError:
                for i, m in enumerate(self.E.diagonal()):
                    if m != 1:
                        if abs(m) < 1:
                            return '({})\\cdot R_{{{}}}\\to R_{{{}}}'.format(m, i+1, i+1)
                        return '{}\\cdot R_{{{}}}\\to R_{{{}}}'.format(m, i+1, i+1)
        st = '\\arraycolsep=1pt\n'
        st += '\\begin{array}{rcrcr}\n'
        for j, col in enumerate(self.E.columns()):
            if col.list().count(0) < self.E.nrows() - 1:
                for i, m in enumerate(col):
                    if i == j:
                        continue
                    if m < 0:
                        if m == -1:
                            st += 'R_{{{}}}-R_{{{}}} &\\to& R_{{{}}} \\\\\n'.format(
                                i+1, j+1, i+1)
                        else:
                            st += 'R_{{{}}}-{}\\cdot R_{{{}}} &\\to& R_{{{}}} \\\\\n'.format(
                                i+1, abs(m), j+1, i+1)
                    if m > 0:
                        if m == 1:
                            st += 'R_{{{}}}+R_{{{}}} &\\to& R_{{{}}} \\\\\n'.format(
                                i+1, j+1, i+1)
                        else:
                            st += 'R_{{{}}}+{}\\cdot R_{{{}}} &\\to& R_{{{}}} \\\\\n'.format(
                                i+1, m, j+1, i+1)
        st += '\\end{array}'
        return st


class GJ(object):

    def __init__(self, A):
        self.A = A

    def gj_steps(self):
        return list(self.gj_steps_it())

    def n_steps(self):
        return len(self.gj_steps())

    def elementary_matrices_it(self):
        return (t[0] for t in self.gj_steps_it())

    def elementary_matrices(self):
        return list(self.elementary_matrices_it())

    def elem_latex(self):
        return map(Elem, self.elementary_matrices_it())

    def stages_it(self):
        return (t[1] for t in self.gj_steps_it())

    def stages(self):
        return list(self.stages_it())

    def rref_inverse(self):
        return prod(reversed(self.elementary_matrices()))

    def needs_QQ(self):
        for Ai in self.stages_it():
            for a in Ai.list():
                if a not in ZZ:
                    return True
        return False

    def _latex_(self):
        tex = self.A._latex_()
        tex += '\n'
        for si, Ai in zip(self.elem_latex(), self.stages()):
            tex += r'&\xrightarrow{' + si._latex_() + '}' + Ai._latex_()
            tex += r'\\' + '\n'
        return tex[:-3]

    def __repr__(self):
        m, n = self.A.dimensions()
        n_steps = self.n_steps()
        S = 'ZZ'
        if self.needs_QQ():
            S = 'QQ'
        return 'Gauss-Jordan steps for a {}x{} matrix in {} steps over {}.'.format(m, n, n_steps, S)

    def gj_steps_it(self):
        A = self.A
        m, n = A.dimensions()
        i = j = 0
        elem = partial(elementary_matrix, m)
        while i <= m-1 and j <= n-1:
            aij = A[i, j]
            if aij == 0:
                col = A.columns()[j]
                def f((k, x)): return False if x == 0 or k <= i else True
                swaps = filter(f, enumerate(col))
                if not swaps:
                    j += 1
                    continue
                k, _ = swaps[0]
                E = elem(row1=i, row2=k)
                A = E*A
                yield E, A
                continue
            if aij == 1:
                col = A.columns()[j]
                def f((k, x)): return False if x == 0 or k == i else True
                mults = filter(f, enumerate(col))
                if not mults:
                    i += 1
                    j += 1
                    continue
                E = identity_matrix(m)
                for k, mult in mults:
                    E *= elem(row1=k, row2=i, scale=-mult)
                A = E*A
                yield E, A
                i += 1
                j += 1
                continue
            E = elem(row1=i, scale=1/aij)
            A = E*A
            yield E, A


def random_echelonizable_ZZ(m, n, r, nsteps=None, upper_bound=20):
    A = random_matrix(ZZ, m, n, algorithm='echelonizable',
                      rank=r, upper_bound=upper_bound)
    g = GJ(A)
    if nsteps is None:
        while g.needs_QQ():
                A = random_matrix(ZZ, m, n, algorithm='echelonizable', rank=r,
                                  upper_bound=upper_bound)
                g = GJ(A)
                print g
        return A
    while g.n_steps() != nsteps or g.needs_QQ():
        A = random_matrix(ZZ, m, n, algorithm='echelonizable',
                          rank=r, upper_bound=upper_bound)
        g = GJ(A)
    return A
