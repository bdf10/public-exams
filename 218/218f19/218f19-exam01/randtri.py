from sage.all import random_matrix, zero_matrix, ZZ


def extract_lower(A):
    L = zero_matrix(A.nrows(), A.ncols())
    for i, row in enumerate(A.rows()):
        for j, aij in enumerate(row):
            if i > j:
                L[i, j] = aij
    return L


def extract_upper(A):
    return extract_lower(A.T).T


def random_upper_triangular_matrix(m, n, r=None):
    if not r is None:
        A = random_matrix(ZZ, r, n, algorithm='echelonizable', rank=r)
        A = A.stack(zero_matrix(m-r, n))
    else:
        A = random_matrix(ZZ, m, n)
    return A-extract_lower(A)

def random_lower_triangular_matrix(m, n, r=None):
    return random_upper_triangular_matrix(n, m, r).T
