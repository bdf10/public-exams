\documentclass[12pt]{exam}

\usepackage{fitzmath}
\usepackage{fitzexam}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2019}{12}{4}{-1}}

\begin{document}

\begin{questions}

  \newcommand{\myEvals}{
    \begin{array}{rcc}
      \toprule
      \multicolumn{1}{c}{\lambda} & \multicolumn{1}{c}{\gm_A(\lambda)} & \multicolumn{1}{c}{\am_A(\lambda)} \\
      \midrule
      -3      & 1              & 2                                                                          \\
      1       & 2              & 5                                                                          \\
      2       & 1              & 2                                                                          \\
      \bottomrule
    \end{array}
  }
  \newcommand{\myEQ}{
    \begin{aligned}
      n &= \fillin[$2+5+2=9$]                          & \chi_A(t) &= \fillin[$(t+3)^2\cdot(t-1)^5\cdot(t-2)^2$][2in] \\ \\
      \det(A) &=\fillin[$(-3)^2\cdot(1)^5\cdot2^2=36$] & \trace(A) &= \fillin[$2\cdot(-3)+5\cdot1+2\cdot 2=3$][2in]
    \end{aligned}
  }
\question Suppose that an $n\times n$ matrix $A$ has ``eigendata'' organized
  in the following table.
  \begin{align*}
    \myEvals && \myEQ
  \end{align*}
  \begin{parts}
  \part[4] Fill-in each of the blanks above.
  \part[2] In $\chi_A(t)$, the constant coefficient is $c_0=\fillin[$(-1)^9\cdot\det(A)=-36$][2in]$.\vspace{.5cm}
  \part[2] In $\chi_A(t)$, the coefficient of $t^{n-1}$ is $c_{n-1}=\fillin[$-\trace(A)=-3$][2in]$.
  \part[4] Which of the following adjectives apply to $A$?

    \begin{oneparcheckboxes}
      \correctchoice nonsingular
      \choice Hermitian
      \choice indefinite
      \choice diagonalizable
    \end{oneparcheckboxes}

  \part[6] Which of the following matrices is \emph{not} similar to $A$? Select all that apply.

    \begin{oneparcheckboxes}
      \choice $J_2(-3)\oplus J_2(1)\oplus J_3(1)\oplus J_2(2)$
      \correctchoice $J_1(-3)\oplus J_1(-3)\oplus J_1(1)\oplus J_1(1)\oplus J_3(1)\oplus J_2(2)$
      \correctchoice $J_1(-3)\oplus J_1(-3)\oplus J_5(1)\oplus J_2(2)$
      \choice $J_2(-3)\oplus J_1(1)\oplus J_4(1)\oplus J_2(2)$
    \end{oneparcheckboxes}
  \end{parts}

  \begin{sagesilent}
    S = matrix(ZZ, [(2, 3, -11), (3, 10, -3), (-11, -3, 2)])
    D = matrix(QQ, [(16, 0, 0), (0, 7, 0), (0, 0, -9)])
  \end{sagesilent}
  \newcommand{\myQ}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{3}}  & \frac{1}{\sqrt{6}}  & \frac{1}{\sqrt{2}} \\
        \frac{1}{\sqrt{3}}  & \frac{-2}{\sqrt{6}} & 0                  \\
        \frac{-1}{\sqrt{3}} & \frac{-1}{\sqrt{6}} & \frac{1}{\sqrt{2}}
      \end{array}
    \right]
  }
  \newcommand{\myQT}{
    \left[
      \begin{array}{rrr}
        \frac{1}{\sqrt{3}} & \frac{1}{\sqrt{3}}  & \frac{-1}{\sqrt{3}} \\
        \frac{1}{\sqrt{6}} & \frac{-2}{\sqrt{6}} & \frac{-1}{\sqrt{6}} \\
        \frac{1}{\sqrt{2}} & 0                   & \frac{1}{\sqrt{2}}
      \end{array}
    \right]
  }

\question Consider the spectral factorization
  \[
    \overset{S}{\sage{S}}
    =
    \overset{Q}{\myQ}
    \overset{D}{\sage{D}}
    \overset{Q^\ast}{\myQT}
  \]
  Let $q(x_1, x_2, x_3)$ be the quadratic form corresponding to $S$.\vspace{.5cm}
  \begin{parts}
  \part[3] $q(x_1, x_2, x_3)=
    \fillin[$\sage{S[0, 0]}$][.5in]\cdot x_1^2+
    \fillin[$\sage{S[1, 1]}$][.5in]\cdot x_2^2+
    \fillin[$\sage{S[2, 2]}$][.5in]\cdot x_3^2+
    \fillin[$\sage{2*S[0, 1]}$][.5in]\cdot x_1x_2+
    \fillin[$\sage{2*S[0, 2]}$][.5in]\cdot x_1x_3+
    \fillin[$\sage{2*S[1, 2]}$][.5in]\cdot x_2x_3
    $
  \part[2] What is the definiteness of $S$? Select all that apply.

    \begin{oneparcheckboxes}
      \choice pos definite
      \choice pos semidefinite
      \choice neg definite
      \choice neg semidefinite
      \correctchoice indefinite
    \end{oneparcheckboxes}

  \part[6] To ``complete the square'' we introduce variables
    $\Set{y_1, y_2, y_3}$. Fill-in the blanks below to express
    $\Set{y_1, y_2, y_3}$ in terms of the variables $\Set{x_1, x_2, x_3}$.\vspace{.5cm}
    \begin{align*}
      y_1 &= \fillin[$\frac{1}{\sqrt{3}}\cdot(x_1+x_2-x_3)$][1.5in] & y_2 &= \fillin[$\frac{1}{\sqrt{6}}\cdot(x_1-2\,x_2-x_3)$][1.5in] & y_3 &= \fillin[$\frac{1}{\sqrt{2}}\cdot(x_1+x_3)$][1.5in]
    \end{align*}
    Fill-in the blanks below to express $q(x_1, x_2, x_3)$ as a function of
    $\Set{y_1, y_2, y_3}$.\vspace{.5cm}
    \[
      q(y_1, y_2, y_3)=
      \fillin[$16$][.5in]\cdot y_1^2+
      \fillin[$7$][.5in]\cdot y_2^2+
      \fillin[$-9$][.5in]\cdot y_3^2+
      \fillin[$0$][.5in]\cdot y_1y_2+
      \fillin[$0$][.5in]\cdot y_1y_3+
      \fillin[$0$][.5in]\cdot y_2y_3
    \]

  \part[5] If possible, find a matrix $A$ such that $S=A^\ast A$. If this is
    not possible, then explain why.
    \begin{solution}[\stretch{1}]
      This is \emph{not possible} since only positive semidefinite matrices
      decompose as $S=A^\ast A$.
    \end{solution}
  \end{parts}


  \skippage
  \begin{sagesilent}
    detP = 2
    P = matrix.column([(-1, 1, -2), vector([1, 0, 2]), detP*vector([0, 2, -1])])
    D = diagonal_matrix([3, 2, -1])
    A = P*D*P.inverse()
    var('x', latex_name=r'\ast')
    SA = A.change_ring(SR)
    SA[1, 1] = x
    SP = P.change_ring(SR)
    SP[0, 2] = SP[1, 2] = SP[2, 2] = x
    SPi = x*ones_matrix(A.nrows())
  \end{sagesilent}
\question Consider the factorization
  \[
    \overset{A}{\sage{SA}}
    =
    \overset{P}{\sage{SP}}
    \overset{D}{\sage{D}}
    \overset{P^{-1}}{\sage{SPi}}
  \]
  where $\det(P)=\sage{detP}$ and the entries marked $\ast$ are unknown.
  \begin{parts}
  \part[4] Explain why the unknown entry in $A$ must be $\ast=\sage{A[1,
      1]}$. You should use this fact throughout the rest of this problem.
    \begin{solution}[\stretch{1}]
      The matrices $A$ and $D$ are \emph{similar}, so they have the same
      trace. The two traces are
      \begin{align*}
        \trace(A) &= \sage{SA[0, 0]}+\sage{SA[1, 1]}+\sage{SA[2, 2]} = \sage{SA.trace()} & \trace(D) &= \sage{D[0, 0]}+\sage{D[1, 1]}+\sage{D[2, 2]} = \sage{D.trace()}
      \end{align*}
      Equating these two traces gives $\sage{SA.trace()}=\sage{D.trace()}$, so
      $\ast=\sage{A[1, 1]}$.
    \end{solution}
  \part[6] Find the $(3, 1)$ entry of $P^{-1}$ without computing any missing
    entries in $P$.
    \begin{solution}[\stretch{1}]
      The adjugate formula for inverses states that
      $P^{-1}=\frac{\adj(P)}{\det(P)}$. The $(3, 1)$ entry of $P^{-1}$ is
      therefore
      \newcommand{\myDet}{
        \left|
          \begin{array}{rrr}
            &   &  \\
            1 & 0 &  \\
            -2 & 2 &
          \end{array}
        \right|
      }
      \[
        \frac{C_{13}}{\det(P)}
        = \frac{1}{2}\cdot(-1)^{1+3}\myDet
        = \frac{2}{2}
        = 1
      \]
    \end{solution}
  \part[10] Find the unknown entries in $P$.
    \begin{solution}[\stretch{4}]
      The given factorization is a diagonalization of $A$. The unknown third
      column is therefore an eigenvector of $A$ corresponding to the eigenvalue
      $\lambda=\sage{D[2, 2]}$. The relevant eigenspace here is
      \begin{sagesilent}
        N = A-D[2, 2]*identity_matrix(A.nrows())
        v, = N.change_ring(ZZ).right_kernel().basis()
        var('c')
      \end{sagesilent}
      \[
        \mathcal{E}_A(\sage{D[2, 2]})
        = \Null\overset{A-(\sage{D[2, 2]})\cdot I_{\sage{A.nrows()}}}{\sage{N}}
        = \Span\Set{\sage{v}}
      \]
      The third column of $P$ is then of the form $\sage{c*v}$. To get the
      correct multiple, we enforce $\det(P)=\sage{detP}$ with
      \begin{sagesilent}
        p1, p2, p3 = P.columns()
        latex.matrix_delimiters(left='|', right='|')
        SP = matrix.column([p1, p2, c*v])
        from functools import partial
        elem = partial(elementary_matrix, SP.nrows())
        E1 = elem(row1=1, row2=0, scale=1)
        E1 = E1*elem(row1=2, row2=0, scale=-2)
      \end{sagesilent}
      \[
        2
        = \sage{SP}
        = \sage{E1*SP}
        = \sage{SP.det()}
      \]
      This tells us that $P$ is
      \begin{sagesilent}
        latex.matrix_delimiters(left='[', right=']')
      \end{sagesilent}
      \[
        P = \sage{P}
      \]
    \end{solution}
  \end{parts}


\question[8] Let $A$ be an $m\times n$ matrix and let $\lambda\neq0$ be an
  eigenvalue of $A^\ast A$. Define $\vv{w}=A\vv{v}$ where
  $\vv{v}\in\mathcal{E}_{A^\ast A}(\lambda)$. Show that $\vv{w}$ is an
  eigenvector of $AA^\ast$.
  \begin{solution}[\stretch{1}]
    $AA^\ast\vv{w}=AA^\ast A\vv{v}=A\lambda\vv{v}=\lambda A\vv{v}=\lambda\cdot\vv{w}$
  \end{solution}

  \skippage
  \begin{sagesilent}
    P = matrix(ZZ, [[3, 11], [1, 4]])
    D = matrix([(-2, 0), (0, 19)])
    A = P*D*P.inverse()
    u0 = vector([1, 0])
  \end{sagesilent}
\question Consider the factorization
  \[
    \overset{A}{\sage{A}}
    =
    \sage{P}\sage{D}{\sage{P}}^{-1}
  \]
  \begin{parts}
  \part[2] $\det(A)=\fillin[$\sage{A.det()}$]$
  \part[6] Find the solution $\vv{u}(t)$ to the initial value problem
    $\frac{d\vv{u}}{dt}=A\vv{u}$ with $\vv{u}(0)=\sage{u0}$.
    \begin{solution}[\stretch{1}]
      From class, we know that the solution is
      $\vv{u}(t)=\exp(At)\vv{u}(0)$. The factorization $A=PDP^{-1}$ then gives
      \newcommand{\myExpDt}{
        \left[
          \begin{array}{rr}
            e^{-2\,t} & 0         \\
            0         & e^{19\,t}
          \end{array}
        \right]
      }
      \newcommand{\myPExpDt}{
        \left[
          \begin{array}{rr}
            3\,e^{-2\,t} & 11\,e^{19\,t} \\
            e^{-2\,t}   &  4\,e^{19\,t}
          \end{array}
        \right]
      }
      \newcommand{\mySolution}{
        \left[
          \begin{array}{r}
            12\,e^{-2\,t} - 11\,e^{19\,t} \\
            4 \,e^{-2\,t} -  4\,e^{19\,t}
          \end{array}
        \right]
      }
      \begin{gather*}
        \vv{u}(t)
        =
        \overset{P}{\sage{P}}
        \overset{\exp(Dt)}{\myExpDt}
        \overset{P^{-1}}{\sage{P.inverse()}}
        \overset{\vv{u}(0)}{\sage{matrix.column(u0)}}
        =
        \myPExpDt
        \sage{P.inverse()*matrix.column(u0)}
        =
        \mySolution
      \end{gather*}
    \end{solution}
  \part[6] Let $V$ be the vector space consisting all vectors $\vv{v}$ such that
    the solution $\vv{u}(t)$ to $\frac{d\vv{u}}{dt}=A\vv{u}$ with
    $\vv{u}(0)=\vv{v}$ satisfies
    $\displaystyle\lim_{t\to\infty}\vv{u}(t)=\vv{O}$. Find a basis of $V$.
    \begin{sagesilent}
      var('x1 x2')
      v = vector([x1, x2])
      vc = matrix.column(v)
      piv1, piv2 = P.inverse()*v
      N = matrix([-1, 3])
      n, = N.right_kernel().basis()
    \end{sagesilent}
    \begin{solution}[\stretch{1}]
      Consider the equation
            \newcommand{\myExpDt}{
        \left[
          \begin{array}{rr}
            e^{-2\,t} & 0         \\
            0         & e^{19\,t}
          \end{array}
        \right]
      }
      \newcommand{\myPExpDt}{
        \left[
          \begin{array}{rr}
            3\,e^{-2\,t} & 11\,e^{19\,t} \\
            e^{-2\,t}   &  4\,e^{19\,t}
          \end{array}
        \right]
      }
      \newcommand{\mySolution}{
        \left[
          \begin{array}{rr}
            (\sage{piv1})\cdot e^{-2\,t} &  \\
                                         & (\sage{piv2})\cdot e^{19\,t}
          \end{array}
        \right]
      }
      \begin{align*}
        \vv{u}(t)
        &=
        \overset{P}{\sage{P}}
        \overset{\exp(Dt)}{\myExpDt}
        \overset{P^{-1}}{\sage{P.inverse()}}
        \overset{\vv{v}}{\sage{vc}} \\
        &=
        \overset{P}{\sage{P}}
        \overset{\exp(Dt)}{\myExpDt}
        \sage{P.inverse()*vc} \\
        &=
        \overset{P}{\sage{P}}
        \mySolution
      \end{align*}
      To ensure that $\displaystyle\lim_{t\to\infty}\vv{u}(t)=\vv{O}$, we need
      the coefficient of $e^{19\,t}$ to be zero, which forces
      $\sage{piv2}=0$. This implies that $V=\Null\sage{N}=\Span\Set{\sage{n}}$.
    \end{solution}
  \end{parts}



  \skippage

  \begin{sagesilent}
    S = matrix(ZZ, [[1, 3, -2, 2], [3, 10, -4, 9], [-2, -4, 12, -2], [2, 9, -2, 18]])
    from functools import partial
    elem = partial(elementary_matrix, S.nrows())
    m21 = S[1, 0] / S[0, 0]
    E1 = elem(row1=1, row2=0, scale=-m21)
    m31 = S[2, 0] / S[0, 0]
    E1 = E1*elem(row1=2, row2=0, scale=-m31)
    m41 = S[3, 0] / S[0, 0]
    E1 = E1*elem(row1=3, row2=0, scale=-m41)
    m32 = (E1*S)[2, 1] / (E1*S)[1, 1]
    E2 = elem(row1=2, row2=1, scale=-m32)
    m42 = (E1*S)[3, 1] / (E1*S)[1, 1]
    E2 = E2*elem(row1=3, row2=1, scale=-m42)
    m43 = (E2*E1*S)[3, 2] / (E2*E1*S)[2, 2]
    E3 = elem(row1=3, row2=2, scale=-m43)
  \end{sagesilent}
\question Consider the elementary row operations
  \newcommand{\myStepA}{\arraycolsep=1pt\tiny
    \begin{array}{rcrcr}
      R_2 &-&  \sage{m21} \cdot R_1 &\to& R_2 \\
      R_3 &-& (\sage{m31})\cdot R_1 &\to& R_3 \\
      R_4 &-&  \sage{m41} \cdot R_1 &\to& R_4
    \end{array}
  }
  \newcommand{\myStepB}{\arraycolsep=1pt\tiny
    \begin{array}{rcrcr}
      R_3 &-& \sage{m32}\cdot R_2 &\to& R_3 \\
      R_4 &-& \sage{m42}\cdot R_2 &\to& R_4
    \end{array}
  }
  \newcommand{\myStepC}{\arraycolsep=1pt\tiny
    \begin{array}{rcrcr}
      R_4 &-& (\sage{m43})\cdot R_3 &\to& R_4
    \end{array}
  }
  \begin{gather*}
    \overset{S}{\sage{S}}
    \xrightarrow{\myStepA} \sage{E1*S}
    \xrightarrow{\myStepB} \sage{E2*E1*S}
    \xrightarrow{\myStepC} \sage{E3*E2*E1*S}
  \end{gather*}
  Note that $S$ is $4\times 4$ and Hermitian, so $S$ defines a quadratic form $q(x_1, x_2, x_3, x_4)$.
  \begin{parts}
  \part[2] $\det(S)=\fillin[$\sage{S.det()}$]$
  \part[2] What is the definiteness of $S$? Select all that apply.

    \begin{oneparcheckboxes}
      \correctchoice pos definite
      \correctchoice pos semidefinite
      \choice neg definite
      \choice neg semidefinite
      \choice indefinite
    \end{oneparcheckboxes}
  \part[10] If possible, write $q(x_1, x_2, x_3, x_4)$ as a sum of squares. If
    this is not possible, then explain why.
    \begin{solution}[\stretch{3}]
      This is possible because $S$ is positive definite. In fact, the given
      elementary row operations yield an $S=LU$ factorization
      \begin{sagesilent}
        _, L, U = S.LU(pivot='nonzero')
      \end{sagesilent}
      \[
        \overset{S}{\sage{S}}
        =
        \overset{L}{\sage{L}}
        \overset{U}{\sage{U}}
      \]
      This gives the Cholesky factorization $S=R^\ast R$ where
      \begin{sagesilent}
        sqrtD = diagonal_matrix(map(sqrt, U.diagonal()))
        R = S.cholesky().T
      \end{sagesilent}
      \[
        \overset{R}{\sage{R}}
        =
        \overset{\sqrt{D}}{\sage{sqrtD}}
        \overset{L^\ast}{\sage{L.T}}
      \]
      Now, our quadratic form is
      \begin{sagesilent}
        r1, r2, r3, r4 = R.rows()
        var('x1 x2 x3 x4')
        x = vector([x1, x2, x3, x4])
      \end{sagesilent}
      \[
        q(\vv{x})
        = \norm{R\vv{x}}^2
        = (\sage{r1*x})^2+(\sage{r2*x})^2+(\sage{r3*x})^2+{\sage{r4*x}}^2
      \]
    \end{solution}
  \end{parts}
\question An $n\times n$ matrix $A$ is called \emph{normal} if
  $A^\ast A=AA^\ast$.
  \begin{parts}
  \part[5] Suppose that $A$ is normal. Show that
    $\norm{A\bv{v}}^2=\norm{A^\ast\bv{v}}^2$.
    \begin{solution}[\stretch{1}]
      $\norm{A\bv{v}}^2=\inner{A\bv{v}, A\bv{v}}=\inner{A^\ast A\bv{v}, \bv{v}}=\inner{AA^\ast\bv{v}, \bv{v}}=\inner{A^\ast\bv{v}, A^\ast\bv{v}}=\norm{A^\ast\bv{v}}^2$
    \end{solution}
  \part[5] Suppose that $A$ satisfies $A^\ast=AQ$ where $Q$ is unitary. Show
    that $A$ is normal.
    \begin{solution}[\stretch{1}]
      Since $A^\ast=AQ$, we have $A=(A^\ast)^\ast=(AQ)^\ast=Q^\ast A^\ast$. It follows that
      \[
        A^\ast A
        = (AQ)(Q^\ast A^\ast)
        = A(QQ^\ast)A^\ast
        = AI_n A^\ast
        = AA^\ast
      \]
    \end{solution}
  \end{parts}


\end{questions}

\end{document}
