#!/bin/bash

infile=raw.tex

student=218s18-exam03
studentsage=$student.sagetex.sage

professor=218s18-exam03-solutions
professorsage=$professor.sagetex.sage

pdflatex -jobname=$student "\def\student{}\input{$infile}"
sage $studentsage
pdflatex -jobname=$student "\def\student{}\input{$infile}"
pdflatex -jobname=$student "\def\student{}\input{$infile}"

pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
sage $professorsage
pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
pdflatex -jobname=$professor "\def\professor{}\input{$infile}"
