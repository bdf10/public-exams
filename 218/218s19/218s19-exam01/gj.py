from functools import partial
from sage.all import elementary_matrix, flatten, identity_matrix, random_matrix, ZZ


class GJ(object):

    def __init__(self, A):
        self.A = A

    def gj_steps(self):
        return list(self.gj_steps_it())

    def n_steps(self):
        return len(self.gj_steps())

    def elementary_matrices_it(self):
        return (t[0] for t in self.gj_steps_it())

    def elementary_matrices(self):
        return list(self.elementary_matrices())

    def stages_it(self):
        return (t[1] for t in self.gj_steps_it())

    def stages(self):
        return list(self.stages_it())

    def needs_QQ(self):
        for Ai in self.stages_it():
            for a in Ai.list():
                if a not in ZZ:
                    return True
        return False

    def __repr__(self):
        m, n = self.A.dimensions()
        n_steps = self.n_steps()
        S = 'ZZ'
        if self.needs_QQ():
            S = 'QQ'
        return 'Gauss-Jordan steps for a {}x{} matrix in {} steps over {}.'.format(m, n, n_steps, S)

    def gj_steps_it(self):
        A = self.A
        m, n = A.dimensions()
        i = j = 0
        elem = partial(elementary_matrix, m)
        while i <= m-1 and j <= n-1:
            aij = A[i, j]
            if aij == 0:
                col = A.columns()[j]
                def f((k, x)): return False if x == 0 or k <= i else True
                swaps = filter(f, enumerate(col))
                if not swaps:
                    j += 1
                    continue
                k, _ = swaps[0]
                E = elem(row1=i, row2=k)
                A = E*A
                yield E, A
                continue
            if aij == 1:
                col = A.columns()[j]
                def f((k, x)): return False if x == 0 or k == i else True
                mults = filter(f, enumerate(col))
                if not mults:
                    i += 1
                    j += 1
                    continue
                E = identity_matrix(m)
                for k, mult in mults:
                    E *= elem(row1=k, row2=i, scale=-mult)
                A = E*A
                yield E, A
                i += 1
                j += 1
                continue
            E = elem(row1=i, scale=1/aij)
            A = E*A
            yield E, A


def random_echelonizable_ZZ(m, n, r, nsteps, upper_bound=20):
    A = random_matrix(ZZ, m, n, algorithm='echelonizable',
                      rank=r, upper_bound=upper_bound)
    l = list(gj(A))
    while len(l) != nsteps or any(map(lambda x: x not in ZZ, flatten([t[1].list() for t in l]))):
        A = random_matrix(ZZ, m, n, algorithm='echelonizable', rank=r)
        l = list(gj(A))
    return A
