class IncidenceLatex:

    def __init__(self, A):
        self.A = A
        self.m = A.nrows()
        self.n = A.ncols()

    def _latex_(self):
        head = r'\begin{blockarray}{*{' + str(self.n+1) + '}r}\n'
        head += r'\begin{block}{*{' + str(self.n+1) + '}c}\n'
        for i in range(self.n):
            head += '& a_{{{}}} '.format(i+1)
        head += r'\\' + '\n'
        head += r'\end{block}' + '\n'
        head += r'\begin{block}{r[*{' + str(self.n) + '}r]}\n'
        tex = self.A._latex_().split('\n')[1:-1]
        tex[-1] += r'\\'
        for i, line in enumerate(tex):
            tex[i] = 'v_{} & '.format(i+1) + line
        tail = '\n' + r'\end{block}' + '\n'
        tail += r'\end{blockarray}'
        return head + '\n'.join(tex) + tail
