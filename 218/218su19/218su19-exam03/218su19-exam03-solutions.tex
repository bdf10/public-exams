\documentclass[12pt]{exam}

\usepackage{fitzmath}
\usepackage{fitzexam}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{75-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2019}{6}{21}{-1}}

\setlength\answerclearance{1ex}

\begin{document}


\begin{questions}

  \begin{sagesilent}
    A = matrix([(-2, -1, 1), (0, -2, 1), (0, 0, -3)])
    l1, l2 = set(A.eigenvalues())
    n = A.nrows()
    I = identity_matrix(n)
    El = lambda l: A-l*I
    v = lambda l: El(l).change_ring(QQ).right_kernel(basis='pivot').basis()[0]
    var('t')
    chi = factor(A.characteristic_polynomial(t))
  \end{sagesilent}
\question Consider the matrix $A = \sage{A}$.
  \vspace{.2in}
  \begin{parts}
  \part[2] The eigenvalues of $A$ are \fillin[$\lambda_1=\sage{l1}$ and
    $\lambda_2=\sage{l2}$][3in].

    \vspace{.2in}
  \part[2] The (factored) characteristic polynomial of $A$ is
    $\chi_A(t)=\fillin[$\sage{chi}$][3in]$.

    \vspace{.2in}
  \part[2] The coefficient of $t^{\sage{n}}$ in $\chi_A(t)$ is
    $c_{\sage{n}}=\fillin[$1$][2in]$.

    \vspace{.2in}
  \part[2] The coefficient of $t^{\sage{n-1}}$ in $\chi_A(t)$ is
    $c_{\sage{n-1}}=\fillin[$-\trace(A)=\sage{-A.trace()}$][2in]$.

    \vspace{.2in}
  \part[2] The constant coefficient of $\chi_A(t)$ is
    $c_{0}=\fillin[$(-1)^{\sage{n}}\det(A)=\sage{(-1)**n * A.det()}$][2in]$.

  \part[6] Find a basis of each eigenspace of $A$.
    \begin{solution}[\stretch{1}]
      Our eigenspaces are given by
      \begin{align*}
        E_{\sage{l1}} &= \Null\overset{A-(\sage{l1})\cdot I_{\sage{n}}}{\sage{El(l1)}} = \Null\overset{\rref(A-(\sage{l1})\cdot I_{\sage{n}})}{\sage{El(l1).rref()}} = \Span\Set{\sage{v(l1)}} \\
        E_{\sage{l2}} &= \Null\overset{A-(\sage{l2})\cdot I_{\sage{n}}}{\sage{El(l2)}} = \Null\overset{\rref(A-(\sage{l2})\cdot I_{\sage{n}})}{\sage{El(l2).rref()}} = \Span\Set{\sage{v(l2)}}
      \end{align*}
    \end{solution}


  \part[6] Find the Jordan canonical form $J$ of $A$.
    \begin{solution}[\stretch{1}]
      Our computations above give the following table of eigenvalues.
      \begin{sagesilent}
        a1 = A.eigenvalues().count(l1)
        a2 = A.eigenvalues().count(l2)
        g1 = El(l1).right_nullity()
        g2 = El(l2).right_nullity()
      \end{sagesilent}
      \[
        \begin{array}{rcc}
          \multicolumn{1}{c}{\lambda} & \gm_A(\lambda) & \am_A(\lambda) \\ \hline
          \sage{l1}                   & \sage{g1}      & \sage{a1}      \\
          \sage{l2}                   & \sage{g2}      & \sage{a2}
        \end{array}
      \]
      This means that $J=J_{1}(\sage{l1})\oplus J_{2}(\sage{l2})$.
    \end{solution}

  \part[4] Which adjectives (if any) apply to $A$?

    \begin{oneparcheckboxes}
      \choice diagonalizable
      \choice Hermitian
      \choice unitary
      \choice positive definite
    \end{oneparcheckboxes}

  \part[4] Which adjectives (if any) apply to $A^\intercal A$?

    \begin{oneparcheckboxes}
      \CorrectChoice diagonalizable
      \CorrectChoice Hermitian
      \choice unitary
      \CorrectChoice positive definite
    \end{oneparcheckboxes}
  \end{parts}

  \skippage
  \begin{sagesilent}
    P = matrix([(2, -1, -2), (-1, 1, 0), (-1, 1, 1)])
    P = matrix([(-1, -1, -1), (1, -1, 0), (2, -1, 1)])
    D = diagonal_matrix([0, 3, -1])
    A = P*D*P.inverse()
    u0 = vector([-1, 0, 1])
    u0c = matrix.column(u0)
    var('ast')
    As = A.change_ring(SR)
    Ps = P.change_ring(SR)
    Ds = D.change_ring(SR)
    Pis = P.inverse().change_ring(SR)
    As[0, 1] = As[1, 0] = As[1, 2] = ast
    # Ds[1, 1] = ast
    Ps[0, 1] = Ps[1, 1] = Ps[2, 1] = ast
    Ds[0, 0] = ast
    Pis[0, 1] = Pis[1, 1] = Pis[2, 1] = ast
  \end{sagesilent}
\question Consider the factorization
  \[
    \overset{A}{\sage{A}}
    =
    \overset{P}{\sage{Ps}}
    \overset{D}{\sage{Ds}}
    \overset{P^{-1}}{\sage{Pis}}
  \]
  where each entry marked $\ast$ is unknown.
  \begin{parts}
  \part[4] Find the missing entry in $D$.
    \begin{solution}[\stretch{1}]
      The equation $A=PDP^{-1}$ implies that $A$ and $D$ are \emph{similar}. In
      particular, $A$ and $D$ have the same trace. This gives the equation
      \[
        \sage{A[0, 0]}+\sage{A[1, 1]}+\sage{A[2, 2]}
        =
        \sage{Ds[0, 0]}+\sage{Ds[1, 1]}+(\sage{Ds[2, 2]})
      \]
      Simplifying gives $\ast=0$.

      Alternatively, since the given equation is a diagonalization of $A$, we
      know that the missing entry of $D$ is an eigenvalue of $A$ with
      corresponding eigenvector given by the second column of $P$. The equation
      $A\sage{P.T[0]}=\sage{A*P.T[0]}$ then implies that $\ast=0$.
    \end{solution}
  \part[6] The missing second column of $P$ spans a one-dimensional subspace of
    $\mathbb{R}^{\sage{A.nrows()}}$. Find a basis of this subspace.
    \begin{sagesilent}
      l = D[1, 1]
      I = identity_matrix(A.nrows())
      v, = (A-l*I).change_ring(QQ).right_kernel(basis='pivot').basis()
    \end{sagesilent}
    \begin{solution}[\stretch{2}]
      The given equation is a diagonalization of $A$. This means that the
      missing second column of $P$ must be a basis vector of the eigenspace
      $E_{\sage{l}}$. By definition, we have
      \[
        E_{\sage{l}}
        = \Null\overset{A-\sage{l}\cdot I_{\sage{A.nrows()}}}{\sage{A-l*I}}
        = \Null\overset{\rref(A-\sage{l}\cdot I_{\sage{A.nrows()}})}{\sage{(A-l*I).rref()}}
        = \Span\Set{\sage{v}}
      \]
      This gives our desired basis vector $\sage{v}$.
    \end{solution}

  \part[8] Find the solution $\vv{u}(t)$ to the system of differential
    equations $\vv{u}^\prime=A\vv{u}$ with initial condition
    $\vv*{u}{0}=\sage{u0}$. \emph{Hint.} This can be done without computing any
    of the unknown entries. Start by finding $P^{-1}\vv{u}(0)$.
    \begin{solution}[\stretch{2}]
      As we stated in class, the solution is
      $\vv{u}(t)=\exp(At)\vv{u}(0)=P\exp(Dt)P^{-1}\vv{u}(0)$. Computing this
      expression gives
      \begin{sagesilent}
        expDs = Ds
        expDs[0, 0] = exp(ast*t)
        expDs[1, 1] = exp(3*t)
        expDs[2, 2] = exp(-t)
      \end{sagesilent}
      \begin{align*}
        \vv{u}(t)
        &=
          \overset{P}{\sage{Ps}}
          \overset{\exp(Dt)}{\sage{expDs}}
          \overset{P^{-1}}{\sage{Pis}}
          \overset{\vv{u}(0)}{\sage{u0c}} \\
        &= \sage{Ps}\sage{expDs}\sage{Pis*u0c} \\
        &= \sage{Ps}\sage{expDs*Pis*u0c} \\
        &= \sage{Ps*expDs*Pis*u0c}
      \end{align*}
    \end{solution}
  \end{parts}

  \skippage
  \begin{sagesilent}
    A = matrix([(4, 1, -2), (-12, 1, -6), (-6, -1, 0)])
    var('t')
    I = identity_matrix(A.nrows())
    chi = t*I-A
    from functools import partial
    elem = partial(elementary_matrix, A.nrows())
    E1 = elem(row1=0, row2=2, scale=-A[0, 1]/A[2, 1])
    E2 = elem(row1=1, row2=2, scale=-A[1, 0]/A[2, 0])
    f1 = gcd((E2*E1*chi)[0].change_ring(ZZ[t]))
    S1 = elem(row1=0, scale=1/f1)
    f2 = gcd((E2*E1*chi)[1].change_ring(ZZ[t]))
    S2 = elem(row1=1, scale=1/f2)
    R = S2*(S1*(E2*E1*chi))
    E3 = elem(row1=2, row2=0, scale=-R[2, 0]/R[0, 0])
    E4 = elem(row1=2, row2=1, scale=-R[2, 1]/R[1, 1])
  \end{sagesilent}
\question[12] Find the eigenvalues of $A=\sage{A}$.
  \begin{solution}[\stretch{3}]
    We can compute the characteristic polynomial $\chi_A(t)$ by row reducing
    \begin{sagesilent}
      latex.matrix_delimiters(left='|', right='|')
    \end{sagesilent}
    \begin{align*}
      \chi_A(t)
      &= \det(t\cdot I_{\sage{A.nrows()}}-A) \\
      &= \sage{chi} \\
      &= \sage{E2*E1*chi} \\
      &= \sage{factor(f2*f1)}\sage{S2*(S1*(E2*E1*chi))} \\
      &= \sage{factor(f2*f1)}\sage{E4*E3*(S2*(S1*(E2*E1*chi)))} \\
      &= \sage{A.fcp(t)}
    \end{align*}
    The roots of $\chi_A(t)$ are the eigenvalues of $A$. The eigenvalues of $A$ are thus
    \begin{sagesilent}
      l1, l2, l3 = A.eigenvalues()
    \end{sagesilent}
    \begin{align*}
      \lambda_1 &= \sage{l1} & \lambda_2 &= \sage{l2} & \lambda_3 &= \sage{l3}
    \end{align*}
    \begin{sagesilent}
      latex.matrix_delimiters(left='[', right=']')
    \end{sagesilent}
  \end{solution}


\question A matrix $S$ is \emph{skew Hermitian} if $S^\ast=-S$.
  \begin{parts}
  \part[5] Suppose that $S$ is skew Hermitian. Show that $H=iS$ is Hermitian.
    \begin{solution}[\stretch{1}]
      Note that
      \[
        H^\ast
        = (iS)^\ast
        = \overline{i}S^\ast
        = (-i)(-S)
        = iS
        = H
      \]
      This proves that $S$ is Hermitian.
    \end{solution}
  \part[5] Suppose that $S$ is skew Hermitian. Show that $S$ is
    diagonalizable. \emph{Hint.} What major theorem from class applies to
    $H=iS$?
    \begin{solution}[\stretch{1}]
      Since $H=iS$ is Hermitian, the Spectral theorem ensures a factorization
      $H=QDQ^\ast$ where $Q$ is unitary and $D$ is real-diagonal. Multiplying
      this factorization by $-i$ gives
      \[
        S=Q(-iD)Q^\ast
      \]
      Since $-iD$ is diagonal, this implies that $S$ is diagonalizable as
      required.
    \end{solution}
  \end{parts}

  \skippage
  \begin{sagesilent}
    S = matrix([(1, -2, 1), (-2, 8, -4), (1, -4, 11)])
    from functools import partial
    elem = partial(elementary_matrix, S.nrows())
    m21 = S[1, 0] / S[0, 0]
    m31 = S[2, 0] / S[0, 0]
    E21 = elem(row1=1, row2=0, scale=-m21)
    E31 = elem(row1=2, row2=0, scale=-m31)
    S1 = E31*E21*S
    m32 = S1[2, 1] / S1[1, 1]
    E32 = elem(row1=2, row2=1, scale=-m32)
    S2 = E32*S1
  \end{sagesilent}
\question[10] Find the Cholesky factorization of $S=\sage{S}$.
  \begin{solution}[\stretch{2}]
    Consider the row reductions
    \newcommand{\myStepA}{\arraycolsep=1pt\tiny
      \begin{array}{rcrcr}
        R_2 &-& (\sage{m21})\cdot R_1 &\to& R_2 \\
        R_3 &-& (\sage{m31})\cdot R_1 &\to& R_3
      \end{array}
    }
    \[
      \sage{S}
      \xrightarrow{\myStepA}\sage{S1}
      \xrightarrow{R_3-(\sage{m32})\cdot R_2\to R_3}\sage{S2}
    \]
    This gives the $LU$ factorization
    \begin{sagesilent}
      P, L, U = S.LU(pivot='nonzero')
    \end{sagesilent}
    \[
      \overset{S}{\sage{S}}
      =
      \overset{L}{\sage{L}}
      \overset{U}{\sage{U}}
    \]
    We then define $D$ by zeroing out the nondiaonal entries of $U$.
    \begin{sagesilent}
      D = diagonal_matrix(U.diagonal())
    \end{sagesilent}
    \[
      D = \sage{D}
    \]
    This gives the Cholesky factorization $S=A^\intercal A$ where
    \begin{sagesilent}
      sqrtD = diagonal_matrix(map(sqrt, U.diagonal()))
    \end{sagesilent}
    \[
      \overset{\sqrt{D}}{\sage{sqrtD}}
      \overset{L^\intercal}{\sage{L.T}}
      =
      \overset{A}{\sage{sqrtD*L.T}}
    \]
  \end{solution}

  \skippage

  \begin{sagesilent}
    S = matrix([(-5, -2, 2), (-2, -2, 1), (2, 1, -2)])
    P = matrix([(2, 1, 3), (1, 2, -1), (-1, 4, 5)])
    D = P.inverse()*S*P
    var('x1 x2 x3 y1 y2 y3')
    x = vector([x1, x2, x3])
    y = vector([y1, y2, y3])
    xc = matrix.column([x1, x2, x3])
    yc = matrix.column([y1, y2, y3])
    Q = expand(x*S*x)
  \end{sagesilent}
\question Consider the factorization
  \[
    \overset{S}{\sage{S}}
    =
    \overset{P}{\sage{P}}
    \overset{D}{\sage{D}}
    \overset{P^{-1}}{\sage{P.inverse()}}
  \]
  Note that $S$ is real-symmetric and thus defines a quadratic form
  $Q\sage{tuple(x)}$.
  \vspace{.25in}
  \begin{parts}
  \part[6] $Q\sage{tuple(x)}=\fillin[$\sage{Q}$][5in]$
  \part[4] Determine the definiteness of $Q\sage{tuple(x)}$. Clearly explain
    your reasoning for full credit.
    \begin{sagesilent}
      l1, l2, l3 = S.eigenvalues()
    \end{sagesilent}
    \begin{solution}[\stretch{1}]
      We are given a diagonalization of $S$, so the eigenvalues of $S$ are
      $\lambda_1=\sage{l1}$ and $\lambda_2=\sage{l2}$. Each eigenvalue is
      negative, so our classification theorem implies that $Q\sage{tuple(x)}$ is
      \emph{negative definite}.
    \end{solution}
  \part[10] Complete the square to write $Q\sage{tuple(x)}$ as a linear
    combination of square-terms involving the variables $x_1$, $x_2$, and $x_3$.
    \begin{solution}[\stretch{3}]
      The equation $S=PDP^{-1}$ gives us bases for the eigenspaces
      \begin{sagesilent}
        v1, v2, v3 = P.columns()
      \end{sagesilent}
      \begin{align*}
        E_{\sage{l1}} &= \Span\Set{\sage{v1}} & E_{\sage{l2}} &= \Span\Set{\sage{v2}, \sage{v3}}
      \end{align*}
      We need a spectral decomposition to complete the square, which means that
      we need \emph{orthonormal} bases for the eigenspaces.
      \begin{sagesilent}
        v1, v2 = v2, v3
        w1 = v1
        w2 = v2-(w1*v2)/(w1*w1)*w1
      \end{sagesilent}
      For $E_{\sage{l2}}$, we apply the Gram-Schmidt algorithm to the two basis
      vectors $\vv*{v}{1}$ and $\vv*{v}{2}$. First, we have
      $\vv*{w}{1}=\vv*{v}{1}=\sage{w1}$. It follows that
      \begin{align*}
        \vv*{w}{2}
        &= \vv*{v}{2}-\proj_{\vv*{w}{1}}(\vv*{v}{2}) \\
        &= \sage{v2}-\oldfrac{\sage{w1}\cdot\sage{v2}}{\sage{w1}\cdot\sage{w1}}\sage{w1} \\
        &= \sage{w2}
      \end{align*}
      This allows us to redefine $P$ to obtain the spectral decomposition
      $S=PDP^\intercal$ where
      \[
        P
        =
        \left[
          \begin{array}{rrr}
            \frac{2}{\sqrt{6}} & \frac{1}{\sqrt{21}} & \frac{2}{\sqrt{14}} \\
            \frac{1}{\sqrt{6}} & \frac{2}{\sqrt{21}} & \frac{-3}{\sqrt{14}} \\
            \frac{-1}{\sqrt{6}} & \frac{4}{\sqrt{21}} & \frac{1}{\sqrt{14}}
          \end{array}
        \right]
      \]
      Defining $\vv{y}=P^\intercal\vv{x}$ then gives
      \begin{sagesilent}
        v1, v2, v3 = P.columns()
        v3 = w2
      \end{sagesilent}
      \begin{align*}
        Q\sage{tuple(x)}
        &= \lambda_1\cdot y_1^2 + \lambda_2\cdot y_2^2 + \lambda_3\cdot y_3^2 \\
        &= \sage{l1}\cdot\Set*{\oldfrac{1}{\sqrt{6}}\pair{\sage{v1*x}}}^2
          -\Set*{\oldfrac{1}{\sqrt{21}}\pair{\sage{v2*x}}}^2 \\
        &\qquad-\Set*{\oldfrac{1}{\sqrt{14}}\pair{\sage{v3*x}}}^2
      \end{align*}
    \end{solution}
  \end{parts}

\end{questions}




\end{document}
