\documentclass[12pt]{exm}

\usepackage{extarrows}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2021}{11}{19}{-1}}

\begin{document}

\begin{questions}

  \begin{sagesilent}
    A = matrix(ZZ, [[1, -1, 1, 0, -2], [-1, 0, -1, 1, 0], [-1, 1, 0, 1, 0], [-2, -1, -2, 0, 1], [0, 0, -1, -1, 1]])
    C = A.adjugate().T
    var('ast')
    C = C.change_ring(SR)
    i, j = 4, 3
    Cij = C[i-1, j-1]
    C[i-1, j-1] = C[i-1, j] = C[i-2, j-1] = C[i-2, j-2] = ast
  \end{sagesilent}
  \question Consider the invertible matrix $A$ and its cofactor matrix $C$ given
  by
  \begin{align*}
    A &= \sage{A} & C &= \sage{C}
  \end{align*}
  Note that $C$ is missing several entries.
  \begin{parts}
    \part[10] Find the missing $\sage{(i, j)}$ entry of $C$.
    \begin{solution}[\stretch{1}]
      This is the $\sage{(i, j)}$ cofactor of $A$, which is
      $C_{\sage{i}\sage{j}}=(-1)^{\sage{i}+\sage{j}}\cdot\abs{A_{\sage{i}\sage{j}}}=\sage{(-1)**(i+j)}\cdot\abs{A_{\sage{i}\sage{j}}}$. The
      missing entry of $C$ is then
      \begin{sagesilent}
        Aij = A.matrix_from_rows_and_columns([x for x in range(A.nrows()) if x != i-1], [x for x in range(A.nrows()) if x != j-1])
        from functools import partial
        e = partial(elementary_matrix, Aij.nrows())
        Aij1 = e(row1=1, row2=0, scale=1)*e(row1=2, row2=0, scale=1)*Aij
        Aij2 = e(row1=3, row2=2, scale=1)*Aij1
      \end{sagesilent}
      \newcommand{\StepA}{
        \begin{NiceArray}{rcrcr}[small]
          \bv[r]_2 &+& \bv[r]_1 &\to& \bv[r]_2 \\
          \bv[r]_3 &+& \bv[r]_1 &\to& \bv[r]_3
        \end{NiceArray}
      }
      \[
        \ast
        = \sage{(-1)**(i+j)}\cdot\sage{detmat(Aij)}
        \xlongequal{\StepA}\sage{(-1)**(i+j)}\cdot\sage{detmat(Aij1)}
        \xlongequal{\bv[r]_4+\bv[r]_3\to\bv[r]_4}\sage{(-1)**(i+j)}\cdot\sage{detmat(Aij2)}
        = \sage{Cij}
      \]
    \end{solution}
    \part[10] Find $\chi_A(0)$ (the constant coefficient of the characteristic
    polynomial of $A$). \emph{Hint}. This can be done by calculating a single
    inner product.
    \begin{solution}[\stretch{1}]
      We know from class that $\chi_A(0)=(-1)^5\cdot\det(A)=-\det(A)$. We also
      know that $A C^\intercal=A\adj(A)=\det(A)\cdot I_5$, which means that
      $\det(A)$ can be calculated as the inner product of the $i$th row of $A$
      and the $i$th row of $C$. So, the constant coefficient of the
      characteristic polynomial of $A$ is
      \[
        \chi_A(0)
        = -\det(A)
        = -\inner{\sage{A[0]}, \sage{C[0]}}
        = \sage{-A[0]*C[0]}
      \]
    \end{solution}

    \part[10] Find the solution $\bv[x]$ to the system $A\bv[x]=\bv[b]$ where
    $\bv[b]=\det(A)\cdot\nv{1 0 0 0 1}$.
    \begin{solution}[\stretch{1}]
      Our matrix $A$ is invertible, so we have a single solution given by
      \[
        \bv[x]
        = A^{-1}\bv[b]
        = \frac{1}{\det(A)}C^\intercal\det(A)\cdot\nvc{1; 0; 0; 0; 1}
        = C^\intercal\nvc{1; 0; 0; 0; 1}
        = \sage{C[0].column()}+\sage{C[-1].column()}
        = \sage{C[0].column()+C[-1].column()}
      \]
    \end{solution}
  \end{parts}

  \skippage
  \begin{sagesilent}
    A = matrix(ZZ, [[5, -3], [6, -4]])
    var('t')
    chi = A.characteristic_polynomial(t)
    var('f g')
    u = vector([f, g])
    up = A*u
    var('x y')
    x, y = 1, -2
    u0 = vector([x, y])
    l1, l2 = A.eigenvalues()
    E = lambda l: l*identity_matrix(A.nrows())-A
    v1 = vector([1, 1])
    v2 = vector([1, 2])
    X = matrix.column([v1, v2])
    D = diagonal_matrix([l1, l2])
  \end{sagesilent}
  \newcommand{\IVP}{
    \begin{NiceArray}{rcrcr}
      f^\prime &=& 5\,f &-& 3\,g \\
      g^\prime &=& 6\,f &-& 4\,g
    \end{NiceArray}
  }
  \newcommand{\IV}{
    \begin{NiceArray}{rcr}
      f(0) &=& \sage{u0[0]} \\
      g(0) &=& \sage{u0[1]}
    \end{NiceArray}
  }
  \question[20] Consider the system of differential equations given by
  \begin{align*}
    \IVP && \IV
  \end{align*}
  Find $f(t)$ and $g(t)$.
  \begin{solution}[\stretch{1}]
    This initial value problem is $\bv[u]^\prime=A\bv[u]$ with $\bv[u](0)=\bv[u]_0$ where
    \begin{align*}
      \bv[u] &= \nvc{f(t); g(g)} & A &= \sage{A} & \bv[u]_0 &= \sage{u0.column()}
    \end{align*}
    The solution is $\bv[u](t)=\exp(At)\bv[u]_0$. To calculate this matrix
    exponential, we must diagonalize $A$. We start with the characteristic
    polynomial
    \[
      \chi_A(t)
      = t^2-\trace(A)\,t+\det(A)
      = \sage{chi}
      = \sage{factor(chi)}
    \]
    So, we have $\EVals(A)=\sage{Set(A.eigenvalues())}$. The eigenspaces are
    \begin{align*}
      \mathcal{E}_A(\sage{l1}) &= \Null\overset{\sage{l1}\cdot I_{\sage{A.nrows()}}-A}{\sage{E(l1)}} = \Span\Set*{\sage{v1.column()}} &
                                                                                                                                        \mathcal{E}_A(\sage{l2}) &= \Null\overset{\sage{l2}\cdot I_{\sage{A.nrows()}}-A}{\sage{E(l2)}} = \Span\Set*{\sage{v2.column()}}
    \end{align*}
    This gives our diagonalization
    \[
      \overset{A}{\sage{A}}
      =
      \overset{X}{\sage{X}}
      \overset{D}{\sage{D}}
      \overset{X^{-1}}{\sage{X.inverse()}}
    \]
    Now, the solution to our initial value problem is
    \newcommand{\ExpDt}{
      \begin{bNiceMatrix}
        {} e^{\sage{l1*t}} &                 \\
        {}                 & e^{\sage{l2*t}}
      \end{bNiceMatrix}
    }
    \begin{align*}
      \nvc{f(t); g(t)}
      &=
        \overset{X}{\sage{X}}
        \overset{\exp(Dt)}{\ExpDt}
        \overset{X^{-1}}{\sage{X.inverse()}}
        \overset{\bv[u]_0}{\sage{u0.column()}}
        = \sage{X}\ExpDt\sage{X.inverse()*u0.column()}
        = \sage{X}\nvc{4\,e^{2\,t}; -3\,e^{-t}}
        = \nvc{4\,e^{2\,t}-3\,e^{-t}; 4\,e^{2\,t}-6\,e^{-t}}
    \end{align*}
  \end{solution}


  \skippage

  \begin{sagesilent}
    var('l1 l2')
    U = matrix([(1/2*I, 1/2*I, 1/2*I*sqrt(2), 0), (1/2*I, -1/2*I, 0, -1/2*sqrt(2)), (1/2*I, 1/2*I, -1/2*I*sqrt(2), 0), (1/2, -1/2, 0, -1/2*I*sqrt(2))])
    D = diagonal_matrix([l2, l1, l2, l2])
    H = U*D*U.H
    lambda1, lambda2 = 2, 10
    H = H(l1=lambda1, l2=lambda2)
    v = 2*U.T[1]
    var('ast')
    H_old = copy(H)
    E = lambda l: l*identity_matrix(H_old.nrows())-H_old
    H = H.change_ring(SR)
    H[0, 1] = H[1, 0] = H[3, 0] = H[0, 3] = H[2, 1] = H[1, 2] = H[2, 3] = H[3, 1] = H[1, 3] = ast
  \end{sagesilent}
  \question The matrix $H$ below is Hermitian with exactly two eigenvalues
  $\EVals(H)=\Set{\lambda_1, \lambda_2}$ where $\lambda_1=2$ and $\lambda_2$ is
  unknown. A basis of $\mathcal{E}_H(\sage{lambda1})$ is given below.
  \begin{align*}
    H &= \sage{H} & \mathcal{E}_H(\sage{lambda1}) &= \Span\Set{\sage{v}}
  \end{align*}
  Note that $H$ is missing several entries.
  \begin{parts}
    \vspace{0.25cm}
    \part[3] The $(3, 4)$ entry of $H$ is \fillin[$\overline{\sage{H_old[3, 2]}}=\sage{H_old[2, 3]}$].
    \vspace{0.25cm}
    \part[3] The algebraic multiplicity of $\lambda_1=\sage{lambda1}$ as an
    eigenvalue of $H$ is \fillin[$\sage{E(lambda1).right_nullity()}$][0.5in].
    \vspace{0.25cm}
    \part[3] The algebraic multiplicity of $\lambda_2$ as an
    eigenvalue of $H$ is \fillin[$\sage{E(lambda2).right_nullity()}$][0.5in].
    \vspace{0.25cm}
    \part[3] The coefficient of $t^{\sage{H.nrows()-1}}$ in $\chi_H(t)$ is
    \fillin[$-\trace(H)=\sage{-H.trace()}$].
    \begin{sagesilent}
      w = vector([I, 1, -I, -I])
    \end{sagesilent}
    \part[8] Determine if $\sage{w}\in\mathcal{E}_H(\lambda_2)$. Explain your resoning.
    \begin{solution}[\stretch{1}]
      The Spectral Theorem says that $\mathcal{E}_H(\lambda_2)$ is orthogonal to
      $\mathcal{E}_H(\sage{lambda1})$. The inner product
      \begin{align*}
        \inner{\sage{w}, \sage{v}}
        &= \overline{(\sage{w[0]})}\cdot(\sage{v[0]})
          + \overline{(\sage{w[1]})}\cdot(\sage{v[1]})
          + \overline{(\sage{w[2]})}\cdot(\sage{v[2]})
          + \overline{(\sage{w[3]})}\cdot(\sage{v[3]}) \\
        &= (\sage{w[0].conjugate()*v[0]})
          + (\sage{w[1].conjugate()*v[1]})
          + (\sage{w[2].conjugate()*v[2]})
          + (\sage{w[3].conjugate()*v[3]}) \\
        &= \sage{w.hermitian_inner_product(v)} \\
        &\neq 0
      \end{align*}
      then tells us that $\sage{w}\notin\mathcal{E}_H(\lambda_2)$.
    \end{solution}
    \part[8] Find $\lambda_2$. Explain your reasoning.
    \begin{solution}[\stretch{1}]
      We know that
      $\trace(H)=\am_H(\lambda_1)\cdot\lambda_1+\am_H(\lambda_2)\cdot\lambda_2$,
      which gives
      \[
        \sage{H.trace()}
        =
        1\cdot2+3\cdot\lambda_2
      \]
      This implies that $\lambda_2=\sage{lambda2}$.
    \end{solution}

  \end{parts}


  \skippage
  \begin{sagesilent}
    set_random_seed(931)
    v1 = vector([1, 3, -2, 2])
    v2 = vector([4, -2, 1, 5])
    v3 = v1+v2
    A = matrix.column([v1, v2, v3])
    var('x1 x2 x3')
    x = vector([x1, x2, x3])
    y = A*x
    q = y*y
  \end{sagesilent}
  \question Consider the quadratic form on $\mathbb{R}^3$ given by
  \[
    q(x_1, x_2, x_3)
    = (\sage{y[0]})^2+(\sage{y[1]})^2+(\sage{y[2]})^2+(\sage{y[3]})^2
  \]
  Note that this quadratic form may be written as
  $q(\bv[x])=\inner{\bv[x], S\bv[x]}$ where $S$ is real-symmetric.
  \begin{parts}
    \part[3] Which of the following adjectives correctly describes $q(\bv[x])$?

    \begin{oneparcheckboxes}
      \correctchoice positive semidefinite
      \choice negative semidefinite
      \choice indefinite
    \end{oneparcheckboxes}

    \part[3] Which of the following correctly describes the eigenvalues of $S$?

    \begin{checkboxes}
      \choice Some eigenvalues of $S$ are positive and some eigenvalues of $S$ are negative.
      \choice The eigenvalues of $S$ are all nonpositive.
      \correctchoice The eigenvalues of $S$ are all nonnegative.
    \end{checkboxes}

    \part[10] If possible, find $A$ such that $S=A^\intercal A$. If this is not
    possible, then explain why.
    \begin{solution}[\stretch{1}]
      Recall that $S=A^\intercal A$ implies
      $q(\bv[x]) = \inner{\bv[x], S\bv[x]} = \inner{\bv[x], A^\intercal A\bv[x]}
      = \inner{A\bv[x], A\bv[x]} = \norm{A\bv[x]}^2 $. Our quadratic form is
      given to us as a sum of squares, so we have $S=A^\intercal A$ where
      \[
        A = \sage{A}
      \]
    \end{solution}

    \part[6] If possible, find $\bv[x]\neq\bv[O]$ satisfying $q(\bv[x])=0$. If this
    is not possible, then explain why.
    \begin{solution}[\stretch{1}]
      From (\emph{c}), we know that $q(\bv[x])=\norm{A\bv[x]}^2$. This means
      that any $\bv[x]\neq\bv[O]$ in $\Null(A)$ will do. Noting that the third
      column of $A$ is the sum of the first two then tells us that
      $q(1, 1, -1)=0$.
    \end{solution}



  \end{parts}



\end{questions}




\end{document}
