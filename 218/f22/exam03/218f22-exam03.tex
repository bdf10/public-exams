\documentclass[12pt]{exm}

\usepackage{extarrows}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2022}{12}{02}{-1}}

\begin{document}

\begin{questions}

  \begin{sagesilent}
    A = matrix(ZZ, [[1, 27, 0, 0, 0], [0, -1, 0, 0, 0], [0, 0, 2, 1, 2], [0, 0, 8, 3, 10], [0, 0, -4, -1, -4]])
    var('t')
    chi = A.characteristic_polynomial(t)
    var('c')
    Ac = A.change_ring(SR)
    i, j = 4, 5
    Ac[i-1,j-1] = c
    from functools import partial
    e = partial(elementary_matrix, Ac.nrows())
    Ac1 = e(row1=3, row2=2, scale=-4)*e(row1=4, row2=2, scale=2)*Ac
    Ac2 = e(row1=4, row2=3, scale=1)*Ac1
  \end{sagesilent}
  \question Consider the matrix $A$ and its characteristic polynomial $\chi_A(t)$ given by
  \begin{align*}
    A &= \sage{Ac} & \chi_A(t) &= t^5+\fillin[$\sage{-A.trace()}$][0.5in]t^4-5\,t^3+5\,t^2+4\,t-4
  \end{align*}
  Note that the $\sage{(i, j)}$ entry of $A$ is a variable marked $c$. Also note
  that the coefficient of $t^4$ in $\chi_A(t)$ is blank.
  \begin{parts}
    \part[4] Fill in the blank coefficient of $t^4$ in $\chi_A(t)$.
    \part[10] Find $c$. \emph{Hint}. What is $\det(A)$?
    \begin{solution}[\stretch{3}]
      This $A$ is $5\times 5$, so
      $\det(A)=(-1)^5\cdot\chi(0)=\sage{A.det()}$. On the other hand, we have
      the direct calculation
      \newcommand{\StepA}{
        \begin{NiceArray}[small]{rcrcr}
          \bv[r]_4 &+& 2\cdot\bv[r]_3 &\to& \bv[r]_4 \\
          \bv[r]_5 &-&       \bv[r]_3 &\to& \bv[r]_5
        \end{NiceArray}
      }
      \newcommand{\StepB}{
        \begin{NiceArray}[small]{rcrcr}
          \bv[r]_5 &+& \bv[r]_4 &\to& \bv[r]_5
        \end{NiceArray}
      }
      \begin{align*}
        \overset{\det(A)}{\sage{detmat(Ac)}}
        &\xlongequal{\StepA}\sage{detmat(Ac1)} \\
        &\xlongequal{\StepA}\sage{detmat(Ac2)} \\
        &= 2\,(c-8)
      \end{align*}
      Putting this together gives $2\,(c-8)=4$, which implies
      $c=\sage{A[i-1,j-1]}$.
    \end{solution}
    \part[10] The scalar $2$ is an eigenvalue of $A$. Suppose that $v$ is an
    eigenvector of $A$ corresponding to the eigenvalue $2$. Show that $\bv$ is
    also an eigenvector of $\adj(A)$ and identify its corresponding eigenvalue
    $\lambda$.
    \begin{solution}[\stretch{2}]
      We are told that $A\bv=2\cdot\bv$. We wish to show that
      $\adj(A)\bv=\lambda\cdot\bv$. To do so, we calculate
      \begin{align*}
        \adj(A)\bv
        &= \adj(A)\frac{2}{2}\bv \\
        &= \frac{1}{2}\adj(A)2\cdot\bv \\
        &= \frac{1}{2}\adj(A)A\bv \\
        &= \frac{1}{2}\det(A)\cdot I_5\bv \\
        &= \frac{\det(A)}{2}\cdot\bv \\
        &= \frac{\sage{A.det()}}{2}\cdot\bv \\
        &= \sage{A.det()/2}\cdot\bv
      \end{align*}
      This shows that $\bv$ is indeed an eigenvector of $\adj(A)$ with
      corresponding eigenvalue $\lambda=\sage{A.det()/2}$.
    \end{solution}

  \end{parts}

  \skippage

  \begin{sagesilent}
    set_random_seed(894140)
    D = diagonal_matrix([3, I, 1-I, 1-I])
    X = random_matrix(ZZ, D.nrows(), algorithm='unimodular', upper_bound=5)
    A = X*D*X.inverse()
  \end{sagesilent}

  \newcommand{\BlankMat}[1][A]{
    \begin{bNiceArray}{cccc}
      \Block{4-4}<\LARGE>{#1} &&&\\[1ex]
      \phantom{x}&&&\phantom{x}\\
      &&&\\
      &&&
    \end{bNiceArray}
  }
  \newcommand{\MatrixD}{
    \begin{bNiceArray}{rrcc}
      3 & 0 & 0   & 0 \\
      0 & i & 0   & 0 \\
      0 & 0 & 1-i & 0 \\
      0 & 0 & 0   & 1-i
    \end{bNiceArray}
  }

  \question Consider the following matrix factorization $A=XDX^{-1}$.
  \[
    \BlankMat
    =
    \overset{X}{\sage{X}}
    \overset{D}{\MatrixD}
    \overset{X^{-1}}{\sage{X.inverse()}}
  \]

  \vspace{1em}

  \begin{parts}
    \part[5] The eigenvalue of $A$ with the largest geometric multiplicity is
    $\lambda=\fillin[$1-i$][0.5in]$ and that multiplicity is
    $\fillin[$2$][0.5in]$.

    \part[4] $\BlankMat\sage{matrix.column(X.T[1])}={\setlength\answerclearance{4em}\fillin[$\sage{A*matrix.column(X.T[1])}$][1.25in]}$

    \part[8] Calculate $\det(A)$. Simplify your answer to a complex number of
    the form $a+b\,i$.
    \begin{solution}[\stretch{1}]
      This is a diagonalization, so
      \[
        \det(A)
        = \det(D)
        = 3\,i(1-i)^2
        = 3\,i(1-2\,i+i^2)
        = 3\,i(1-2\,i-1)
        = 3\,i(-2\,i)
        = -6\,i^2
        = \sage{D.det()}
      \]
    \end{solution}

    \part[8] Calculate $\chi_A(3-i)$. Simplify your answer to a complex number
    of the form $a+b\,i$.
    \begin{solution}[\stretch{1}]
      The characteristic polynomial of $A$ factors as
      \[
        \chi_A(t)=(t-3)(t-i)(t-(1-i))^2
      \]
      It follows that
      \begin{align*}
        \chi_A(1+i)
        &= (3-i-3)(3-i-i)((3-i)-(1-i))^2 \\
        &= (-i)(3-2\,i)(3-i-1+i)^2 \\
        &= (-i)(3-2\,i)2^2 \\
        &= (-3\,i+2\,i^2)2^2 \\
        &= (-3\,i-2)2^2 \\
        &= -8-12\,i
      \end{align*}
    \end{solution}


  \end{parts}

  \skippage

  \begin{sagesilent}
    B = matrix([(0, 1, 2), (0, 0, 1), (0, 0, 0)])
    set_random_seed(48101)
    X = random_matrix(ZZ, B.nrows(), algorithm='unimodular', upper_bound=4)
    A = X*B*X.inverse()
    var('t')
    I = identity_matrix(B.nrows())
    u0 = vector([1,1,0])
  \end{sagesilent}
  \question Consider the factorization $A=XBX^{-1}$ below.
  \begin{align*}
    \overset{A}{\sage{A}} &= \overset{X}{\sage{X}}\overset{B}{\sage{B}}\overset{X^{-1}}{\sage{X.inverse()}} & B^2 &= {\setlength\answerclearance{2.5em}\fillin[$\sage{B**2}$][1.25in]} & B^3 &= {\setlength\answerclearance{2.5em}\fillin[$\sage{B^3}$][1.25in]}
  \end{align*}

  \begin{parts}
    \part[3] Fill in the blanks above to calculate $B^2$ and $B^3$.
    \vspace{1em}
    \part[4] $\trace(A)=\fillin[$\sage{A.trace()}$][0.5in]$, $\det(A)=\fillin[$\sage{A.det()}$][0.5in]$, and $\chi_A(t)=\fillin[$t^3$]$
    \part[10] Use the Taylor series definition of matrix exponentials to show
    that $\exp(Bt)=\sage{exp(B*t)}$.
    \begin{solution}[\stretch{1}]
      The key insight here is that $B^3=\bv[O]$, which means that $B^k=\bv[O]$
      for all $k\geq 3$. The Taylor series definition of matrix exponentials then gives
      \begin{align*}
        \exp(Bt)
        &= \sum_{k=0}^\infty\frac{1}{k!}B^kt^k \\
        &= I_3+B\,t+\frac{1}{2}B^2\,t^2 \\
        &= \sage{I}+\sage{B*t}+\sage{B**2*t**2/2} \\
        &= \sage{exp(B*t)}
      \end{align*}
    \end{solution}
    \part[10] Let $\bv[u](t)$ be the solution to $\bv[u]^\prime=A\bv[u]$ with
    initial condition $\bv[u](0)=\sage{u0}$. Calculate $\bv[u](2)$.
    \begin{solution}[\stretch{1}]
      Here, $\bv[u](t)=\exp(At)\bv[u]_0=X\exp(Bt)X^{-1}\bv[u]_0$. So, we calculate
      \begin{align*}
        \bv[u](2)
        &=
          \overset{X}{\sage{X}}
          \overset{\exp(2\,B)}{\sage{exp(2*B)}}
          \overset{X^{-1}}{\sage{X.inverse()}}\overset{\bv[u]_0}{\sage{matrix.column(u0)}} \\
        &= \sage{X}\sage{exp(2*B)}\sage{X.inverse()*matrix.column(u0)} \\
        &= \sage{X}\sage{exp(2*B)*X.inverse()*matrix.column(u0)} \\
        &= \sage{exp(2*A)*matrix.column(u0)} \\
      \end{align*}
    \end{solution}

  \end{parts}


  \skippage
  \begin{sagesilent}
    X = matrix(ZZ, [[0, 1, -1, 2], [1, 0, 0, 0], [0, 1, -1, -2], [0, -1, -2, 0]])
    l1, l2 = 3, 12
    D = diagonal_matrix([l1, l1, l2, 0])
    S = X*D*X.inverse()
    v1, v2, v3, v4 = map(matrix.column, X.columns())
    v3 = -v3
    v2 = 3*v1-v2
    var('c')
    v2s = v2.change_ring(SR)
    Sv2s = S*v2s
    v2s[-1,0] = c
    Sv2s[-1,0] = l1*c
    n, = S.right_kernel().basis()
    def proj(w, v): return (vector(v)*vector(w))/(vector(w)*vector(w))*vector(w)
  \end{sagesilent}
  \newcommand{\BlankS}{
    \begin{bNiceArray}{cccc}
      \Block{4-4}<\LARGE>{S} &&&\\
      \phantom{x}&&&\phantom{x}\\
      &&&\\
      &&&
    \end{bNiceArray}
  }

  \question Consider the quadratic form $q(\bv[x])=\inner{\bv[x], S\bv[x]}$
  where $S$ is the \emph{\textbf{singular}} real-symmetric matrix satisfying
  \begin{align*}
    \BlankS\overset{\bv_1}{\sage{v1}} &= \sage{S*v1} & \BlankS\overset{\bv_2}{\sage{v2s}} &= \sage{Sv2s} & \BlankS\overset{\bv_3}{\sage{v3}} &= \sage{S*v3}
  \end{align*}
  Note that the last coordinate of the vector $\bv_2$ above is marked as the
  variable $c$.
  \vspace{2em}
  \begin{parts}
    \part[7] The trace of $S$ is $\trace(S)=\fillin[$\sage{S.trace()}$]$ and the
    value of $c$ is $c=\fillin[$\sage{v2[-1,0]}$][0.5in]$.
    \part[3] Which of the following adjectives apply to $q(\bv[x])$? Select all
    that apply (no partial credit on this problem).

    \begin{oneparcheckboxes}
      \choice positive definite
      \correctchoice positive semidefinite
      \choice negative definite
      \choice negative semidefinite
      \choice indefinite
    \end{oneparcheckboxes}

    \part[4] Which of the following vectors $\bv[x]$ satisfies $q(\bv[x])=0$?

    \begin{oneparcheckboxes}
      \correctchoice $\bv[x]=\sage{n}$
      \choice $\bv[x]=\sage{n+vector(v1)}$
      \choice $\bv[x]=\sage{n+vector(v2)}$
      \choice none of these
    \end{oneparcheckboxes}

    % \part[4] Show that $\proj_{\bv_1}(\bv_2)=\sage{proj(v1, v2s)}$.
    % \begin{solution}[\stretch{1}]
    %   This is a direct application of the projection formula from class.
    %   \[
    %     \proj_{\bv_1}(\bv_2)
    %     = \frac{\inner{\bv_1,\bv_2}}{\inner{\bv_1,\bv_1}}\bv_1
    %     = \frac{3}{1}\cdot\sage{v1}
    %     = \sage{matrix.column(proj(v1, v2s))}
    %   \]
    % \end{solution}

    % \part[10] Calculate $\chi_S(2)$.

    \part[6] Complete the square to write $q(\bv[x])$ as a linear combination of
    squares. \emph{Note}. You may leave $c$ as a variable to solve this problem.
    \begin{solution}[\stretch{4}]
      The given equations are $S\bv_1 = 3\cdot\bv_1$, $S\bv_2 = 3\cdot\bv_2$,
      and $S\bv_3 = 12\cdot\bv_3$.  This means that $3$ and $12$ are
      eigenvalues of $S$. We are also told that $S$ is singular, so $0$ must
      also be an eigenvalue of $S$.

      More succinctly, we have
      \begin{align*}
        \mathcal{E}_S(3) &= \Span\Set{\bv_1,\bv_2} & \mathcal{E}_S(12) &= \Span\Set{\bv_3} & \mathcal{E}_S(0) &= \Span\Set{\bv_4}
      \end{align*}
      A spectral factorization would give
      \[
        q(\bv[x])
        = 3\cdot y_1^2 + 3\cdot y_2^2 + 12\cdot y_3^2 + 0\cdot y_4^2
        = 3\cdot y_1^2 + 3\cdot y_2^2 + 12\cdot y_3^2
      \]
      So, we only need formulas for $y_1$, $y_2$, and $y_3$. To do so, we need
      orthonormal bases for $\mathcal{E}_S(3)$ and $\mathcal{E}_S(12)$. Applying
      Gram-Schmidt to the basis of $\mathcal{E}_S(3)$ gives
      $\bv[w]_1=\sage{vector(v1)}$ and then
      \[
        \bv[w]_2
        = \bv_2-\proj_{\bv[w]_1}(\bv_2)
        = \sage{vector(v2s)}-\sage{proj(v1, v2s)}
        = \sage{vector(v2s)-proj(v1, v2s)}
      \]
      The orthonormal bases are
      \begin{sagesilent}
        q1 = v1
        q2 = matrix.column(vector(v2s)-proj(v1, v2s))
        q3 = v3
      \end{sagesilent}
      \begin{align*}
        \mathcal{E}_S(3) &= \Span\Set*{\sage{q1}, \frac{1}{\sqrt{2+c^2}}\sage{q2}} & \mathcal{E}_S(12) &= \Span\Set*{\frac{1}{\sqrt{6}}\sage{q3}}
      \end{align*}
      Our formulas for $y_1$, $y_2$, and $y_3$ are then
      \begin{align*}
        y_1 &= x_2 & y_2 &= \frac{-x_1-x_3+c\,x_4}{\sqrt{2+c^2}} & y_3 &= \frac{x_1+x_3+2\,x_4}{\sqrt{6}}
      \end{align*}
    \end{solution}

    \part[4] The quadratic form $q(\bv[x])=q(x_1,x_2,x_3,x_4)$ is a scalar
    field on $\mathbb{R}^4$. Calculate $\dfrac{\partial q}{\partial
      x_2}$.
    \begin{solution}[\stretch{1}]
      The cool part now is that $y_2$ and $y_3$ have no $x_2$-dependence!

      Our partial derivative ends up becoming
      \[
        \frac{\partial q}{\partial x_2}
        =
        2\cdot 3\,y_1\frac{\partial y_1}{\partial x_2}
        + 2\cdot 3\,y_2\frac{\partial y_2}{\partial x_2}
        + 2\cdot 12\,y_2\frac{\partial y_3}{\partial x_2}
        = 6\,y_1\cdot1 + 6\,y_2\cdot 0 + 24\,y_3\cdot 0
        = 6\,x_2
      \]
    \end{solution}

  \end{parts}



\end{questions}

\end{document}
