\documentclass[12pt]{exm}

\renewcommand{\examtitle}{Exam I}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2023}{9}{29}{-1}}

\begin{document}
\begin{questions}

  \noindent
  \begin{minipage}[T]{.55\textwidth}
    \question This figure depicts displacement vectors $\bv$, $\bv[w]$,
    $\bv[x]$, and $\bv[y]$ between points $P$, $Q$, $R$, and $S$ in
    $\mathbb{R}^5$ (so each of $P$, $Q$, $R$, and $S$ has five
    coordinates). Throughout this problem, assume that $A$ is a matrix
    satisfying $A\bv=\nvc{2;3}$ and $A\bv[w]=\nvc{7;1}$.
  \end{minipage}% This must go next to Y
  \begin{minipage}{.45\textwidth}
    \[
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , thick
        ]

        \coordinate (P) at (0,0);
        \coordinate (Q) at (3, -1);
        \coordinate (R) at (4, 1);
        \coordinate (S) at (1, 1);

        \fill (P) circle[radius=3pt] node[overlay, below left] {$P$};
        \fill (Q) circle[radius=3pt] node[overlay, below right] {$Q$};
        \fill (R) circle[radius=3pt] node[overlay, above right] {$R$};
        \fill (S) circle[radius=3pt] node[overlay, above left] {$S$};

        \draw[->, shorten >=3.5pt, shorten <=3.5pt] (P) -- (Q) node[midway, below, sloped] {$\bv$};
        \draw[->, shorten >=3.5pt, shorten <=3.5pt] (R) -- (Q) node[midway, right] {$\bv[w]$};
        \draw[->, shorten >=3.5pt, shorten <=3.5pt] (S) -- (P) node[midway, above, sloped] {$\bv[x]$};
        \draw[->, shorten >=3.5pt, shorten <=3.5pt] (S) -- (R) node[midway, above] {$\bv[y]$};

      \end{tikzpicture}
    \]
  \end{minipage}
  \begin{parts}
    \part[3] Which point is the \emph{tail} of $\bv$?
    \begin{oneparcheckboxes}
      \correctchoice $P$
      \choice $Q$
      \choice $R$
      \choice $S$
    \end{oneparcheckboxes}

    \vspace{2em}
    \part[3] Only one of the following formulas for $\bv[w]$ is correct. Select
    this formula.

    \begin{oneparcheckboxes}
      \choice $\bv[w]=\vv{QR}$
      \choice $\bv[w]=-\vv{RQ}$
      \choice $\bv[w]=\vv{Q}$
      \correctchoice $\bv[w]=\vv{RQ}$
      \choice $\bv[w]=\vv{R}$
    \end{oneparcheckboxes}

    \vspace{2em}
    \part[3] Only one of the following formulas for $\vv{RP}$ is correct. Select
    this formula.

    \begin{oneparcheckboxes}
      \choice $\vv{RP}=\bv[x]+\bv[y]$
      \correctchoice $\vv{RP}=\bv[x]-\bv[y]$
      \choice $\vv{RP}=-\bv[x]-\bv[y]$
      \choice $\vv{RP}=\bv[w]+\bv$
      \choice $\vv{RP}=\bv[y]$
    \end{oneparcheckboxes}

    \vspace{2em}
    \part[3] The number of rows of $A$ is \fillin[$2$][0.5in] and the number of
    columns of $A$ is \fillin[$5$][0.5in].

    \vspace{2em}
    \part[4] If we assumed that $P$ and $Q$ have coordinates $P(1,1,1,1,1)$ and
    $Q(1,1,2,1,1)$, then only one of the following statements is guaranteed to
    be correct. Select this statement.

    \begin{oneparcheckboxes}
      \choice The second column of $A$ is $\nvc{2;3}$
      \choice The second column of $A$ is $\nvc{7;1}$
      \choice The third column of $A$ is $\nvc{-2;-3}$ \\[0.5em]
      \choice The third column of $A$ is $\nvc{7;1}$
      \correctchoice The third column of $A$ is $\nvc{2;3}$
    \end{oneparcheckboxes}

    \vspace{2em}
    \part[8] Calculate the matrix-vector product
    $A(\bv[x]-\bv[y]+\bv[w])$. Clearly explain your reasoning to receive credit.
    \begin{solution}[\stretch{1}]
      The diagram demonstrates that $\bv[x]-\bv[y]=\vv{RP}=\bv[w]-\bv$, which implies
      \[
        A(\bv[x]-\bv[y]+\bv[w])
        = A(\bv[w]-\bv+\bv[w])
        = A(2\cdot\bv[w]-\bv)
        = 2\cdot A\bv[w]-A\bv
        = 2\cdot\nvc{7;1}-\nvc{2;3}
        = \nvc{12;-1}
      \]
    \end{solution}

  \end{parts}

  \skippage

  \newcommand{\BlockA}[1][A]{
    \begin{bNiceMatrix}[columns-width=0.5em]
      \Block{5-5}{#1} &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
    \end{bNiceMatrix}
  }

  \begin{sagesilent}
    set_random_seed(0)
    import e1
    m, n = 5, 5
    A = e1.random_incidence_matrix(m, n)
    var('ast')
    n1 = A.right_kernel().basis()[0]
    o1 = vector([1]*m)
    v1 = vector([7,5,7,7,7])
    e = identity_matrix(m)[1]
    b1 = vector([1,3,0,2,0])
    c = 3
    bn = c*b1
    B = matrix.column(SR, [b1, n1, o1, e, v1])
    AB = A*B
    AB = AB.augment(vector([ast]*m))
    B = B.augment(bn)
  \end{sagesilent}

  \newcommand{\ArrowChoice}[2]{
    $\begin{array}{c}
       \begin{tikzpicture}[grph, black]%, node distance=3.75cm]
         \node[state, fill=none] (v1)                                     {$#1$};
         \node[state, fill=none] (v2) [right=of v1]                       {$#2$};

         \draw[->] (v1) edge node {$a_2$} (v2);
       \end{tikzpicture}
     \end{array}$
   }

   \question The equation below depicts the result of multiplying the incidence
   matrix $A$ of a digraph $G$ by another matrix $B$.

   \[
     \BlockA\overset{B}{\sage{B}} = \overset{AB}{\sage{AB}}
   \]

   Note that the entries in the last column of $AB$ are unknown and marked as
   $\ast$.

   \begin{parts}
     \vspace{1em}
     \part[6] The number of nodes in $G$ is \fillin[$\sage{m}$][0.5in], the
     number of arrows in $G$ is \fillin[$\sage{n}$][0.5in], and
     $\chi(G)=\fillin[$\sage{m-n}$][0.5in]$.
     \vspace{1em}
     \part[5] Which of the following is the correct visualization of the second
     arrow $a_2$ in the digraph $G$?

     \begin{oneparcheckboxes}
       \correctchoice \ArrowChoice{v_1}{v_3}
       \choice \ArrowChoice{v_3}{v_1}
       \choice \ArrowChoice{v_2}{v_3}
       \choice \ArrowChoice{v_1}{v_2}
       \choice \ArrowChoice{v_2}{v_1}
     \end{oneparcheckboxes}

     \vspace{1em}
     \part[4] Suppose we make $G$ a weighted digraph by assigning a weight of $5$
     to the second arrow $a_2$ and a weight of $7$ to all of the other
     arrows. Then the net flow through the last node of $G$ is
     {\setlength\answerclearance{1em}\fillin[$\sage{(A*v1)[-1]}$][0.5in]}.

     \vspace{1em}
     \part[4] What is the missing column of $AB$?

     \begin{oneparcheckboxes}
       \choice $\sage{AB.T[0].column()}$
       \choice $\sage{B.T[-1].column()}$
       \correctchoice $\sage{A*bn.column()}$
       \choice $\sage{B.T[0].column()}$
       \choice not enough information to tell
     \end{oneparcheckboxes}

     \vspace{1em}
     \part[5] Which of these statements best articulates the relationship of $A$
     to the terms ``singular'' and ``nonsingular''?

     \begin{oneparcheckboxes}
       \correctchoice $A$ is singular.
       \choice $A$ is nonsingular.
       \choice $A$ is neither singular nor nonsingular.\\[1em]
       \choice $A$ is either singular or nonsingular, but we do not have enough information to decide which.
     \end{oneparcheckboxes}
   \end{parts}

   % \begin{sagesilent}
   %   c1, c2, c5, c7, _ = identity_matrix(5)
   %   c3 = 6*c2
   %   c4 = 5*c1-9*c2
   %   c6 = c2+8*c5
   %   var('ast')
   %   ast_v = vector([ast] * 5)
   %   R_ast = matrix.column([c1, c2, ast_v, ast_v, c5, ast_v, c7])
   %   R = matrix.column([c1, c2, c3, c4, c5, c6, c7])
   % \end{sagesilent}
   % \question[8] There is only one $5\times 7$ matrix $R$ in reduced row echelon
   % form with exactly these three column relations:
   % \begin{align*}
       %        \Col_3 &= 6\cdot\Col_2 & \Col_4 &= 5\cdot\Col_1-9\cdot\Col_2 & \Col_6 &= \Col_2+8\cdot\Col_5
       %      \end{align*}
       %      Find $R$. Clearly eplain your reasoning to receive credit.
       %      \begin{solution}[\stretch{1}]
       %        The equations tell us that the third, fourth, and sixth columns of $R$ are
       %        the \emph{nonpivot} columns. This means that the \emph{pivot} columns are
       %        columns one, two, five, and seven, so $R$ looks like
       %        \[
       %          R = \sage{R_ast}
       %        \]
       %        We can then fill in the missing nonpivot columns by accounting for the
       %        column relations
       %        \[
       %          R = \sage{R}
       %        \]
       %      \end{solution}

   \begin{sagesilent}
     import e1
     set_random_seed(859)
     A1 = e1.random_ref()
     A2 = e1.random_ref(5, 8, r=2)
     A3 = random_matrix(ZZ, 9, 2, algorithm='echelonizable', rank=2)
     A4 = vector([1, 1, 1, 1, 2]).outer_product(vector([1, 4, 3, 2, 9]))
     A5 = e1.random_rref(4, 6, r=3)
     A6 = e1.random_ref(8, 10, r=7)
   \end{sagesilent}
   \question[12] Select every matrix below whose rank is two (each option is
   worth 2pts).

   \begin{oneparcheckboxes}
     \correctchoice $\sage{A2}$
     \choice $\sage{A4}$
     \correctchoice $\sage{A3}$
     \choice $\sage{A5}$
     \choice $\sage{A6}$
     \correctchoice $\sage{A1}$
   \end{oneparcheckboxes}


   \skippage

   \question[12] An $n\times n$ matrix $M$ is called \emph{idempotent} if
   $M^2=M$.

   Suppose that $A=XMX^{-1}$ where $M$ is idempotent. Show that $A$ is
   idempotent. \textbf{You must avoid circular logic to receive credit.}
   \begin{solution}[\stretch{1}]
     We are given $A=XMX^{-1}$ where $M$ is idempotent, which means that
     $M^2=M$. We wish to demonstrate that $A$ is idempotent, which means we want $A^2=A$. This is then accomplished with
     \[
       A^2
       = (XMX^{-1})(XMX^{-1})
       = XMX^{-1}XMX^{-1}
       = XMI_nMX^{-1}
       = XMMX^{-1}
       = XMX^{-1}
       = A
     \]
   \end{solution}

   \begin{sagesilent}
     set_random_seed(4809)
     import e1
     R = e1.random_rref(m=4, n=6, r=4)
     from functools import partial
     e = partial(elementary_matrix, R.nrows())
     E3 = e(row1=1, row2=3, scale=-7)
     E2 = e(row1=2, row2=1, scale=-1)
     E1 = e(row1=1, scale=-1/5)
     A = E1.inverse()*E2.inverse()*E3.inverse()*R
   \end{sagesilent}

   \newcommand{\BlockE}[1][0.5]{
     \begin{bNiceMatrix}[columns-width=#1em]
       \Block{4-4}{E_1} &&&\\[0.125em]
       &&&\\[0.125em]
       &&&\\[0.125em]
       &&&\\[0.125em]
     \end{bNiceMatrix}
   }

   \question The data below depicts the result of representing the Gau\ss-Jordan
   algorithm applied to a matrix $A$ with elementary matrices.
   \[
     \overset{E_3}{\sage{E3}}
     \overset{E_2}{\sage{E2}}
     \BlockE
     \overset{A}{\sage{A}}
     =
     \overset{R}{\sage{R}}
   \]
   Note that the elementary matrix $E_1$ is not explicitly described.
   \begin{parts}
     \vspace{2em}
     \part[4] The \emph{second} elementary row operation used in the algorithm
     was \fillin[${\bv[r]_3-\bv[r]_2\to\bv[r]_3}$][2in]. \textbf{You must use
       the proper notation we learned in class to represent this elementary row
       operation.}
     \part[8] Find $E_1$. Clearly explain your reasoning to receive credit.
     \begin{solution}[\stretch{1}]
       This is the elementary matrix corresponding to the first elementary row
       operation in the Gau\ss-Jordan algorithm when applied to $A$, which is
       $-\frac{1}{5}\cdot\bv[r]_2\to\bv[r]_2$. The elementary matrix we are
       looking for is obtained by applying this operation to the identity
       matrix, which gives
       \[
         \overset{I_4}{\sage{identity_matrix(4)}}\xrightarrow{\sfrac{-1}{5}\cdot\bv[r]_2\to\bv[r]_2}\overset{E_1}{\sage{E1}}
       \]
     \end{solution}

   \end{parts}

   \skippage

   \begin{sagesilent}
     set_random_seed(5)
     import e1
     m, n = 4, 5
     U = e1.random_ref(m, n, pivots=(0,2,4), ub=3)
     P = Permutations(U.nrows()).random_element().to_matrix()
     L = e1.random_upper(m=m, n=m, ub=3).T.inverse()
     var('h')
     b = vector([0, h-1, 0, h+1]).column()
     y = L.inverse()*P*b
   \end{sagesilent}

   \renewcommand{\BlockA}[2][A]{
     \begin{bNiceMatrix}[columns-width=#2em]
       \Block{4-5}{#1} &&&&\\
       &&&&\\
       &&&&\\
       &&&&\\
     \end{bNiceMatrix}
   }

   \question The data below depicts a $PA=LU$ factorization along with the
   inverse marix $L^{-1}$ and a vector $\bv[b]$.
   \begin{align*}
     \overset{P}{\sage{P}}\BlockA{0.5}&=\BlockA[L]{0.25}\overset{U}{\sage{U}} & L^{-1} &= \sage{L.inverse()} & \bv[b] &= \sage{b}
   \end{align*}
   Note that $A$ and $L$ are not explicitly described, and that $\bv[b]$ is
   defined in terms of a constant $h$.
   \begin{parts}
     \vspace{2em}
     \part[4] $\nullity(U^\intercal)=\fillin[$\sage{U.left_nullity()}$][0.5in]$
     \vspace{1em}
     % \part[3] In the system $U\bv[x]=\bv[O]$, which of the following variables
     % are \emph{free}? Select all that apply (no partial credit on this problem).
     % \begin{oneparcheckboxes}
     %   \choice $x_1$
     %   \correctchoice $x_2$
     %   \choice $x_3$
     %   \correctchoice $x_4$
     %   \choice $x_5$
     % \end{oneparcheckboxes}
     % \vspace{1em}
     \part[12] There is only one value of $h$ that makes the system
     $A\bv[x]=\bv[b]$ consistent. Find this value of $h$. Clearly explain your
     reasoning to receive credit.
     \begin{solution}[\stretch{1}]
       In class we demonstrated that the solutions to $A\bv[x]=\bv[b]$ are the
       solutions to $U\bv[x]=\bv[y]$ where $L\bv[y]=P\bv[b]$. Conveniently, we
       are given $L^{-1}$, so we can quickly solve $L\bv[y]=P\bv[b]$ with
       \[
         \bv[y]
         = \overset{L^{-1}}{\sage{L.inverse()}}\overset{P}{\sage{P}}\overset{\bv[b]}{\sage{b}}
         = \overset{L^{-1}}{\sage{L.inverse()}}\overset{P\bv[b]}{\sage{P*b}}
         = \sage{y}
       \]
       Now, in augmented form, the system $U\bv[x]=\bv[y]$ is
       \[
         \sage{U.change_ring(SR).augment(y.T[0], subdivide=True)}
       \]
       The only way to avoid a pivot in the augmented column is to force
       $\sage{y.T[0][-1]}=0$, which only works when
       $h=\sage{y.T[0][-1].roots()[0][0]}$.
     \end{solution}

   \end{parts}
 \end{questions}
\end{document}
