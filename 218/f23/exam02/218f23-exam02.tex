\documentclass[12pt]{exm}

\usetikzlibrary{positioning, tikzmark}

\renewcommand{\examtitle}{Exam II}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2023}{10}{27}{-1}}

\begin{document}
\begin{questions}

  \begin{sagesilent}
    A = matrix(ZZ, [(2, -3, 5, -6), (1, 0, 3, -8), (-1, -3, 8, -6), (-1, 1, 0, 6)])
    l1, l2, l3, l4 = A.eigenvalues()
    chi = lambda l: (l*identity_matrix(A.nrows())-A).change_ring(ZZ)
    v1, = chi(l1).right_kernel().basis()
    v1 = v1.column()
    v2, = chi(l2).right_kernel().basis()
    var('ast')
    v2_ast = matrix.column([v2[0], ast, ast, ast])
    v2 = v2.column()
  \end{sagesilent}

  \question Each of the three equations below provides information about the
  eigenvalues of $A$ (the matrix in the first of the three equations).
  \begin{align*}
    \overset{A}{\sage{A}}\overset{\bv_1}{\sage{v1}} &= \sage{A*v1} & \sage{chi(l2)}\overset{\bv_2}{\sage{v2_ast}} &= \sage{chi(l2)*v2} & \rref\sage{chi(l3)} &= \sage{chi(l3).rref()}
  \end{align*}
  Note that the vector $\bv_2$ from the second equation is missing three entries
  marked $\ast$.

  \vspace{1em}

  \begin{parts}
    \part[4] The vector $\bv_1$ is an eigenvector of $A$ corresponding to the
    eigenvalue $\lambda=\fillin[$\sage{l1}$][0.5in]$.

    \vspace{1em}
    \part[5] Only one of the following statements correctly characterizes the
    vector $\bv_2$. Select this statement.

    \begin{oneparcheckboxes}
      \correctchoice $\bv_2\in\mathcal{E}_A(3)$
      \choice $\bv_2\in\mathcal{E}_A(-3)$
      \choice $\bv_2\in\mathcal{E}_A(2)$
      \choice $\bv_2\in\mathcal{E}_A(-2)$
      \choice $\bv_2\notin\mathcal{E}_A(\lambda)$ for any value of $\lambda$
    \end{oneparcheckboxes}

    \vspace{1em}
    \part[5] The eigenvalue $\lambda=\fillin[$\sage{l3}$][0.5in]$ of $A$
    satisfies $\gm_A(\lambda)=2$.

    \vspace{1em}
    \part[4] Which of the following statements correctly summarizes the data of
    the eigenvalues of $A$?
  \end{parts}
  \begin{checkboxes}
    \choice $A$ has two eigenvalues with geometric multiplicity two.
    \correctchoice $A$ has two eigenvalues with geometric multiplicity one and one eigenvalue with geometric multiplicity two.
    \choice $A$ has one eigenvalue with geometric multiplicity one and two eigenvalues with geometric multiplicity two.
    \choice $A$ has three eigenvalues with geometric multiplicity two.
    \choice $A$ has three eigenvalues with geometric multiplicity one.
  \end{checkboxes}


  \begin{sagesilent}
    import e2
    m, n = 7, 6
    set_random_seed(18)
    A = e2.random_incidence_matrix(m, n)
    set_random_seed(4801)
    X = random_matrix(ZZ, n, n-2, x=0, y=3)
    Y = A*X
    y1,y2,y3,y4 = Y.T
    Y = matrix.column([y1, 0*y2, y3, y4])
  \end{sagesilent}

  \newcommand{\BlockA}[1][A]{
    \begin{bNiceMatrix}[columns-width=0.5em]
      \Block{5-5}{#1} &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
      &&&&\\[0.25em]
    \end{bNiceMatrix}
  }

  \question[10] Suppose that $G$ is a directed graph with exactly one connected
  component and that $A$ is the incidence matrix of $G$.  The equation below
  depicts the result of Brian's attempt to multiply $A$ by a matrix $X$ and
  record the result as $Y$.
  \[
    \BlockA \overset{X}{\sage{X}} = \overset{Y}{\sage{Y}}
  \]
  Despite his best efforts, Brian made a mistake in his calculation! One (and
  only one) of the columns of $Y$ above is incorrect. Identify which column of
  $Y$ was calculated incorrectly. Clearly explain your reasoning to receive
  credit.
  \begin{solution}[\stretch{1}]
    First, note that $A$ must be $\sage{m}\times\sage{n}$ because $X$ is
    $\sage{X.nrows()}\times\sage{X.ncols()}$ and $Y$ is
    $\sage{Y.nrows()}\times\sage{Y.ncols()}$. The Euler characteristic formula
    from class then allows us to infer $h_1(G)$.
    \[
      \begin{tikzpicture}[remember picture]
        \node {\(\subnode{chi}{\color{grn}\chi(G)}=\subnode{h0}{\color{blu}h_0(G)}-\subnode{h1}{\color{red}h_1(G)}\)};

        \draw[<-, thick, grn] (chi.north) |- ++(-3mm, 2.5mm)
        node[left] {$\scriptstyle 7-6=1$};

        \draw[<-, thick, blu] (h0.north) |- ++(-3mm, 2.5mm)
        node[left] {$\scriptstyle 1$};

        \draw[<-, thick, red] (h1.south) |- ++(3mm, -2.5mm)
        node[right] {$\scriptstyle 0$};
      \end{tikzpicture}
    \]
    So, $h_1(G)=0$. But then $\dim\Null(A)=h_1(G)=0$, which means that
    $\Null(A)$ is home only to the zero vector.

    The problem is that the second column of $Y$ is the zero vector, but the
    second column of $X$ is not the zero vector!
  \end{solution}




  \skippage

  \begin{sagesilent}
    import e2
    set_random_seed(8590)
    A1 = matrix.column([(1,2,3,4),(1,2,0,4)])
    A2 = e2.random_ref(6,4,r=4)
    A3 = random_matrix(ZZ, 5, 9, x=1, y=5)
    A4 = vector([1, 2, 1, 2, 1]).outer_product(vector([1, 3, 2]))
    A5 = e2.random_rref(5, 5, r=3)
    A6 = e2.random_ref(8, 10, r=7)
  \end{sagesilent}
  \question[12] Select every matrix below whose columns are linearly independent
  (each option is worth 2pts).

  \begin{oneparcheckboxes}
    \choice $\sage{A5}$
    \correctchoice $\sage{A1}$
    \choice $\sage{A3}$
    \choice $\sage{A4}$ \\
    \choice $\sage{A6}$
    \correctchoice $\sage{A2}$
  \end{oneparcheckboxes}

  \question[12] Suppose that $A$ is a $9\times 7$ matrix and that
  $\bv\in\mathbb{R}^7$ and $\bv[w]\in\mathbb{R}^9$ satisfy the two equations
  \begin{align*}
    A\bv &= 9\cdot\bv[w] & A^\intercal\bv[w] &= 5\cdot\bv
  \end{align*}
  where $\bv\neq\bv[O]$. Show that $\bv$ is an eigenvector of the Gramian of $A$
  and identify the corresponding eigenvalue.
  \begin{solution}
    We want to demonstrate that $A^\intercal A\bv=\lambda\cdot\bv$ for some
    identifiable $\lambda$. To do so, note that
    \[
      A^\intercal A\bv
      = A^\intercal (9\cdot\bv[w])
      = 9\cdot A^\intercal \bv[w]
      = 9\cdot 5\cdot\bv
      = 45\cdot\bv
    \]
    This demonstrates that $\bv$ is an eigenvector of $A^\intercal A$ with
    corresponding eigenvalue $\lambda=45$.
  \end{solution}


  \skippage

  \begin{sagesilent}
    var('h')
    n1, n2 = map(matrix.column, matrix([(1, 2, 0, 1), (2, h, 0, 2)]))
    set_random_seed(89)
    import e2
    l1, l2, l3 = map(matrix.column, e2.random_ref(6,3,r=3).T)
    A = random_matrix(ZZ, 6, 4, algorithm='echelonizable', rank=3)
    b1 = matrix.column([1,0,0,1,0,0])
    b2 = matrix.column([0,1,0,0,1,0])
    b3 = matrix.column([0,0,1,0,0,1])
    b4 = matrix.column([1,1,1,1,0,1])
    correct = matrix.column([0,0,0,1,1,1])
  \end{sagesilent}

  \question Suppose that $A$ is a matrix satisfying the following properties
  \begin{align*}
    \sage{n1},\sage{n2} &\in\Null(A) & \Null(A^\intercal) &= \Span\Set*{\sage{l1},\sage{l2},\sage{l3}}
  \end{align*}
  Note that the second vector in the first equation above is defined in terms of
  a constant labeled $h$.

  \begin{parts}
    \part[10] Fill in every missing label in the picture of the four fundamental
    subspaces of $A$ below, including the dimension of each fundamental
    subspace.
    \[
      \begin{tikzpicture}[draw=black, text=black]

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        % , fill=blubg
        % , minimum height=1.25cm
        % , minimum width=1.5cm
        , minimum height=2.25cm
        , minimum width=2.75cm
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A^\intercal)}$\fi};

        \node (Null) [
        , LeftSpace
        % , fill=grnbg
        , anchor=north east
        , minimum height=2.0cm
        , minimum width=2.75cm
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.right_nullity()}}{\Null(A)}$\fi};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^{\fillin[$\sage{A.ncols()}$][0.5in]}$};

        \draw[LeftSpace] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=9.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , text=blue
          ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A)}$\fi};

          \node(LNull) [
          , RightSpace
          % , fill=redbg
          , anchor=north west
          , minimum height=2.0cm
          , minimum width=2.75cm
          , text=blue
          ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.left_nullity()}}{\Null(A^\intercal)}$\fi};

          \draw[RightSpace] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^{\fillin[$\sage{A.nrows()}$][0.5in]}$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above, overlay] {$A$};

        % \draw[Point, blu] (Col.north) |- ++(3mm, 2.5mm) node[right]
        % {$\scriptstyle\beta=\Set*{\nv[small]{1 1 5}, \nv[small]{4 3 24}}$};

        % \draw[Point, red] (LNull.north) |- ++(2mm, 2.5mm) node[right]
        % {$\scriptstyle\beta=\Set*{\nv[small]{-9 4 1}}$};

      \end{tikzpicture}
    \]
    \part[4] The only valid value of $h$ is $h=\fillin[4][0.5in]$.
    \part[4] Only one of the following formulas for $\bv[b]$ makes the system
    $A\bv[x]=\bv[b]$ consistent. Select this vector.

    \begin{oneparcheckboxes}
      \choice $\bv[b]=\sage{b1}$
      \choice $\bv[b]=\sage{b2}$
      \choice $\bv[b]=\sage{b3}$
      \correctchoice $\bv[b]=\sage{correct}$
      \choice $\bv[b]=\sage{b4}$
    \end{oneparcheckboxes}

    \part[8] Find the projection matrix onto $\Null(A)$.
    \begin{solution}[\stretch{1}]
      The digram tells us that $\dim\Null(A)=1$ and we know
      $\bv_1=\sage{n1.T[0]}\in\Null(A)$, so the one-dimensional projection formula gives
      \[
        P_{\Null(A)}
        = \frac{1}{\norm{\bv_1}^2}\bv_1\bv_1^\intercal
        = \frac{1}{\sage{n1.T[0]*n1.T[0]}}\sage{n1}\sage{n1.T}
        = \frac{1}{\sage{n1.T[0]*n1.T[0]}}\sage{n1*n1.T}
      \]
    \end{solution}

  \end{parts}





  \skippage
  \begin{sagesilent}
    set_random_seed(144)
    A = random_matrix(ZZ, 4, 3)
    P = A*A.pseudoinverse()
    l, = A.left_kernel().basis()
    AA = A.augment(l)
    e = identity_matrix(A.nrows())
    X = random_matrix(ZZ, A.nrows(), A.ncols()).augment(matrix.column([2*e[-3]+e[-2], e[-2]+e[-1]]))
    Y = AA*X
    var('ast')
    A_ast = A.change_ring(SR)
    A_ast[0,0] = A_ast[1,0] = A_ast[1,1] = A_ast[3,1] = ast
    xhat = A.pseudoinverse()*Y.T[-1]
  \end{sagesilent}

  \question The data below depicts a matrix $A$ and the result of multiplying
  the projection matrix onto $\Col(A)$ by another matrix $Y$.
  \begin{align*}
    A &= \sage{A_ast} & \BlockA[P_{\Col(A)}]\overset{Y}{\sage{Y}} &= \sage{P*Y}
  \end{align*}
  Note that $A$ is missing several entries marked $\ast$.
  \begin{parts}
    \part[6] Only one of the columns of $Y$ is in the column space of
    $A$. Select this column.

    \begin{oneparcheckboxes}
      \choice first column
      \choice second column
      \choice third column
      \correctchoice fourth column
      \choice fifth column
    \end{oneparcheckboxes}

    \part[9] Let $\bv[b]=\sage{Y.T[-1]}$ (the last column of $Y$). Find the
    least squares approximate solution $\widehat{\bv[x]}$ to $A\bv[x]=\bv[b]$.
    \begin{solution}[\stretch{1}]
      We have two options for solving the least squares problem
      \begin{align*}
        A\widehat{\bv[x]} &= P\bv[b] & A^\intercal A\widehat{x} &= A^\intercal\bv[b]
      \end{align*}
      The second option is significantly less attractive than the first because
      of all of the unknown entries in $A$. The data gives us $P\bv[b]$, so we
      can at least look at the system $A\widehat{\bv[x]}=P\bv[b]$, which is
      \[
        \overset{A}{\sage{A_ast}}
        \overset{\widehat{\bv[x]}}{\nvc{\widehat{x}_1;\widehat{x}_2;\widehat{x}_3}}
        =
        \overset{P\bv[b]}{\sage{P*Y.T[-1].column()}}
      \]
      We see here that $P\bv[b]$ is the last column of $A$! This means that
      $\widehat{x}=\sage{xhat}$.
    \end{solution}

    \part[7] Let $\bv[b]=\sage{Y.T[1]}$ (the second column of $Y$) and let
    $\widehat{x}$ be the least squares approximate solution to
    $A\bv[x]=\bv[b]$. Find the error $E$ in this approximation.
    \begin{solution}[\stretch{1}]
      This is
      \[
        E
        = \norm{\bv[b]-A\widehat{x}}^2
        = \norm{\bv[b]-P\bv[b]}^2
        = \norm*{\sage{Y.T[1].column()}-\sage{P*Y.T[1].column()}}^2
        = \norm*{\sage{Y.T[1].column()-P*Y.T[1].column()}}^2
        = \sage{(Y.T[1]-P*Y.T[1]).norm()**2}
      \]
    \end{solution}

  \end{parts}

\end{questions}
\end{document}
