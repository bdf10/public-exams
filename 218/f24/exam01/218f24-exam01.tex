\documentclass[12pt]{exm}

\usepackage{btrmt}
\usepackage{bm}
\usetikzlibrary{calc, angles, quotes}

\renewcommand{\examtitle}{Exam I}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2024}{9}{27}{-1}}

\begin{document}
\begin{questions}

  \newcommand{\MyACols}{\btrmtCol[pad=4em, cols={\ba_1, \ba_2, \ba_3, \ba_4}]}

  \newcommand{\MyAFig}{
    \begin{array}{c}
      \begin{tikzpicture}[
        , line join=round
        , line cap=round
        , very thick
        , rotate=-10
        , transform shape
        ]

        \tikzset{dot/.style={
            , circle
            , minimum size=5pt
            , inner sep=0
            , fill=black
          }}

        \pgfmathsetmacro{\r}{1.75}
        \pgfmathsetmacro{\c}{\r*(sqrt(3)+3)/2}

        \node[dot] (O) at (0,0)  {};
        \node[dot] (P) at (\r,0) {};
        \node[dot] (Q) at (0,\r) {};
        \node[dot] (R) at (\c,\c) {};

        \pic[
        , draw
        , thick
        , <->
        , angle radius=15mm
        , angle eccentricity=1.30
        , "$\sfrac{\pi}{4}$"
        ] {angle = Q--R--P};

        \pic[
        , draw
        ] {right angle = Q--O--P};

        \draw[->] (O) -- (P) node[midway, below]  {$\bm{a}_1$};
        \draw[->] (O) -- (Q) node[midway, left] {$\bm{a}_2$};
        \draw[->] (R) -- (P) node[midway, below, sloped] {$\bm{a}_3$};
        \draw[->] (R) -- (Q) node[midway, above, sloped] {$\bm{a}_4$};
      \end{tikzpicture}
    \end{array}
  }

  \question The matrix $A$ below is $5\times 4$ whose columns are labeled as
  $\ba_1$, $\ba_2$, $\ba_3$, and $\ba_4$. The figure below depicts a geometric
  visualization of the columns of $A$.
  \begin{align*}
    A &= \MyACols & \MyAFig
  \end{align*}
  Note that the vectors $\ba_1$ and $\ba_2$ form a right angle and that $\ba_3$
  and $\ba_4$ form an angle of $\sfrac{\pi}{4}$. Additionally, it is known that
  $\ba_1$ and $\ba_2$ are unit vectors and that
  $\norm{\ba_3}=\norm{\ba_4}=\dfrac{\sqrt{3}+3}{2}$.

  \vspace{2em}

  \begin{parts}
    \part[2] Each of $\ba_1$, $\ba_2$, $\ba_3$, and $\ba_4$ is a vector in
    $\mathbb{R}^\ast$ where $\ast=\fillin[$5$][0.5in]$.

    \vspace{2em}

    \part[2] The expression $A\bv$ is a vector with $\fillin[$5$][0.5in]$
    coordinates, provided that the vector $\bv$ has $\fillin[$4$][0.5in]$
    coordinates.

    \vspace{2em}

    \part[5] Only one of the following equations is correct. Select this
    equation.

    \newcommand{\MyBlockA}{\btrmtBlock[width=0.75em, height=1.25em]{A}}
    \newcommand{\MyZero}{\nvc{0; 0; 0; 0; 0}}
    \begin{oneparcheckboxes}
      \choice $\MyBlockA\nvc{1; 1; 1; 1}=\MyZero$
      \choice $\MyBlockA\nvc{1; 0; 0; 0}=\MyZero$
      \correctchoice $\MyBlockA\nvc{1; -1; -1; 1}=\MyZero$
      \choice $\MyBlockA\nvc{1; -1; 1; -1}=\MyZero$
    \end{oneparcheckboxes}

    \part[5] Only one of the following scalar quantities is
    \emph{negative}. Select this quantity.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\inner{\ba_1, \ba_2}$
      \choice $\ba_2^\intercal\ba_1$
      \correctchoice $\ba_4^\intercal(-\ba_1+\ba_2-\ba_4)$
      \choice The $(3, 4)$ entry of $A^\intercal A$.
    \end{oneparcheckboxes}
    \begin{solution}[\stretch{1}]
      The first two are $\inner{\ba_1, \ba_2}=0$ and
      $\ba_2^\intercal\ba_1=\inner{\ba_2, \ba_1}=0$ because $\ba_1$ and $\ba_2$
      are orthogonal. The $(3, 4)$ entry of $A^\intercal A$ is
      $\inner{\ba_3, \ba_4}>0$ because $\ba_3$ and $\ba_4$ are \emph{acute}. The
      third is
      $\ba_4^\intercal(-\ba_1+\ba_2-\ba_4)=\inner{\ba_4, -\ba_3}=-\inner{\ba_4,
        \ba_3}<0$.
    \end{solution}

    \part[8] Fill-in the blanks in this equation with valid integers:
    $\trace(A^\intercal
    A)=\fillin[$8$][0.5in]+\fillin[$3$][0.5in]\cdot\sqrt{3}$. Show your work
    below to receive credit.
    \begin{solution}[\stretch{4}]
      The trace of $A^\intercal A$ is the sum of the diagonal entries of
      $A^\intercal A$. We know from class that
      \[
        A^\intercal A
        =
        \begin{bNiceMatrix}[c]
          \inner{\ba_1, \ba_1} & \inner{\ba_1, \ba_2} & \inner{\ba_1, \ba_3} & \inner{\ba_1, \ba_4} \\
          \inner{\ba_2, \ba_1} & \inner{\ba_2, \ba_2} & \inner{\ba_2, \ba_3} & \inner{\ba_2, \ba_4} \\
          \inner{\ba_3, \ba_1} & \inner{\ba_3, \ba_2} & \inner{\ba_3, \ba_3} & \inner{\ba_3, \ba_4} \\
          \inner{\ba_4, \ba_1} & \inner{\ba_4, \ba_2} & \inner{\ba_4, \ba_3} & \inner{\ba_4, \ba_4} \\
        \end{bNiceMatrix}
        =
        \begin{bNiceMatrix}[c]
          \norm{\ba_1}^2  & \ast           & \ast           & \ast \\
          \ast            & \norm{\ba_2}^2 & \ast           & \ast \\
          \ast            & \ast           & \norm{\ba_3}^2 & \ast \\
          \ast            & \ast           & \ast           & \norm{\ba_4}^2 \\
        \end{bNiceMatrix}
      \]
      All of the entries marked $\ast$ are irrelevant to our calculation (even
      though we could quickly infer the $(1,2)$, $(2, 1)$, $(3, 4)$, and
      $(4, 3)$ entries). Our desired quantity is then
      \begin{align*}
        \trace(A^\intercal A)
        &= 1^2 + 1^2 + \left(\frac{\sqrt{3}+3}{2}\right)^2 + \left(\frac{\sqrt{3}+3}{2}\right)^2 \\
        &= 2 + \frac{2}{2^2}(\sqrt{3}+3)^2 \\
        &= 2 + \frac{1}{2}((\sqrt{3})^2+3^2+2\cdot3\cdot\sqrt{3}) \\
        &= 2+\frac{1}{2}(3+9+6\,\sqrt{3}) \\
        &= 8+3\cdot\sqrt{3}
      \end{align*}
    \end{solution}

  \end{parts}



  \newpage

  % a rich environment for aking questions about singular/nonsingular matrices
  % tell them the ranks
  % have them calculate ABC
  \begin{sagesilent}
    A = matrix(ZZ, [[-388, 119, -438, 414, 14], [291, -53, 235, -152, -359], [-213, 712, -365, 725, 300]])
    B = matrix(ZZ, [[423, 423, 423, 423, 423], [417, 417, 416, 416, 416], [432, 431, 431, 431, 431], [449, 449, 449, 448, 448], [379, 379, 379, 379, 378]])
    C = matrix(ZZ, [[1, 0, 0, 0], [-1, 1, 0, 0], [0, -1, 1, 0], [0, 0, -1, 1], [0, 0, 0, -1]])
    M = A*B*C
  \end{sagesilent}

  \question Each of the matrices $A$, $B$, and $C$ below is a ``full rank''
  matrix.
  \begin{align*}
    A &= \sage{A} & B &= \sage{B} & C &= \sage{C}
  \end{align*}
  Note that the matrix $C$ is the incidence of a directed graph $G$.

  \vspace{2em}

  \begin{parts}
    \part[3] $\rank(A)=\fillin[$\sage{A.rank()}$][0.5in]$, $\rank(B)=\fillin[$\sage{B.rank()}$][0.5in]$, and $\rank(C)=\fillin[$\sage{C.rank()}$][0.5in]$

    \vspace{2em}

    \part[6] $\nullity(A)=\fillin[$\sage{A.right_nullity()}$][0.5in]$, $\nullity(A^\intercal)=\fillin[$\sage{A.T.right_nullity()}$][0.5in]$, and $\nullity(A^\intercal A)=\fillin[$\sage{(A.T*A).right_nullity()}$][0.5in]$

    \vspace{2em}

    \part[9] Which (if any) of the following matrices is \emph{invertible}?
    Select all that apply (1.5pts each).

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $A$
      \correctchoice $B$
      \choice $C$
      \choice $A^\intercal A$
      \correctchoice $B^\intercal B$
      \correctchoice $C^\intercal C$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[6] The number of connected components of $G$ is \fillin[$1$][0.5in],
    the circuit rank of $G$ is \fillin[$0$][0.5in], and
    $\chi(G)=\fillin[$\sage{C.nrows()-C.ncols()}$][0.5in]$.
    \begin{solution}[\stretch{1}]
      The picture here is
      \begin{tikzpicture}[grph, baseline]
        \tikzset{state/.append style={text=black}}
        \tikzset{edge/.style={->, black}}
        \node[state] (v1) [] {$v_1$};
        \node[state,right=of v1] (v2) {$v_2$};
        \node[state,right=of v2] (v3) {$v_3$};
        \node[state,right=of v3] (v4) {$v_4$};
        \node[state,right=of v4] (v5) {$v_5$};

        \draw[edge] (v1) edge node{$a_1$} (v2);
        \draw[edge] (v2) edge node{$a_2$} (v3);
        \draw[edge] (v3) edge node{$a_3$} (v4);
        \draw[edge] (v4) edge node{$a_4$} (v5);
      \end{tikzpicture}
    \end{solution}
    \part[10] Calculate the matrix $M=ABC$. This can be done quickly without
    complicated arithmetic, so no partial credit will be awarded for arithmetic
    errors. You can ensure an award of partial credit by correctly conveying the
    size of $M$ and correctly aritculating any properties of matrix arithmetic
    that help calculate $M$.
    \begin{solution}[\stretch{4}]
      First, $A$ is $\sage{A.nrows()}\times\sage{A.ncols()}$, $B$ is
      $\sage{B.nrows()}\times\sage{B.ncols()}$, and $C$ is
      $\sage{C.nrows()}\times\sage{C.ncols()}$. This means that $M=ABC$ is a
      valid expression and expected to be a
      $\sage{M.nrows()}\times\sage{M.ncols()}$ matrix.

      Of course, we cannot help but notice that the numbers in $A$ and $B$ are
      terrible. However, the numbers in $C$ are not so bad. In fact, we also
      notice that the numbers in $B$ are ``close together''.

      The \emph{associative property} of matrix multiplication asserts that
      $M=(AB)C=A(BC)$. It looks clear here that the calculation $M=A(BC)$ is
      preferred over $M=(AB)C$. This gives
      \begin{align*}
        M
        &= \btrmtBlock{A}\left(\overset{B}{\sage{B}}\overset{C}{\sage{C}}\right) \\
        &= \overset{A}{\sage{A}}\sage{B*C} \\
        &= \sage{M}
      \end{align*}
      The key to this problem is that $BC$ turns out to have a single $1$ in
      every column and the rest of its entries are zero. Calculating $M=A(BC)$
      is then selects columns three, two, four and five of $A$ respectively.
    \end{solution}

  \end{parts}


  \newpage

  \begin{sagesilent}
    A = matrix(ZZ, [[1, 4, -1, -2, -9], [-1, -4, 1, 2, 9], [2, 8, 0, 6, 4], [-1, -4, -1, -8, -13], [0, 0, 1, 5, 11]])
    E = matrix(ZZ, [[8, 1, -3, 0, 7], [7, 1, -4, -2, 5], [1, 0, 0, 1, 2], [0, 1, 0, -1, -2], [0, 0, 1, 2, 2]])
    R = A.rref()
    b = vector(ZZ, [0, 0, -2, 2, -1])
    b_wrong1 = vector(ZZ, [1, 2, -3, 0, 0])
    b_wrong2 = vector(ZZ, [1, 1, 0, 0, 0])
    b_wrong3 = vector(ZZ, [0, 1, -2, 1, 0])
  \end{sagesilent}

  \question Consider the following $EA=R$ factorization
  \begin{align*}
    \overset{E}{\sage{E}}\overset{A}{\sage{A}} &= \overset{R}{\sage{R}}
  \end{align*}

  \vspace{1em}

  \begin{parts}
    \part[6] $\rank(E)=\fillin[$\sage{E.rank()}$][0.5in]$, $\rank(A)=\fillin[$\sage{A.rank()}$][0.5in]$, and $\rank(R)=\fillin[$\sage{R.rank()}$][0.5in]$.

    \vspace{2em}

    \part[4] The columns of $A$ satisfy all of the following equations. However,
    according to our terminology from class, only one of the following equations
    is called a \emph{column relation}. Select this equation.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\Col_5 = -\Col_1+6\,\Col_3+\Col_4$
      \correctchoice $\Col_4 = 3\,\Col_1+5\,\Col_3$
      \choice $\Col_4 = 7\,\Col_1-\Col_2+5\,\Col_3$\\[2em]
      \choice $\Col_4 = -\Col_1+\Col_2+5\,\Col_3$
      \choice $\Col_5 = 3\,\Col_1-\Col_2+6\,\Col_3+\Col_4$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[4] Only one of the following formulas for $\bb$ makes the system
    $A\bx=\bb$ consistent. Select this formula.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\bb=\sage{b_wrong1}$
      \choice $\bb=\sage{b_wrong2}$\\[2em]
      \choice $\bb=\sage{b_wrong3}$
      \correctchoice $\bb=\sage{b}$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[10] If we use the Gau\ss-Jordan algorithm as articulated in class to
    define $E$ as the product of elementary matrices, then it would take
    \emph{eight} elementary matrices to do so $E=E_8E_7E_6E_5E_4E_3E_2E_1$. Find
    the elementary row operation that would define $E_4$ in this context and
    write this operation with proper notation in this blank:
    $\raisebox{-1em}{\fillin[$\br_2\leftrightarrow \br_3$][2.00in]}$
    . \textbf{\emph{Clearly label your row operations to receive full credit.}}
    \begin{solution}[\stretch{1}]
      We need to follow the Gau\ss-Jordan algorithm to row-reduce $A$, but we
      can stop after four operations. Here, we have
      \begin{sagesilent}
        from functools import partial
        e = partial(elementary_matrix, A.nrows())
        E1 = e(row1=1, row2=0, scale=1)
        E2 = e(row1=2, row2=0, scale=-2)
        E3 = e(row1=3, row2=0, scale=1)
        A1 = E3*E2*E1*A
        E4 = e(row1=1, row2=2)
        A2 = E4*A1
      \end{sagesilent}
      \newcommand{\MySteps}{
        \begin{NiceArray}{rcrcr}
          \br_2 &+&    \br_1 &\to& \br_2 \\
          \br_3 &-& 2\,\br_1 &\to& \br_3 \\
          \br_4 &+&    \br_1 &\to& \br_4 \\
        \end{NiceArray}
      }
      \begin{align*}
        \overset{A}{\sage{A}}
        \xrightarrow{\MySteps}\sage{A1}
        \xrightarrow{\br_2\leftrightarrow\br_3}\sage{A2}
      \end{align*}
      The fourth elementary row operation is $\br_2\leftrightarrow\br_3$.
    \end{solution}

    
  \end{parts}


  \newpage

  \question[10] Let $A$ be a nonzero $2024\times 2024$ matrix satisfying $A^2=A$
  and let $M=I_{2024}+A$. There is only one scalar value of $c$ for which
  $M^{-1}=I_{2024}+c\cdot A$. Find this value of $c$ and record your answer in
  this blank:
  $c=\raisebox{-1em}{\fillin[$-\sfrac{1}{2}$][0.75in]}$. \emph{\textbf{You must
      clearly explain your work and avoid circular reasoning to receive
      credit.}}

  \begin{solution}[\stretch{1}]
    The purpose of $M^{-1}$ is that it is the only matrix satisfying
    $MM^{-1}=I_{2024}$. So, we want
    \[
      I_{2024}
      = (I+A)(I+c\cdot A)
      = I+c\cdot A+A+c\cdot A^2
      = I+c\cdot A+A+c\cdot A
      = I+(2\,c+1)\cdot A
    \]
    The only way to get this to work is if $2\,c+1=0$, or $c=-\sfrac{1}{2}$.
  \end{solution}


  \begin{sagesilent}
    P = matrix(ZZ, 5, 5, {(0,0):1, (1,3):1, (2,1):1, (3,4):1, (4,2):1})
    A = matrix(ZZ, [[6, -12, 6, 0, -24], [4, -8, 10, 6, 8], [18, -36, 23, -2, -45], [8, -16, 13, 10, -17], [14, -28, 18, -1, -35]])
    L = matrix(ZZ, [[3, 0, 0, 0], [4, 5, 0, 0], [2, 6, 2, 0], [7, 4, 3, 2], [9, 5, 4, 1]])
    U = matrix(ZZ, [[2, -4, 2, 0, -8], [0, 0, 1, 2, 3], [0, 0, 0, -3, 3], [0, 0, 0, 0, 0]])
    b = -vector(ZZ, [-6, -10, -9, -23, -8])
    y = L.pseudoinverse()*P*b
    M = L.augment(P*b, subdivide=True)
  \end{sagesilent}
  \question[10] Consider $P$, $L$, and $U$ below along with the calculation of
  $\rref[L\mid P\bb]$ for $\bb=\sage{b}$.
  \begin{align*}
    P &= \sage{P} & L &= \sage{L} & U &= \sage{U} & \rref\sage{M} &= \sage{M.rref()}
  \end{align*}
  Let $A$ be the matrix satisfying $PA=LU$. The system $A\bx=\bb$ has infinitely
  many solutions. Find the solution $\bx$ obtained by setting every free
  variable in this system equal to one. Clearly explain your reasoning to
  receive credit.
  \begin{solution}[\stretch{3}]
    Given $PA=LU$, the system $A\bx=\bb$ is solved by first solving $L\by=P\bb$
    for $\by$ and then solving $U\bx=\by$ for $\bx$. We are given
    $\rref[L\mid P\bb]$, which automatically tells us $\by=\sage{y}$ in
    $L\by=P\bb$.

    Now, we turn our attention to the system $U\bx=\by$. There are five
    variables in this system $x_1,x_2,x_3,x_4,x_5$. The pivots in $U$ are in
    columns one, three, and four. This means that the dependent variables are
    $x_1,x_3,x_4$ and the free variables are $x_2,x_5$.

    Normally, we would solve our system by relabeling the free variables as
    $x_2=c_1$ and $x_5=c_2$. The problem instructs us, however, to use
    $x_2=x_5=1$. The system $U\bx=\by$ is then
    \[
      \begin{NiceArray}{rcrcrcrcrcrcrcr}
        2\,x_1 &-& 4\,(1) &+& 2\,x_3 & &        &-& 8\,(1) &=& \sage{y[0]} &\to& x_1 &=&  13\\
        {}     & &        & &    x_3 &+& 2\,x_4 &+& 3\,(1) &=& \sage{y[1]} &\to& x_3 &=&  -6\\
        {}     & &        & &        &-& 3\,x_4 &+& 3\,(1) &=& \sage{y[2]} &\to& x_4 &=&   3\\
        {}     & &        & &        & &        & &      0 &=& \sage{y[3]} \\
      \end{NiceArray}
    \]
    The back-substution gives our desired solution as $\bx=\nv{13 1 -6 3 1}$.
  \end{solution}


  % \newpage
  % \begin{sagesilent}
  %   C = matrix(ZZ, [[152, 29, 72], [59, 76, 40], [133, 36, 131], [68, 97, 188], [71, 128, 64]])
  %   R = matrix(ZZ, [[1, 1, 0, 0, 0, 1], [0, 0, 1, 0, 1, 0], [0, 0, 0, 1, 0, 1]])
  %   A = C*R
  % \end{sagesilent}
  % \question[9] Every entry in the reduced row echelon form of the matrix $A$
  % depicted below is either $0$ or $1$.
  % \begin{align*}
  %   A &= \sage{A} & \rref(A) &= \sage{A.rref()}
  % \end{align*}
  % Use this information to fill-in the missing entries in $\rref(A)$
  % \emph{\textbf{without doing row reductions}} (1.5pts per column).

\end{questions}
\end{document}
