;; -*- lexical-binding: t; -*-

(TeX-add-style-hook
 "218f24-exam01-solutions"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exm" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("btrmt" "") ("bm" "")))
   (TeX-run-style-hooks
    "latex2e"
    "exm"
    "exm12"
    "btrmt"
    "bm")
   (TeX-add-symbols
    "MyACols"
    "MyAFig"
    "MyBlockA"
    "MyZero"
    "MySteps"))
 :latex)

