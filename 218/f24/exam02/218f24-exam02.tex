\documentclass[12pt]{exm}

\renewcommand{\examtitle}{Exam II}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2024}{10}{25}{-1}}

\begin{document}
\begin{questions}

  \begin{sagesilent}
    m, n = 31, 17
  \end{sagesilent}

  \question Suppose that $A\bx=\bb$ is a \emph{consistent system} where $A$ is
  $\sage{m}\times \sage{n}$ and that $\by$ and $\bz$ are solutions to this
  system where $\by\neq\bz$ (this means that $\by$ and $\bz$ are vectors
  satisfying $A\by=\bb$ and $A\bz=\bb$).

  \vspace{2em}

  \begin{parts}
    \part[3] The vector $\bb$ has $\fillin[$\sage{m}$][0.5in]$ coordinates and
    the vectors $\by$ and $\bz$ have $\fillin[$\sage{n}$][0.5in]$ coordinates.

    \vspace{1em}

    \part[4] Only one of the following statements is \emph{guaranteed} to be
    correct. Select this statement.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\bb$ belongs to the row space of $A$
      \choice $\bb$ belongs to the null space of $A$ \\[1.5em]
      \correctchoice $\bb$ belongs to the  column space of $A$
      \choice $\bb$ belongs to the left null space of $A$ \\[1.5em]
      \choice we do not have enough information to determine if $\bb$ belongs to one of the four fundamental subspaces of $A$
    \end{oneparcheckboxes}

    \vspace{2em}

    \newcommand{\atay}{A^\intercal A\by}
    \part[4] Only one of the following statements is \emph{guaranteed} to
    accurately characterize $\atay$. Select this statement.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice $\atay$ is a vector in the row space of $A$
      \choice $\atay$ is a vector in the null space of $A$ \\[1.5em]
      \choice $\atay$ is a vector in the column space of $A$
      \choice $\atay$ is a vector in the left null space of $A$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[8] Prove that $\by-\bz\in\Null(A^\intercal A)$. Use the space outside
    the box below to brainstorm your thoughts, but your proof must be neatly and
    succinctly presented in the box below. \textbf{You must avoid circular
      reasoning to receive credit.}

    \begin{solution}[\stretch{1}]
      We know that $A\by=\bb$ and that $A\bz=\bb$. We wish to prove that
      $\by-\bz\in\Null(A^\intercal A)$, which is the same as demonstrating that
      $A^\intercal A(\by-\bz)=\bO$. The proof then succinctly fits into our box
      below:
    \end{solution}

    \[
      \begin{tikzpicture}[line join=round, line cap=round, very thick]

        \node[draw, rectangle, minimum width=6in, minimum height=1in, text=blue, align=center] {
          \ifprintanswers\(A^\intercal A(\by-\bz) = A^\intercal A\by - A^\intercal A\bz = A^\intercal\bb - A^\intercal\bb = \bO\)\fi
        };

      \end{tikzpicture}
    \]

    \part[9] The facts that $\by\neq\bz$ and that
    $\by-\bz\in\Null(A^\intercal A)$ imply that some, but not all, of the
    following statements are \emph{guaranteed} to be true. Select the true
    statements (1.5pts each).

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice $\dim\Null(A^\intercal A)>0$
      \choice $\dim\Null(A^\intercal A)=1$
      \choice $A^\intercal A$ has independent columns \\[1.5em]
      \correctchoice $A^\intercal A$ is singular
      \choice $\lambda=0$ is \emph{not} and eigenvalue of $A$
      \correctchoice $\by-\bz$ is an eigenvector of $A^\intercal A$% \\[1.5em]
      % \choice $\by-\bz\perp\Col(AA^\intercal)$
      % \correctchoice $\by-\bz\perp\Col(A^\intercal A)$
    \end{oneparcheckboxes}
    \begin{solution}[\stretch{1}]
      The fact that $\by\neq\bz$ means that $\by-\bz\neq\bO$. The fact that
      $\by-\bz\in\Null(A^\intercal A)$ then tells us that $\Null(A^\intercal A)$
      contains a vector other than the zero vector. From this we conclude that
      $\dim\Null(A^\intercal A)>0$ but we don't have enough information to say
      that $\dim\Null(A^\intercal A)=1$. Since
      $\nullity(A^\intercal A)=\dim\Null(A^\intercal A)>0$, we further conclude
      that $A^\intercal A$ has \emph{dependent} columns and is therefore
      \emph{singular} (so $\lambda=0$ \emph{is} and eigenvalue of
      $A^\intercal A$). Since $A^\intercal A(\by-\bz)=\bO=0\cdot(\by-\bz)$, we
      know that $\by-\bz$ \emph{is} an eigenvector of $A^\intercal A$ (with
      eigenvalue zero).

      % Since $\by-\bz\in\Null(A^\intercal A)$, we know
      % $\by-\bz\perp\Col((A^\intercal A)^\intercal)=\Col(A^\intercal A)$
      % (\emph{not} $\Col(AA^\intercal)$).
    \end{solution}



  \end{parts}


  \newpage

  \begin{sagesilent}
    A = matrix(ZZ, [[22, 122, -585, 54, 2079, -454, 1773], [88, 488, -2340, 216, 8316, -1816, 7092], [-91, -516, 2465, -233, -8771, 1894, -7473], [208, 1278, -6074, 594, 21700, -4630, 18437], [279, 1563, -7477, 697, 26591, -5774, 22666], [-2700, -15611, 74504, -7073, -265383, 57245, -225953]])
    m, n = A.dimensions()
    r = A.rank()
  \end{sagesilent}

  \question Suppose $A$ satisfies $\rref(A)=\sage{A.rref()}$ and
  $\rref(A^\intercal)=\sage{A.T.rref()}$.

  \begin{parts}
    \part[10] Fill in every missing label in the picture of the four fundamental
    subspaces of $A$ below, including the dimension of each fundamental
    subspace.
    \[
      \begin{tikzpicture}[draw=black, text=black]

        \pgfmathsetmacro{\PerpLength}{0.25}
        \pgfmathsetmacro{\SpaceRotate}{20}
        \tikzstyle{Space} = [
        , draw
        , ultra thick
        , outer sep=0pt
        , minimum height=3.00cm
        , minimum width=3.5cm
        ]
        \tikzstyle{LeftSpace} = [
        , Space
        , rotate=\SpaceRotate
        ]
        \tikzstyle{RightSpace} = [
        , Space
        , rotate=-\SpaceRotate
        ]
        \tikzstyle{TopSpace} = [
        ]
        \tikzstyle{Point} = [
        , <-
        , thick
        , overlay
        , shorten <=0.5mm
        ]

        \node (Row) [
        , LeftSpace
        , TopSpace
        , anchor=south west
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A^\intercal)}$\fi};

        \node (Null) [
        , LeftSpace
        , anchor=north east
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.right_nullity()}}{\Null(A)}$\fi};
        \node[right] (Rn) at (Row.east) {$\mathbb{R}^{\fillin[$\sage{A.ncols()}$][0.5in]}$};

        \draw[LeftSpace] (0, 0) rectangle (-\PerpLength,\PerpLength);

        \begin{scope}[xshift=10.5cm]
          \node(Col) [
          , RightSpace
          , TopSpace
          , anchor=south east
          , text=blue
          ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A)}$\fi};

          \node(LNull) [
          , RightSpace
          , anchor=north west
          , text=blue
          ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.left_nullity()}}{\Null(A^\intercal)}$\fi};

          \draw[RightSpace] (0, 0) rectangle (\PerpLength,\PerpLength);

          \node[left] (Rm) at (Col.west) {$\mathbb{R}^{\fillin[$\sage{A.nrows()}$][0.5in]}$};
        \end{scope}

        \draw[->, thick] (Rn.east) -- (Rm.west)
        node[midway, above, overlay] {$A$};

      \end{tikzpicture}
    \]
    \part[3] Which of the following is the most accurate geometric description
    of the \emph{left null space} of $A$?

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice a plane in $\mathbb{R}^{\sage{m}}$
      \choice a point with six coordinates
      \choice a line in $\mathbb{R}^{\sage{n}}$
      \correctchoice a line in $\mathbb{R}^{\sage{m}}$
      \choice a plane in $\mathbb{R}^{\sage{n}}$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[4] Only one of the following statements accurately describes the
    columns of $A$. Select this statement.
    
    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $A$ has independent columns
      \choice every choice of five columns of $A$ will be independent \\[1.5em]
      \correctchoice it is impossible to find six independent vectors among the columns of $A$ \\[1.5em]
      \choice the first four columns of $A$ form a basis of $\Col(A)$
    \end{oneparcheckboxes}

    \vspace{2em}

    \newcommand{\raisefill}[1]{\raisebox{-1em}{\fillin[#1][0.5in]}}
    \part[4] The projection matrix $P$ onto the row space of $A$ is
    $P=X(X^\intercal X)^{-1}X^\intercal$ where $X$ is
    $\raisefill{$\sage{n}$}\times\raisefill{$\sage{r}$}$.

    \part[10] Let $V$ be the vector space spanned by the rows of
    $\rref(A^\intercal)$. Find a basis of $V^\perp$. Clearly explain your work
    to receive credit.
    \begin{solution}[\stretch{1}]
      From class we know that the nonzero rows of $\rref(A^\intercal)$ form a
      basis of $\Col(A)$. Including the rows of zeros doesn't change the span,
      so $V=\Col(A)$, which means we want to find a basis of
      $V^\perp=\Null(A^\intercal)$. The picture tells us that
      $\dim\Null(A^\intercal)=1$, so we need only find one nonzero solution to
      $A^\intercal\bx=\bO$. The given $\rref(A^\intercal)$ tells us that the
      equations defining $A^\intercal\bx=\bO$ are
      \begin{align*}
        x_1+4\,x_2 &= 0 & x_3 &= 0 & x_4 &= 0 & x_5 &= 0 & x_6 &= 0
      \end{align*}
      The free variable is $x_2=c_1$ so the general solution is
      \[
        \nv{x_1 x_2 x_3 x_4 x_5 x_6}
        = \nv{-4\,c_1 c_1 0 0 0 0}
        = c_1\cdot\nv{-4 1 0 0 0 0}
      \]
      This tells us that any nonzero multiple of $\nv{-4 1 0 0 0 0}$ is a valid
      basis of $V^\perp$.
    \end{solution}

  \end{parts}


  \newpage

  \begin{sagesilent}
    A = matrix(ZZ, [[5, -5, 0, -5], [25, 0, 25, -5], [-5, 5, 0, 5], [-25, 0, -25, 5]])
    n = A.nrows()
    l = 5
    chi = lambda x: x*identity_matrix(n)-A
    B = matrix.column([(1, 0, -1, 0), (0, 1, 0, -1)])
    b = vector([4, 0, 6, 2])
  \end{sagesilent}
  \question One of the eigenvalues of $A=\sage{A}$ is $\lambda=\sage{l}$.
  \begin{parts}
    \part[10] Find a basis of $\mathcal{E}_A(\sage{l})$. Clearly explain your
    work to receive credit.
    \begin{solution}[\stretch{1}]
      By definition, we want a basis of
      is
      \[
        \mathcal{E}_A(\sage{l})
        = \Null\overset{\sage{l}\cdot I_{\sage{n}}-A}{\sage{chi(l)}}
        = \Null\overset{\rref(\sage{l}\cdot I_{\sage{n}}-A)}{\sage{chi(l).rref()}}
        = \Span\Set*{\nvc{1; 0; -1; 0}, \nvc{0; 1; 0; -1}}
      \]
      The calculation of $\rref(\sage{l}\cdot I_{\sage{n}}-A)$ is done where
      without row-reductions by observing the column relations $\Col_3=\Col_1$
      and $\Col_4=\Col_2$.
    \end{solution}

    \part[9] Let $P$ be the projection matrix onto $\mathcal{E}_A(5)$ and
    suppose that $\bv\in\mathcal{E}_A(5)$. Some, but not all, of the following
    statements are correct. Select each correct statement (1.5pts each).

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice $A\bv=5\cdot\bv$
      \choice $P\bv=5\cdot\bv$
      \choice $A\bv=\bO$
      \correctchoice $P\bv=\bv$
      \correctchoice $\trace(P)=\gm_A(5)$
      \choice $\trace(P)=5$
    \end{oneparcheckboxes}

    \vspace{2em}

    \newcommand{\raisefill}[1]{\raisebox{-1em}{\fillin[#1][0.5in]}}
    \part[10] Let $\bb=\sage{b}$. If we assemble the basis vectors found for
    $\mathcal{E}_A(5)$ in part (\emph{a}) of this problem into the columns of a
    matrix $B$, then we would find that
    $B(B^\intercal B)^{-1} B^\intercal\bb=\sage{B*B.pseudoinverse()*b}$. Use
    this information to calculate the error in the least squares problem
    associated to the system $B\bx=\bb$ and fill in the blank:
    $E=\raisefill{$\sage{(b-B*B.pseudoinverse()*b).norm()**2}$}$. Clearly
    explain your work to receive credit.

    \begin{solution}[\stretch{1}]
      The error in the least squares problem assiciated to $B\bx=\bb$ is the
      square length of the difference between $\bb$ and the projection of $\bb$
      to $\Col(b)$. We are given $\bb=\sage{b}$ and the projection of $\bb$ to
      $\Col(B)$ is $\sage{B*B.pseudoinverse()*b}$, so our error is
      \[
        E
        = \norm*{\sage{b.column()}-\sage{B*B.pseudoinverse()*b.column()}}^2
        = \norm*{\sage{b.column()-B*B.pseudoinverse()*b.column()}}^2
        = \sage{(b-B*B.pseudoinverse()*b).norm()**2}
      \]
    \end{solution}


  \end{parts}


  \newpage

  % another good data set
  % A = matrix(ZZ, [[1, -1], [1, 3], [1, 1]])
  % b = vector(ZZ, [6, 2, 1])

  \begin{sagesilent}
    var('y0 t')
    pts = [(-3, y0), (-2, -1), (-4, 1)]
    p1, p2, p3 = pts
    A = matrix.column([[1]*len(pts), [pt[0] for pt in pts]])
    bs = vector([pt[1] for pt in pts])
    b = bs(y0=3).change_ring(ZZ)
    xhat = A.pseudoinverse()*b
    a0, a1 = xhat
    fhat = a0 + a1*t
  \end{sagesilent}
  \question[12] Find the value of $y_0$ so that the least squares line of best
  fit to the data set
  \[
    \Set{\sage{p1}, \sage{p2}, \sage{p3}}
  \]
  is $\widehat{f}(t)=-2-t$. \emph{Clearly explain your reasoning to
    receive credit}.

  \begin{solution}[\stretch{1}]
    A ``perfect'' line $f(t)=a_0+a_1\,t$ would satisfy
    \[
      \begin{NiceArray}{rcrcrcrcr}
        f(\sage{p1[0]}) &=& \sage{p1[1]} &\to& a_0 &-& 3\,a_1 &=& y_0\\
        f(\sage{p2[0]}) &=& \sage{p2[1]} &\to& a_0 &-& 2\,a_1 &=&  -1\\
        f(\sage{p3[0]}) &=& \sage{p3[1]} &\to& a_0 &-& 4\,a_1 &=&   1\\
      \end{NiceArray}
    \]
    This is the system $A\bx=\bb$ where
    \begin{align*}
      A &= \sage{A} & \bx &= \nvc{a_0; a_1} & \bb &= \sage{bs.column()}
    \end{align*}
    The associated least squares problem is
    $A^\intercal A\widehat{x}=A^\intercal\bb$. The relevant data here is
    \begin{align*}
      \overset{A^\intercal}{\sage{A.T}}\overset{A}{\sage{A}} &= \sage{A.T*A} & \widehat{x} &= \nvc{\widehat{a}_0; \widehat{a}_1} & \overset{A^\intercal}{\sage{A.T}}\overset{\bb}{\sage{bs.column()}} &= \sage{A.T*bs.column()}
    \end{align*}
    The key to this problem is that we are told that the least squares line of
    best fit is $\widehat{f}(t)=\sage{fhat}=\widehat{a}_0+\widehat{a}_1\,t$,
    which means that $\widehat{a}_0=\sage{a0}$ and
    $\widehat{a}_1=\sage{a1}$. The quantity $A^\intercal A\widehat{x}$ is now
    \[
      \overset{A^\intercal A}{\sage{A.T*A}}\overset{\widehat{\bx}}{\sage{xhat.column()}}
      =
      \sage{A.T*b.column()}
    \]
    Now, setting the vector $A^\intercal A\widehat{\bx}$ equal to
    $A^\intercal\bb$ gives the equations
    \begin{align*}
      \sage{(A.T*A*xhat)[0]} &= \sage{(A.T*bs)[0]} & \sage{(A.T*A*xhat)[1]} &= \sage{(A.T*bs)[1]}
    \end{align*}
    The value $y_0=3$ works in both equations. This is our $y_0$!
  \end{solution}


\end{questions}
\end{document}
