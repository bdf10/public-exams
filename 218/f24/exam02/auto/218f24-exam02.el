;; -*- lexical-binding: t; -*-

(TeX-add-style-hook
 "218f24-exam02"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exm" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "exm"
    "exm12")
   (TeX-add-symbols
    '("raisefill" 1)
    "atay"))
 :latex)

