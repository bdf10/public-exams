\documentclass[12pt]{exm}

\usepackage{btrmt}
\usepackage{graphicx}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2024}{11}{22}{-1}}

\begin{document}
\begin{questions}


  \begin{sagesilent}
    Q = matrix(ZZ, [[2, -2, 3], [-2, -4, -2], [-1, 0, 2], [4, -1, -2], [0, -2, 2]])
    R = matrix(ZZ, 3, 3, {(0,0):-5, (0,1):-5, (0,2):2, (1,1):5, (1,2):-4, (2,2):-4})
    adjR = R.adjugate()
    var('t')
    chi = R.characteristic_polynomial(t)
    A = Q*R/5
    b = vector([0,0,500,0,0])
  \end{sagesilent}
  
  \question The data below depicts a matrix $Q$ with orthonormal columns, the
  adjugate of an upper triangular matrix $R$, and the characteristic polynomial
  of $R$.
  \begin{align*}
    Q &= \frac{1}{5}\sage{Q} & \adj(R) &= \sage{adjR} & \chi_R(t) &= \sage{chi}
  \end{align*}
  Let $A=QR$. \textbf{\emph{Do not ignore the factor of $\sfrac{1}{5}$ used to
      define $Q$.}}

  \vspace{2em}

  \begin{parts}
    \part[6] $\trace(Q^\intercal Q)=\fillin[$3$][0.5in]$ and $\trace(R)=\fillin[$\sage{R.trace()}$][0.5in]$
    
    \vspace{2em}

    \part[4] The $(3,1)$ cofactor of $R$ is $\fillin[$\sage{adjR[0,2]}$][0.5in]$.

    \vspace{2em}

    \part[5] Each of the following statements is true. However, only one is
    equivalent to the statement ``$R^{-1}$ exists.'' Select this statement.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\trace(\adj(R))\neq0$
      \choice The eigenvalues of $R$ do not sum to zero.
      \choice $\chi_R(t)$ is monic.\\[2em]
      \correctchoice $\chi_R(0)\neq0$
      \choice $\adj(R)$ exists.
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[12] Let $\bb=\sage{b}$. The system $A\bx=\bb$ is inconsistent. Find
    the least squares approximate solution $\widehat{\bx}$ to
    $A\bx=\bb$. Clearly explain your reasoning to receive credit.
    \begin{solution}[\stretch{1}]
      The relevant feature of $A=QR$ is that the least squares problem
      associated to $A\bx=\bb$ reduces to $R\widehat{\bx}=Q^\intercal\bb$.

      We are given $\adj(R)$ and we also know that
      $\det(R)=(-1)^3\cdot\chi_R(0)=-(-100)=100$. The adjugate formula for
      inverses then allows us to solve for $\widehat{\bx}$ with
      \begin{align*}
        \widehat{\bx}
        &=
          \overset{R^{-1}}{\frac{1}{\sage{R.det()}}\sage{adjR}}
          \overset{Q^\intercal}{\frac{1}{5}\sage{Q.T}}
          \overset{\bb}{\sage{b.column()}} \\
        &= \sage{adjR}\sage{Q.T}\sage{b.column()/500} \\
        &= \sage{adjR}\sage{Q.T*b.column()/500} \\
        &= \sage{adjR*Q.T*b.column()/500}
      \end{align*}
    \end{solution}

  \end{parts}
  


  \newpage



  \begin{sagesilent}
    A = matrix(ZZ, [[-5, 1], [4, -2]])
    chi = lambda t: t*identity_matrix(A.nrows())-A
    l1, l2 = A.eigenvalues()
    var('x y')
    u0 = vector([5*x, 5*y])(x=-1, y=3)
    var('t')
    v1, = chi(l1).right_kernel().basis()
    v2, = chi(l2).right_kernel().basis()
    X = matrix.column([v1, v2])
    D = diagonal_matrix([l1, l2])
  \end{sagesilent}
  \question[18] Let $\bu(t)$ be the solution to the initial value problem
  $\bu^\prime=A\bu$ with $\bu(0)=\bu_0$ where
  \begin{align*}
    A &= \sage{A} & \bu_0 &= \sage{u0.column()}
  \end{align*}
  Find $\bu(t)$. Clearly explain your reasoning to receive credit.
  \begin{solution}[\stretch{1}]
    From class we know that $\bu(t)=\exp(At)\bu_0$. Calculating matrix
    exponentials is hard, so we hope we can diagonalize $A=XDX^{-1}$, which
    gives $\bu(t)=X\exp(Dt)X^{-1}$.

    The characteristic polynomial of $A$ is
    \[
      \chi_A(t)
      = t^2-\trace(A)\,t+\det(A)
      = \sage{A.characteristic_polynomial(t)}
      = \sage{factor(A.characteristic_polynomial(t))}
    \]
    This tells us that $\EVals(A)=\Set{\sage{l1}, \sage{l2}}$. The eigenspaces are
    \begin{align*}
      \mathcal{E}_A(\sage{l1}) &= \Null\overset{\sage{l1}\cdot I_2-A}{\sage{chi(l1)}} = \Span\Set*{\sage{v1.column()}} & \mathcal{E}_A(\sage{l2}) &= \Null\overset{\sage{l2}\cdot I_2-A}{\sage{chi(l2)}} = \Span\Set*{\sage{v2.column()}}
    \end{align*}
    Our diagonalization is then $A=XDX^{-1}$ where
    \begin{align*}
      X &= \sage{X} & D &= \sage{D} & X^{-1} &= \frac{1}{\sage{X.det()}}\sage{X.adjugate()}
    \end{align*}
    Here, we have used the adjugate formula for inverses to calculate
    $X^{-1}$. Putting all this together gives
    \begin{align*}
      \bu(t)
      &= \overset{X}{\sage{X}}\overset{\exp(Dt)}{\sage{exp(D*t)}}\overset{X^{-1}}{\frac{1}{\sage{X.det()}}\sage{X.adjugate()}}\overset{\bu_0}{\sage{u0.column()}} \\
      &= \sage{X}\sage{exp(D*t)}\sage{X.inverse()*u0.column()} \\
      &= \sage{X}\sage{exp(D*t)*X.inverse()*u0.column()} \\
      &= \sage{X*exp(D*t)*X.inverse()*u0.column()}
    \end{align*}
  \end{solution}


  \newpage



  % [    -1     -I -I + 1      I]
  % [     I      1     -1 -I + 1]
  % [ I + 1     -1      1     -1]
  % [    -I  I + 1     -1     -1]
  \newcommand{\MyH}{
    \begin{bNiceArray}{cccc}
      {} ?   & -i   &  1-i &  i   \\
      {} i   &  ?   & -1   &  1-i \\
      {} 1+i & -1   &  ?   & -1   \\
      {}-i   &  1+i & -1   & ?   \\
    \end{bNiceArray}
  }
  % [       1        1        1        5]
  % [      -2    I + 1        0 -3*I - 1]
  % [   I + 2        I       -I   -I - 2]
  % [  -I - 1        0   -I + 1  4*I - 2]
  \newcommand{\MyU}{
    \begin{bNiceArray}{cccc}
      \dfrac{1}{\sqrt{x}} & \dfrac{1}{2} & \dfrac{1}{2} & \dfrac{5}{\sqrt{60}} \\[1em]
      \dfrac{-2}{\sqrt{x}} & \dfrac{1+i}{2} & 0 & \dfrac{-1-3\,i}{\sqrt{60}} \\[1em]
      \dfrac{2+i}{\sqrt{x}} & \dfrac{i}{2} & \dfrac{-i}{2} & \dfrac{-2-i}{\sqrt{60}} \\[1em]
      \dfrac{-1-i}{\sqrt{x}} & 0 & \dfrac{1-i}{2} & \dfrac{-2+4\,i}{\sqrt{60}} \\
    \end{bNiceArray}
  }
  \newcommand{\MyD}{
    \begin{bNiceArray}{cccc}
      {} 3 &    &    &    \\
      {}   &  1 &    &    \\
      {}   &    & -1 &    \\
      {}   &    &    & -3 \\
    \end{bNiceArray}
  }

  \question Suppose that $x>0$ is a real number and consider the spectral
  factorization $H=UDU^\ast$ given by
  \[
    \overset{H}{\MyH}
    =
    \overset{U}{\MyU}
    \overset{D}{\MyD}
    \btrmtBlock[height=4em, width=4em]{{\scalebox{2}{$U^\ast$}}}
  \]
  Note that each entry in the first column of $U$ has a denominator of
  $\sqrt{x}$ and that every diagonal entry of $H$ is unknown and marked $?$.

  \vspace{2em}

  \begin{parts}
    \part[5] $x=\fillin[12][0.5in]$
    \begin{solution}[\stretch{1}]
      Every column of $U$ must be a unit vector, so we need
      \[
        1
        = \norm*{(\sfrac{1}{\sqrt{x}})\cdot\nv{1 -2 {2+i} {-1-i}}}^2
        = (\sfrac{1}{x})\cdot(1^2+(-2)^2+2^2+1^2+(-1)^2+(-1)^2)
        = \sfrac{12}{x}
      \]
      Hence $x=12$.
    \end{solution}

    \part[16] Which of the following scalars is equal to zero? Select all that
    apply (2pts each).

    \vspace{1.5em}

    \begin{oneparcheckboxes}
      \correctchoice $\trace(H)$
      \choice $\det(H)$
      \correctchoice $\nullity(i\cdot I_4-H)$
      \correctchoice The ``imaginary part'' of every entry marked $?$. \\[1.5em]
      \correctchoice $\chi_H(i^2)$
      \correctchoice $\det(3\cdot I_4-H)$
      \choice One of the eigenvalues of $U$. \\[1.5em]
      \correctchoice The ``real part'' of $\exp(h_{31}\cdot\sfrac{\pi}{2})=e^{h_{31}\cdot\sfrac{\pi}{2}}$ where $h_{31}$  is the $(3,1)$ entry of $H$.
    \end{oneparcheckboxes}

    \begin{solution}[\stretch{1}]
      First, $\trace(H)=\trace(D)=3+1-1-3=0$. Then,
      $\det(H)=\det(D)=3\cdot(1)\cdot(-1)\cdot(-3)\neq 0$. Since $\lambda=i$ is
      not an eigenvalue of $H$, the characteristic matrix $i\cdot I_4-H$ is
      \emph{nonsingular}, which means $\nullity(i\cdot I_4-H)=0$.  Next, since
      $H^\ast=H$, every diagonal entry $?$ of $H$ must satisfy $\overline{?}=?$,
      which means every $?$ is a real number. Since $i^2=-1$ is an eigenvalue of
      $H$, $\chi_H(i^2)=0$. Similarly, $\det(3\cdot I_4-H)=0$. We know
      $U^\ast=U^{-1}$, so none of the eigenvalues of $U$ is zero. Finally,
      Euler's formula tells us that
      \[
        \exp(h_{31}\cdot\sfrac{\pi}{2})
        = \exp((\sfrac{\pi}{2})+(\sfrac{\pi}{2})\cdot i)
        = e^{\sfrac{\pi}{2}}\Set{\cos(\sfrac{\pi}{2})+i\,\sin(\sfrac{\pi}{2})}
        = e^{\sfrac{\pi}{2}}\Set{0+i\cdot 1}
        = i\,e^{\sfrac{\pi}{2}}
      \]
      So, the ``real part'' of this number is zero.
    \end{solution}


    \part[10] Suppose that $Y$ is invertible and let $A=(U^\ast Y^{-1}) D
    (YU)$. Calculate $\chi_A(2)$. Clearly explain your reasoning to receive
    credit.
    \begin{solution}[\stretch{3}]
      Let $X=U^\ast Y^{-1}$ and note that
      $X(YU)=U^\ast Y^{-1}YU= U^\ast I U = U^\ast U=I$. This means $X^{-1}=YU$
      so $A=XDX^{-1}$. It follows that
      \[
        \chi_A(t)
        = (t-3)(t-1)(t+1)(t+3)
      \]
      Hence
      \[
        \chi_A(2)
        = (2-3)(2-1)(2+1)(2+3)
        = (-1)(1)(3)(5)
        = -15
      \]
    \end{solution}



  \end{parts}


  \newpage


  \begin{sagesilent}
    S = matrix(ZZ, [[5, -1, 4, 3], [-1, 1, 2, -1], [4, 2, 4, 4], [3, -1, 4, 5]])
    var('x1 x2 x3 x4')
    x = vector([x1, x2, x3, x4])
    q = expand(x*S*x)
    D = S.jordan_form()
    chi = lambda x: x*identity_matrix(S.nrows())-S
  \end{sagesilent}
  \question Let $S$ be the $4\times 4$ real symmetric matrix that defines the
  quadratic form
  \[
    q(\bx)
    = \inner{\bx, S\bx}
    = \sage{q}
  \]
  If we ``complete the square'', then this quadratic form simplifies to
  \[
    q(\bx)=\lambda_1\,y_1^2+3\,y_2^2+2\,y_3^2-2\,y_4^2
  \]
  where $\lambda_1$ is a scalar and $y_1,y_2,y_3$ are expressions that depend on
  $x_1,x_2,x_3$.

  \vspace{1em}

  \begin{parts}
    \part[4] What is the definiteness of $S$? Select all that apply (no partial
    credit here).

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice positive definite
      \choice positive semidefinite
      \choice negative definite
      \choice negative semidefinite
      \correctchoice indefinite
    \end{oneparcheckboxes}

    \vspace{1em}

    \part[5] Only one of the following expressions is a correct formula for the
    quadratic form $f(\bx)=\inner{\bx, \exp(S)\bx}$. Select this expression.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice $f(\bx)=e^{\lambda_1}\,y_1^2+e^3\,y_2^2+e^2\,y_3^2+e^{-2}\,y_4^2$
      \choice $f(\bx)=e^{\lambda_1\,y_1^2}e^{3\,y_2^2}e^{2\,y_3^2}e^{-2\,y_4^2}$\\[1em]
      \choice $f(\bx)=e^{\lambda_1\,y_1^2}+e^{3\,y_2^2}+e^{2\,y_3^2}+e^{-2\,y_4^2}$
      \choice $f(\bx)=\lambda_1\,e^{y_1^2}+3\,e^{y_2^2}+2\,e^{y_3^2}-2\,e^{y_4^2}$
      \choice none of these
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[5] $\lambda_1=\fillin[$12$][0.5in]$
    \begin{solution}[\stretch{1}]
      The formula $q(\bx)=\lambda_1\,y_1^2+3\,y_2^2+2\,y_3^2-2\,y_4^2$ from
      ``completing the square'' tells us that
      $\EVals(S)=\Set{\lambda_1, 3, 2, -2}$. The ``square'' terms in the first
      formula for $q(\bx)$ are $5\,x_1^2,x_2^2,4\,x_3^2,5\,x_4^2$, which means
      that the diagonal entries of $S$ are $5,1,4,5$. The eigenvalue formula for
      trace then tells us that
      \[
        \lambda_1+3+2-2
        = \trace(S)
        = 5+1+4+5
        = 15
      \]
      It follows that $\lambda_1=15-3-2+2=12$.
    \end{solution}

    \part[10] Calculate $y_3^2$ when $x_1=2$, $x_2=4$, $x_3=6$, and
    $x_4=8$. Clearly explain your reasoning to receive credit.

    \newcommand{\MyUCols}{\btrmtCol[pad=2em, cols={\bu_1, \bu_2, \bu_3, \bu_4}]}
    \newcommand{\MyUT}{
      \begin{bNiceMatrix}[
        , margin
        , code-after = {
          \begin{tikzpicture}[
            , thick
            , line cap=round
            , shorten >=0.125em
            , shorten <=0.5em
            ]
            \draw (1.5-|1) -- (1.5-|2);
            \draw (1.5-|4) -- (1.5-|3);
            \draw (2.5-|1) -- (2.5-|2);
            \draw (2.5-|4) -- (2.5-|3);
            \draw (3.5-|1) -- (3.5-|2);
            \draw (3.5-|4) -- (3.5-|3);
            \draw (4.5-|1) -- (4.5-|2);
            \draw (4.5-|4) -- (4.5-|3);
          \end{tikzpicture}
        }
        ]
        \hPhantomSpace{2em}& \bu_1^\intercal &\hPhantomSpace{2em} \\[0.125em]
        \hPhantomSpace{2em}& \bu_2^\intercal &\hPhantomSpace{2em} \\[0.125em]
        \hPhantomSpace{2em}& \bu_3^\intercal &\hPhantomSpace{2em} \\[0.125em]
        \hPhantomSpace{2em}& \bu_4^\intercal &\hPhantomSpace{2em} \\[0.125em]
      \end{bNiceMatrix}
    }
    \renewcommand{\MyD}{\begin{bNiceMatrix}\lambda_1 & & &\\ & 3 & &\\ & & 2 & \\ & & & -2\end{bNiceMatrix}}

    \begin{solution}[\stretch{4}]
      The technique of ``completing the square'' expresses our quadratic form as
      \[
        q(\bx) = \lambda_1\,y_1^2+3\,y_2^2+2\,y_3^2-2\,y_4^2
      \]
      Here, $\EVals(S)=\Set{\lambda_1, 3, 2, -2}$ and $\by=U^\intercal\bx$ where
      $S=UDU^\intercal$ is a spectral factorization. This spectral factorization
      looks like
      \[
        \overset{S}{\sage{S}}
        = \overset{U}{\MyUCols}\overset{D}{\MyD}\overset{U^\intercal}{\MyUT}
      \]
      We know that $y_3=\inner{\bu_3, \bx}$ and that $\bu_3$ is a unit basis
      vector of $\mathcal{E}_S(2)$. We then turn our attention to the eigenspace
      \[
        \mathcal{E}_S(2)
        = \Null\overset{2\cdot I_4-S}{\sage{chi(2)}}
        = \Span\Set*{\bu_3=\frac{\pm1}{\sqrt{2}}\nvc{1;0;0;-1}}
      \]
      Here, the two valid formulas for $\bu_3$ were found by noticing the column
      relation $\Col_1=\Col_4$ in the characteristic matrix $2\cdot I_4-S$. Now, we have
      \[
        y_3^2
        = \left(\frac{\pm (x_1 - x_4)}{\sqrt{2}}\right)^2
        = \left(\frac{\pm (2-8)}{\sqrt{2}}\right)^2
        = \left(\frac{\pm 6}{\sqrt{2}}\right)^2
        = \frac{36}{2}
        = 18
      \]
    \end{solution}

  \end{parts}

\end{questions}
\end{document}
