;; -*- lexical-binding: t; -*-

(TeX-add-style-hook
 "218f24-exam03-solutions"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exm" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("btrmt" "") ("graphicx" "")))
   (TeX-run-style-hooks
    "latex2e"
    "exm"
    "exm12"
    "btrmt"
    "graphicx")
   (TeX-add-symbols
    "MyH"
    "MyU"
    "MyD"
    "MyUCols"
    "MyUT"))
 :latex)

