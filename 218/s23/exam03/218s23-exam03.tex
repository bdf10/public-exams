\documentclass[12pt]{exm}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2023}{04}{21}{-1}}

\begin{document}

\begin{questions}

  \begin{sagesilent}
    L = matrix(ZZ, [[3, 1, 2, 4, 5], [0, 1, 2, 3, 4], [0, 0, 2, 5, 9], [0, 0, 0, 1, 7], [0, 0, 0, 0, 2]])
    adjL = L.adjugate().change_ring(SR)
    var('ast')
    adjL[1,0] = adjL[4,0] = adjL[0,1] = adjL[0,4] = adjL[2,1] = adjL[1,2] = adjL[3,1]= ast
    C15 = L.matrix_from_rows_and_columns([1,2,3,4],[0,1,2,3])
  \end{sagesilent}
  \question Consider the matrix $L$ given by
  \begin{align*}
    L &= \sage{L} & \adj(L) &= \sage{adjL}
  \end{align*}
  Note that several entries in $\adj(L)$ are unknown and marked $\ast$.
  \begin{parts}
    \vspace{1em}
    \part[4] The trace of $L\adj(L)$ is $\fillin[$5\cdot\det(L)=5\cdot12=60$][0.5in]$.
    \vspace{1em}
    \part[4] The $(2,4)$ entry of $L^{-1}$ is $\fillin[$\sage{L.inverse()[1,3]}$][0.5in]$.
    \vspace{1em}
    \part[5] The missing $(5,1)$ entry of $\adj(L)$ is
    $\fillin[$0$][0.5in]$. Show your work below to receive credit, but fill in
    the blank to make your answer clear.
    \begin{solution}[\stretch{1}]
      The $(5,1)$ entry of $\adj(L)$ is the $(1,5)$ cofactor of $L$. We
      calculate this as
      \[
        C_{15}
        = (-1)^{1+5}\cdot\sage{detmat(C15)}
        = 0
      \]
    \end{solution}
  \end{parts}

  \begin{sagesilent}
    var('t')
    rs = 1, -1, 2, 5
    ms = 3, 2, 2, 2
    f = expand(prod((t-r)**m for r, m in zip(rs, ms)))
  \end{sagesilent}
  \question[5] All of the roots of the polynomial $f(t)$ below are known to be
  integers.
  \[
    f(t) = \sage{f}
  \]
  One of the following numbers is \emph{not} a root of $f(t)$. Select this
  number.
  \begin{oneparcheckboxes}
    \choice $-1$
    \choice $1$
    \choice $2$
    \correctchoice $3$
    \choice $5$
  \end{oneparcheckboxes}

  \vspace{2em}
  \begin{sagesilent}
    var('t')
    f = t**8 - 5*t**7 - t**5 - t**4 + 3*t**3 - t**2 - 2*t + 1
    z = 7+2*I
    f = expand((t-z)*(t-z.conjugate())*f)
  \end{sagesilent}
  \question Suppose that $A$ is an $n\times n$ matrix whose characteristic
  polynomial is given by
  \[
    \chi_A(t) = \sage{f}
  \]
  It is known that $\lambda_1=\sage{clx(z)}$ is an eigenvalue of $A$.
  \begin{parts}
    \vspace{2em}
    \part[4] The trace of $A$ is \fillin[$19$][0.5in] and the determinant of $A$ is \fillin[$53$][0.5in].
    \vspace{2em}
    \part[4] An eigenvalue $\lambda_2\neq\lambda_1$ of $A$ is
    $\lambda_2=\fillin[$\sage{clx(z.conjugate())}$]$ (any eigenvalue of $A$ not
    equal to $\lambda_1$ is valid here).
    \vspace{2em}
    \part[8] Which, if any, of the following statements is correct? Select all
    that apply (two points each).

    \begin{oneparcheckboxes}
      \correctchoice $A$ is nonsingular
      \choice $A$ is Hermitian
      \choice $A$ is real symmetric
      \correctchoice $A\adj(A)$ is positive definite
    \end{oneparcheckboxes}
  \end{parts}


  \skippage

  \begin{sagesilent}
    var('t')
    A = matrix(QQ, [[5, -2, -1], [-6, 6, 3], [16, -10, -5]])
    A = matrix(QQ, [[8, -5, -4], [2, 1, -1], [6, -6, -3]])
    A = matrix(QQ, [[-1, 1, 4], [-3, 3, 3], [-1, 1, 4]])
    chi = lambda l: l*identity_matrix(A.nrows())-A
    from functools import partial
    e = partial(elementary_matrix, A.nrows())
    chi1 = e(row1=0, row2=2, scale=-1)*e(row1=1, row2=2, scale=-3)*chi(t)
    chi2 = e(row1=0, scale=1/t)*chi1
    chi3 = e(row1=2, row2=0, scale=-1)*chi2
    chi4 = e(row1=1, row2=2, scale=3)*chi3
    chi5 = chi4.matrix_from_rows_and_columns([0,2],[0,2])
  \end{sagesilent}
  \question[20] Determine if $A=\sage{A}$ is diagonalizable. Clearly explain
  your reasoning to receive credit.
  \begin{solution}[\stretch{1}]
    We need to find the eigenvalues of $A$, so let's look at the characteristic polynomial.
    \begin{align*}
      \chi_A(t)
      &= \sage{detmat(chi(t))} \\
      &= \sage{detmat(chi1)} \\
      &= t\cdot\sage{detmat(chi2)} \\
      &= t\cdot\sage{detmat(chi3)} \\
      &= t\cdot\sage{detmat(chi4)} \\
      &= t\cdot(t-3)\cdot\sage{detmat(chi5)} \\
      &= \sage{factor(A.characteristic_polynomial(t))}
    \end{align*}
    Here, we have used the row-reduction technique illustrated in the SVD
    example from class. Of course, a direct calculation will yield the same
    result.

    Now, we have $\EVals(A)=\Set{0, 3}$ with $\am_A(0)=1$ and
    $\am_A(3)=2$. Diagonalizablity is now a question of whether or not
    $\gm_A(3)=2$. To resolve this issue, we consider
    \[
      \mathcal{E}_A(3)
      = \Null\overset{3\cdot I_3-A}{\sage{chi(3)}}
      = \Span\Set*{\sage{matrix.column([1,0,1])}}
    \]
    The characteristic matrix $3\cdot I_3-A$ has rank two since the first two
    columns are not multiples of each other. This means that
    $\gm_A(3)=1\neq\am_A(3)$ so $A$ is \emph{not diagonalizable}.
  \end{solution}


  \skippage
  \begin{sagesilent}
    var('b c')
    U = matrix(SR, [[2/3, -1/3, 0, 2/3], [-1/3, -2/3, -2/3, 0], [-2/3, 0, 1/3, 2/3], [0, -2/3, 2/3, -1/3]])
    c_value = U[2,2]
    U[2,2] = c
    D = diagonal_matrix([7, -2, b*I, -b*I])
    bv = vector([3, 6, 0, 6])
  \end{sagesilent}
  \newcommand{\BlockS}[1][S]{
    \begin{bNiceMatrix}%[columns-width=0.75em]
      \Block{3-5}{#1} &&&&\\
      &&&&\\
      &&&&\\
    \end{bNiceMatrix}
  }
  \newcommand{\MatD}[1][S]{
    \begin{bNiceMatrix}%[columns-width=0.75em]
      {} 7 &    &     &     \\
      {}   & -2 &     &     \\
      {}   &    &  bi &     \\
      {}   &    &     & -bi \\
    \end{bNiceMatrix}
  }
  \question The following equation depicts a spectral factorization of a real
  symmetric matrix $S$.
  \[
    \BlockS
    =
    \overset{U}{\sage{U}}\overset{D}{\MatD}\overset{U^\intercal}{\sage{U.T}}
  \]
  Note that the variable $b$ in $D$ and the variable $c$ in $U$ (and
  $U^\intercal$) are currently unknown.
  \begin{parts}
    \vspace{2em}
    \part[9] The trace of $S$ is $\fillin[$5$][0.5in]$, the value of $b$ is
    $\fillin[$0$][0.5in]$, and the value of $c$ is
    $\fillin[$\sage{c_value}$][0.5in]$.

    \vspace{4em}
    \part[4] What is the definiteness of $S$? Select all that apply.

    \begin{oneparcheckboxes}
      \choice positive definite
      \choice positive semidefinite
      \choice negative definite
      \choice negative semidefinite
      \correctchoice indefinite
    \end{oneparcheckboxes}

    \vspace{2em}
    \part[4] The largest eigenvalue of $S$ is $\fillin[$7$][0.5in]$ and the
    \emph{smallest} eigenvalue of $\exp(S)$ is $\fillin[$e^{-2}$][0.5in]$.

    \vspace{1em}
    \part[10] Let $q$ be the quadratic form defined by $S$. Calculate
    $q(3,6,0,6)$.
    \begin{solution}[\stretch{1}]
      Note that
      \[
        q(\bv[x])
        = \inner{\bv[x], S\bv[x]}
        = \inner{\bv[x], UDU^\intercal\bv[x]}
        = \inner{U^\intercal\bv[x], DU^\intercal\bv[x]}
      \]
      This is about as far as we'll get with symbols. Now we'll calculate
      \[
        \overset{U^\intercal}{\sage{U.T}}\overset{\bv[x]}{\sage{bv.column()}}
        = \sage{U.T*bv.column()}
      \]
      Our calculation then completes to
      \begin{align*}
        q(3,6,0,6)
        &= \inner{\sage{U.T*bv}, D\sage{U.T*bv}} \\
        &= \inner{\sage{U.T*bv}, \sage{D*U.T*bv}} \\
        &= 7\cdot 0^2-2\cdot(-9)^2+b\,i\cdot0^2-b\,i\cdot0^2 \\
        &= -2\cdot81 \\
        &= -162
      \end{align*}
    \end{solution}

  \end{parts}


  \skippage

  \begin{sagesilent}
    A = matrix(ZZ, [[0, 1, 0, 1], [1, 0, 0, 0], [0, 0, 1, 1], [1, 0, 0, 0], [0, 1, 1, 0]])
    A = matrix(ZZ, [[0, -1, 1, 0], [1, 0, -1, 2], [1, 1, 0, 0], [1, 0, -1, 0], [0, 2, 0, -1]])
    A = matrix(ZZ, [[1, -1, -1, -1], [0, 0, 0, 1], [0, -1, 2, -1], [2, -1, 1, 1], [0, 2, 1, 1]])
    S = A.T*A
    l1, l2, l3, l4 = sorted(S.eigenvalues(), reverse=True)
    E = lambda l: (l*identity_matrix(S.nrows())-S).change_ring(ZZ)
    V = matrix.column(map(lambda l: E(l).right_kernel().basis()[0], sorted(S.eigenvalues(), reverse=True)))
    D = diagonal_matrix(sorted(S.eigenvalues(), reverse=True))
  \end{sagesilent}

  \renewcommand{\MatD}{
    \begin{bNiceMatrix}
      {} 10 &   &   &   \\
      {}    & 8 &   &   \\
      {}    &   & 5 &   \\
      {}    &   &   & 1 \\
    \end{bNiceMatrix}
  }
  \newcommand{\oss}[1][1]{\sfrac{#1}{\sqrt{6}}}
  \newcommand{\ost}[1][1]{\sfrac{#1}{\sqrt{3}}}
  \newcommand{\MatV}{\large
    \begin{bNiceMatrix}[r]
      {} \oss     & \oss    & \ost     & \ost     \\
      {} \oss[-2] & 0       & 0        & \ost     \\
      {} 0        & \oss[2] & \ost[-1] & 0        \\
      {} \oss[-1] & \oss    & \ost     & \ost[-1] \\
    \end{bNiceMatrix}
  }
  \newcommand{\MatVT}{\large
    \begin{bNiceMatrix}[r]
      {} \oss & \oss[-2] & 0        & \oss[-1] \\
      {} \oss & 0        & \oss[2]  & \oss     \\
      {} \ost & 0        & \ost[-1] & \ost     \\
      {} \ost & \ost     & 0        & \ost[-1] \\
    \end{bNiceMatrix}
  }
  \question The equations below depict a matrix $A$ and a spectral factorization
  of $A^\intercal A$.
  \begin{align*}
    A &= \sage{A} & A^\intercal A &= \overset{V}{\MatV}\overset{D}{\MatD}\overset{V^\intercal}{\MatVT}
  \end{align*}
  \begin{parts}
    \vspace{1em}
    \part[3] The rank of $A$ is $r=\fillin[$4$][0.5in]$.
    
    \vspace{1em}
    \part[6] The largest singular value of $A$ is $\sigma_1=\fillin[$\sqrt{10}$][0.5in]$ and the smallest singular value of $A$ is $\sigma_r=\fillin[$1$][0.5in]$.

    \vspace{1em}
    \part[10] Suppose we use the given spectral factorization of $A^\intercal A$
    to calculate singular value decomposition $A=U\Sigma V^\ast$. Find the last
    column of $U$.
    \begin{solution}[\stretch{1}]
      The last column of $U$ fits into the equation
      \[
        A\bv[v]_4 = \sigma_4\cdot\bv[u]_4
      \]
      Since $\sigma_4=1$, we have
      \[
        \bv[u]_4
        = \overset{A}{\sage{A}}\overset{\bv_4}{\frac{1}{\sqrt{3}}\sage{V.T[-1].column()}}
        = \frac{1}{\sqrt{3}}\sage{A*V.T[-1].column()}
      \]
    \end{solution}

  \end{parts}

\end{questions}

\end{document}
