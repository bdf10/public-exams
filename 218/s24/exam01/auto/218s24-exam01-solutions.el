(TeX-add-style-hook
 "218s24-exam01-solutions"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exm" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "exm"
    "exm12")
   (TeX-add-symbols
    '("BlockA" ["argument"] 0)
    "MySystemA"
    "MySystemR"
    "FirstTwoA"
    "FirstTwoB"
    "FirstTwoC"
    "FirstTwoD"
    "LastTwoA"
    "LastTwoB"
    "LastTwoC"
    "LastTwoD"
    "LastTwoE"))
 :latex)

