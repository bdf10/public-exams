\documentclass[12pt]{exm}

\usepackage{btrmt}

\renewcommand{\examtitle}{Exam II}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2024}{3}{22}{-1}}

\begin{document}
\begin{questions}

  \begin{sagesilent}
    var('s t')
    A = matrix(ZZ, [[1, 1, -1, -1], [0, 1, 1, -3], [-2, -1, 3, -1]])
    kr = sum(A.right_kernel().basis()).column().change_ring(SR)
    kl = sum(A.left_kernel().basis()).column().change_ring(SR)
    set_random_seed(2)
    v = kr + (t-2)*random_matrix(ZZ, kr.nrows(), 1, x=-2, y=3)
    set_random_seed(3)
    w = kl + (s-1)*random_matrix(ZZ, kl.nrows(), 1, x=-2, y=3)
  \end{sagesilent}

  \question The two equations below depict matrix-vector products $A\bv$ and
  $A^\intercal\bw$ (note that this matrix $A$ is
  $\sage{A.nrows()}\times\sage{A.ncols()}$).
  \begin{align*}
    \btrmtBlock[width=4em, height=2em]{A}\overset{\bv}{\sage{v}} &= \sage{A*v} & \btrmtBlock[width=4em, height=2em]{A^\intercal}\overset{\bw}{\sage{w}} &= \sage{A.T*w}
  \end{align*}
  Note that the formula for $\bv$ (and thus the formula for $A\bv$) depends on a
  scalar $t$ and that the formula for $\bw$ (and thus the formula for $A\bw$)
  depends on a scalar $s$.

  \vspace{2em}

  \begin{parts}
    \part[5] Every vector in $\Null(A)$ has \fillin[$\sage{A.ncols()}$][0.5in]
    coordinates and every vector in $\Col(A)$ has
    \fillin[$\sage{A.nrows()}$][0.5in] coordinates.

    \vspace{1em}

    \part[5] Setting $t=2$ in the equation for $A\bv$ above leads us to which of
    the following conclusions?

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice $\sage{v.T[0](t=2).column()}\in\Null(A)$
      \choice $\sage{v.T[0](t=2).column()}\in\Col(A)$
      \choice $\sage{v.T[0](t=2).column()}\in\Col(A^\intercal)$
      \choice $\sage{v.T[0](t=2).column()}\in\Null(A^\intercal)$
    \end{oneparcheckboxes}

    \begin{solution}[\stretch{1}]
      This is
      $\btrmtBlock[width=4em, height=2em]{A}\sage{v(t=2)}=\sage{A*v(t=2)}$,
      which means $\sage{v(t=2)}\in\Null(A)$.
    \end{solution}


    \part[5] Setting $t=0$ in the equation for $A\bv$ above leads us to which of
    the following conclusions?

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\sage{v.T[0](t=0).column()}\in\Col(A)$
      \choice $\sage{v.T[0](t=0).column()}\in\Null(A)$
      \choice $\sage{(A*v).T[0](t=0).column()}\in\Null(A)$
      \correctchoice $\sage{(A*v).T[0](t=0).column()}\in\Col(A)$
      \choice $\sage{(A*v).T[0](t=0).column()}\in\Col(A^\intercal)$
    \end{oneparcheckboxes}

    \begin{solution}[\stretch{1}]
      This is
      $\btrmtBlock[width=4em, height=2em]{A}\sage{v(t=0)}=\sage{A*v(t=0)}$,
      which means $\sage{A*v(t=0)}\in\Col(A)$.
    \end{solution}

    \part[5] Setting $s=1$ in the equation for $A^\intercal\bw$ above leads us
    to which of the following conclusions?

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\sage{w.T[0](s=1).column()}\perp\Null(A)$
      \choice $\sage{w.T[0](s=1).column()}\perp\Col(A^\intercal)$
      \choice $\sage{w.T[0](s=1).column()}\perp\Null(A^\intercal)$
      \correctchoice $\sage{w.T[0](s=1).column()}\perp\Col(A)$
    \end{oneparcheckboxes}

    \begin{solution}[\stretch{1}]
      This is
      $\btrmtBlock[width=4em,
      height=2em]{A^\intercal}\sage{w(s=1)}=\sage{A.T*w(s=1)}$, which means
      $\sage{w(s=1)}\in\Null(A^\intercal)$ so $\sage{w(s=1)}\perp\Col(A)$.
    \end{solution}

    \part[5] Setting $s=0$ in the equation for $A^\intercal\bw$ above leads us
    to which of the following conclusions?

    \vspace{1em}

    \begin{oneparcheckboxes}
      \choice $\sage{w.T[0](s=0).column()}\perp\Col(A)$
      \choice $\sage{(A.T*w).T[0](s=0).column()}\perp\Col(A)$
      \choice $\sage{w.T[0](s=0).column()}\perp\Null(A)$
      \correctchoice $\sage{(A.T*w).T[0](s=0).column()}\perp\Null(A)$
      \choice $\sage{(A.T*w).T[0](s=0).column()}\perp\Col(A^\intercal)$
    \end{oneparcheckboxes}

    \begin{solution}[\stretch{1}]
      This is
      $\btrmtBlock[width=4em,
      height=2em]{A^\intercal}\sage{w(s=0)}=\sage{A.T*w(s=0)}$, which means
      $\sage{A.T*w(s=0)}\in\Col(A^\intercal)$ so
      $\sage{A.T*w(s=0)}\perp\Null(A)$.
    \end{solution}

  \end{parts}


  \skippage
  \begin{sagesilent}
    set_random_seed(4891)
    A = random_matrix(ZZ, 9, 4, algorithm='echelonizable', rank=4)
  \end{sagesilent}

  \question[14] Suppose that $A$ is $\sage{A.nrows()}\times\sage{A.ncols()}$
  with linearly independent columns. Fill in every missing label in the picture
  of the four fundamental subspaces of $A$ below, including the dimension of
  each fundamental subspace.
  \[
    \begin{tikzpicture}[draw=black, text=black]

      \pgfmathsetmacro{\PerpLength}{0.25}
      \pgfmathsetmacro{\SpaceRotate}{20}
      \tikzstyle{Space} = [
      , draw
      , ultra thick
      , outer sep=0pt
      % , minimum height=2.75cm
      % , minimum width=3.25cm
      , minimum height=3.00cm
      , minimum width=3.5cm
      ]
      \tikzstyle{LeftSpace} = [
      , Space
      , rotate=\SpaceRotate
      ]
      \tikzstyle{RightSpace} = [
      , Space
      , rotate=-\SpaceRotate
      ]
      \tikzstyle{TopSpace} = [
      % , fill=blubg
      % , minimum height=1.25cm
      % , minimum width=1.5cm
      % , minimum height=2.25cm
      % , minimum width=2.75cm
      ]
      \tikzstyle{Point} = [
      , <-
      , thick
      , overlay
      , shorten <=0.5mm
      ]

      \node (Row) [
      , LeftSpace
      , TopSpace
      , anchor=south west
      , text=blue
      ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A^\intercal)}$\fi};

      \node (Null) [
      , LeftSpace
      % , fill=grnbg
      , anchor=north east
      , minimum height=2.0cm
      , minimum width=2.75cm
      , text=blue
      ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.right_nullity()}}{\Null(A)}$\fi};
      \node[right] (Rn) at (Row.east) {$\mathbb{R}^{\fillin[$\sage{A.ncols()}$][0.5in]}$};

      \draw[LeftSpace] (0, 0) rectangle (-\PerpLength,\PerpLength);

      \begin{scope}[xshift=10.5cm]
        \node(Col) [
        , RightSpace
        , TopSpace
        , anchor=south east
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.rank()}}{\Col(A)}$\fi};

        \node(LNull) [
        , RightSpace
        % , fill=redbg
        , anchor=north west
        , minimum height=2.0cm
        , minimum width=2.75cm
        , text=blue
        ] {\ifprintanswers$\scriptstyle\underset{\dim=\sage{A.left_nullity()}}{\Null(A^\intercal)}$\fi};

        \draw[RightSpace] (0, 0) rectangle (\PerpLength,\PerpLength);

        \node[left] (Rm) at (Col.west) {$\mathbb{R}^{\fillin[$\sage{A.nrows()}$][0.5in]}$};
      \end{scope}

      \draw[->, thick] (Rn.east) -- (Rm.west)
      node[midway, above, overlay] {$A$};

      % \draw[Point, blu] (Col.north) |- ++(3mm, 2.5mm) node[right]
      % {$\scriptstyle\beta=\Set*{\nv[small]{1 1 5}, \nv[small]{4 3 24}}$};

      % \draw[Point, red] (LNull.north) |- ++(2mm, 2.5mm) node[right]
      % {$\scriptstyle\beta=\Set*{\nv[small]{-9 4 1}}$};

    \end{tikzpicture}
  \]
  \begin{solution}[\stretch{1}]
    This matrix must be full column rank since it has independent columns. This
    means that $\dim\Col(A)=\rank(A)=4$ and $\dim\Col(A^\intercal)=4$. The rest
    of the dimensions are inferred from the rank-nullity theorem.
  \end{solution}



  \begin{sagesilent}
    Q = matrix(QQ, [[437/537, -54/179, -250/537, 40/537], [250/537, 135/179, 88/537, -100/537], [14/537, 72/179, 35/537, 424/537], [16/179, -60/179, 40/179, 101/179], [60/179, -46/179, 150/179, -24/179]])
    U = matrix(QQ, [[19/121, 10/121, 118/121], [46/121, 107/121, -20/121], [10/11, -4/11, -1/11], [-8/121, 34/121, 14/121]])
    B = matrix.column([3, 5, 1]).T
  \end{sagesilent}
  \question The equation below defines a matrix $A=BU^\intercal
  Q^\intercal$. Here, B is a $1\times 3$ matrix, $U$ is a $4\times 3$ matrix,
  and $Q$ is a $5\times 4$ matrix. \textbf{It is known that both $U$ and $Q$
    have orthonormal columns}.
  \[
    A
    =
    \overset{B}{\sage{B}}
    \overset{U^\intercal}{\sage{U.T}}
    \overset{Q^\intercal}{\sage{Q.T}}
  \]

  \vspace{1em}

  \begin{parts}
    \part[6] $Q^\intercal Q=\fillin[$I_4$][0.5in]$ and
    $U^\intercal U=\fillin[$I_3$][0.5in]$ (use notation that conveys the size of
    these matrices for full credit).

    \vspace{2em}

    \part[8] Calculate $AA^\intercal$ (this will be a scalar value since $A$ is
    $1\times 5$). \emph{Hint}. Most of this calculation should involve symbolic
    manipulation (there is no hope of carrying out this calculation
    numerically).
    \begin{solution}[\stretch{5}]
      Here we have
      \[
        AA^\intercal
        = (B U^\intercal Q^\intercal)(B U^\intercal Q^\intercal)^\intercal
        = BU^\intercal Q^\intercal Q U B^\intercal
        = BU^\intercal I_4 U B^\intercal
        = BU^\intercal U B^\intercal
        = B I_3 B^\intercal
        = \sage{B}\sage{B.T}
        = \sage{B*B.T}
      \]
    \end{solution}

  \end{parts}


  \skippage

  \begin{sagesilent}
    var('ast h')
    L = matrix([(1, 0, 0, 0), (ast, 2, 0, 0), (ast, ast, 3, 0), (ast, ast, ast, h)])
  \end{sagesilent}

  \newcommand{\MySteps}{
    \begin{NiceArray}{rcrcr}[small]
      \br_2 &+&  3\cdot\br_1 &\to& \br_2 \\
      \br_4 &-& 15\cdot\br_1 &\to& \br_4 \\
    \end{NiceArray}
  }

  \question[8] The expression below depicts a sequence of row reductions applied to
  a $4\times 4$ matrix $A$. \textbf{It is known that $\det(A)=-30$}.
  \[
    \btrmtBlock[width=4em, height=2em]{A}
    \xrightarrow{\MySteps} \btrmtBlock[width=4em, height=2em]{A_1}
    \xrightarrow{\br_2\leftrightarrow\br_3} \btrmtBlock[width=4em, height=2em]{A_2}
    \xrightarrow{\br_4+7\cdot\br_2\to\br_4} \overset{L}{\sage{L}}
  \]
  Note that the lower-triangular matrix labeled $L$ is missing several entries
  marked $\ast$ and has a variable $h$ in its $(4,4)$ position.

  Find the correct value of $h$. Clearly explain your work to receive credit.

  \begin{solution}[\stretch{1}]
    We arrive at $L$ from $A$ with row addition, a row swap, and another row
    addition. Row addition does not change determinants and row swaps negate
    determinants. This means that $\det(L)=-\det(A)=-(-30)=30$.

    However, $L$ is lower-triangular so we also have $\det(L)=6\,h$. Putting
    this together gives $h=5$.
  \end{solution}




  \begin{sagesilent}
    A = matrix.column([(1, 1, 0, 0, 0), (0, 2, 1, 1, 1)])
    P = A*A.pseudoinverse()
    b = matrix(ZZ, [[-2], [-2], [0], [4], [1]])
    p = P*b
    l1, l2, l3 = map(matrix.column, A.left_kernel().basis())
    l = b-p
  \end{sagesilent}

  \question The data below depicts a matrix $A$ and the result of projecting a
  vector $\bb$ onto $\Col(A)$.
  \begin{align*}
    A &= \sage{A} && \textnormal{Projection of }\bb=\sage{b}\textnormal{ onto }\Col(A)\textnormal{ is }\sage{p}.
  \end{align*}
  In this problem, $P$ is the projection matrix onto $\Col(A)$.

  \vspace{2em}

  \begin{parts}
    \part[5] $\trace(P^2)=\fillin[$2$][0.5in]$

    \part[5] What is the projection of $\bb$ onto $\Null(A^\intercal)$?
    \begin{oneparcheckboxes}
      \choice $\sage{b+p}$
      \correctchoice $\sage{l}$
      \choice $\sage{-b-p}$
      \choice $\sage{-l}$
      \choice $\sage{l1+l2+l3}$
    \end{oneparcheckboxes}

    \vspace{2em}

    \part[8] Using the Gram-Schmidt algorithm to calculate $Q$ in $A=QR$
    produces $\bq_1=\dfrac{1}{\sqrt{2}}\nv{1 1 0 0 0}$ as the first column of
    $Q$. Find the second column $\bq_2$ of $Q$. Clearly show your work
    (including any formulas you use) to receive credit.
    \begin{solution}[\stretch{2}]
      Let $\ba_1$ and $\ba_2$ be the two columns of $A$. Gram-Schmidt gives
      $\bw_1=\ba_1$ and
      \[
        \bw_2
        = \ba_2 - \proj_{\bw_1}(\ba_2)
        = \ba_2 - \frac{\inner{\bw_1,\ba_1}}{\inner{\bw_1,\bw_1}}\bw_1
        = \ba_2 - \frac{2}{2}\bw_1
        = \sage{A.T[1].column()}-\sage{A.T[0].column()}
        = \sage{A.T[1].column()-A.T[0].column()}
      \]
      This gives $\bq_2=\frac{1}{\sqrt{5}}\sage{A.T[1]-A.T[0]}$.
    \end{solution}

  \end{parts}




  \skippage

  \begin{sagesilent}
    A1 = matrix.column([(1, 1, 1), (3, 2, -6)])
    b1 = vector([7, -5, 9])
    M1 = A1.augment(b1, subdivide=True)
    M2 = (A1.T*A1).augment(A1.T*b1, subdivide=True)
    M3 = A1.T.augment(A1.T*b1, subdivide=True)
    M4 = matrix([(3, 7), (2, -5), (-6, 9)]).augment(zero_vector(3), subdivide=True)
    M5 = matrix([(3**2, 7**2), (2**2, (-5)**2), ((-6)**2, 9**2)]).augment(vector([1]*3), subdivide=True)
  \end{sagesilent}

  \question[5] Consider the three data points $\Set{(3, 7), (2, -5), (-6, 9)}$. The
  fact that these three data points do not all lie on a line is equivalent to
  the inconsistency of which of the following augmented systems?

  \vspace{2em}

  \begin{oneparcheckboxes}
    \choice $\sage{M2}$
    \choice $\sage{M3}$
    \choice $\sage{M4}$
    \correctchoice $\sage{M1}$
    \choice $\sage{M5}$
  \end{oneparcheckboxes}

  \begin{solution}[\stretch{1}]
    Attempting to fit a line $f(t)=a_0+a_1\,t$ to all three data points leads us
    to the system $\sage{M1}$.
  \end{solution}


  \begin{sagesilent}
    var('x y')
    f1 = x**2-y
    f2 = x-y**2
    data = [(-1, 1), (1, 2), (2, 2)]
    A1 = matrix([(f1(x=t[0], y=t[1]), f2(x=t[0], y=t[1])) for t in data])
    b1 = vector([1]*3)
    M1 = A1.augment(b1, subdivide=True)
    M2 = (A1.T*A1).augment(A1.T*b1, subdivide=True)
    A3 = matrix.column([(1, 1, 1), (-1, 1, 2)])
    b3 = vector([1, 2, 2])
    M3 = A3.augment(b3, subdivide=True)
    ff1 = x**2-y
    ff2 = x**2+y**2
    data = [(-1, 1), (1, 2), (2, 2)]
    A4 = matrix([(ff1(x=t[0], y=t[1]), ff2(x=t[0], y=t[1])) for t in data])
    b4 = vector([1]*3)
    M4 = A4.augment(b4, subdivide=True)
    fff1 = x**3-y
    fff2 = x-y**2
    data = [(-1, 1), (1, 2), (2, 2)]
    A5 = matrix([(ff1(x=t[0], y=t[1]), ff2(x=t[0], y=t[1])) for t in data])
    b5 = vector([1]*3)
    M5 = A5.augment(b4, subdivide=True)
  \end{sagesilent}
  \question[6] Consider the three data points $\Set{(-1, 1), (1, 2), (2,
    2)}$. The fact that it is impossible to fit a curve of the form
  \[
    c_1\,(x^2-y) + c_2\,(x-y^2) = 1
  \]
  to these data points is equivalent to the inconsistency of which of the
  following augmented systems?

  \vspace{2em}

  \begin{oneparcheckboxes}
    \correctchoice $\sage{M1}$
    \choice $\sage{M2}$
    \choice $\sage{M3}$
    \choice $\sage{M4}$
    \choice $\sage{M5}$
  \end{oneparcheckboxes}

  \begin{solution}[\stretch{1}]
    Plugging the $x$ and $y$ values of each data point into this hypothetical
    curve leads us to $\sage{M1}$.
  \end{solution}


  \begin{sagesilent}
    data = matrix(ZZ, [[1, -1], [1, -2], [-1, 1]])
    var = ('x y')
    f1 = x**3
    f2 = x-y
    A = matrix([(f1(x=t[0], y=t[1]), f2(x=t[0], y=t[1])) for t in data])
    b = vector([2]*3)
    xhat = matrix.column([-4, 2])
  \end{sagesilent}
  \question[10] It is impossible to fit the three data points
  $\Set{\sage{tuple(data[0])}, \sage{tuple(data[1])}, \sage{tuple(data[2])}}$ to
  a curve of the form
  \[
    c_1 x^3 + c_2\,(x-y) = 2
  \]
  We can, however, use the method of least squares to \emph{approximate} these
  data points and this process produces the curve $-4\,x^3+2\,(x-y)=2$ (so
  $\widehat{c}_1=-4$ and $\widehat{c}_2=2$). Find the error $E$ in using this
  method to approximate this data.

  \emph{Hint.} You will earn four points for identifying the the system
  $A\bx=\bb$ whose inconsistency is equivalent to the fact that we cannot
  perfectly fit to the original curve to the data.

  \begin{solution}[\stretch{4}]
    Plugging each $x$ and $y$ coordinate of each data point into the original
    curve yields the system
    \newcommand{\MySystem}{
      \begin{NiceArray}{rcrcr}
        1^3\,c_1 &+&(1-(-1))\,c_2 &=& 2 \\
        1^3\,c_1 &+&(1-(-2)))\,c_2 &=& 2 \\
        (-1)^3\,c_1 &+&(-1-1))\,c_2 &=& 2 \\
      \end{NiceArray}
    }
    \[
      \MySystem
    \]
    This is the system $A\bx=\bb$ where
    \begin{align*}
      A &= \sage{A} & \bx &= \nvc{c_1; c_2} & \bb &= \sage{b.column()}
    \end{align*}
    This system is inconsistent, but we are told that the associated least
    squares problem has solution $\widehat{\bx}=\nv{-4 2}$. The error is then
    \[
      E
      = \norm*{\overset{\bb}{\sage{b.column()}}-\overset{A}{\sage{A}}\overset{\widehat{\bx}}{\sage{xhat}}}^2
      = \norm*{\sage{b.column()}-\sage{A*xhat}}^2
      = \norm*{\sage{b.column()-A*xhat}}^2
      = \sage{(b.column()-A*xhat).T[0].norm()**2}
    \]

  \end{solution}


\end{questions}
\end{document}
