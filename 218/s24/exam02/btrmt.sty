\ProvidesPackage{hlmat}[2024/01/30 v1.0 My custom nicematrix enhancements.]

\RequirePackage{nicematrix}
\usepackage{etoolbox}
\usepackage{pgffor}
\RequirePackage{xstring}
\RequirePackage{tikz}
\usetikzlibrary{calc}


%%%%%%%%%%%%%%%%%%%%%%%%%
% HIGHLIGHTING MATRICES %
%%%%%%%%%%%%%%%%%%%%%%%%%

\tikzstyle{btrmt-hl}=[
, rounded corners=3pt
, semithick
]
\tikzstyle{btrmt-annotate}=[
, semithick
, solred
, <-
, line cap=round
, shorten <= 1pt
]

\newcommand{\hlRow}[2][]{
  \tikz
  \pgfmathtruncatemacro{\lastCol}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\nextRow}{#2+1}
  \draw[btrmt-hl, #1]
  (row-#2-|col-1) rectangle (row-\nextRow-|col-\lastCol);
}
\newcommand{\hlCol}[2][]{
  \tikz
  \pgfmathtruncatemacro{\lastRow}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\nextCol}{#2+1}
  \draw[btrmt-hl, #1]
  (row-1-|col-#2) rectangle (row-\lastRow-|col-\nextCol);
}
\newcommand{\hlMat}[1][]{
  \tikz
  \pgfmathtruncatemacro{\lastRow}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\lastCol}{\arabic{jCol}+1}
  \draw[btrmt-hl, #1]
  (row-1-|col-1) rectangle (row-\lastRow-|col-\lastCol);
}
\newcommand{\hlEntry}[3][]{
  \tikz
  \pgfmathtruncatemacro{\nextRow}{#2+1}
  \pgfmathtruncatemacro{\nextCol}{#3+1}
  \draw[btrmt-hl, #1]
  (row-#2-|col-#3) rectangle (row-\nextRow-|col-\nextCol);
}
\newcommand{\hlDiag}[1][]{
  \tikz
  \pgfmathtruncatemacro{\n}{min(\arabic{jCol}, \arabic{iRow})+1}
  \draw[btrmt-hl, #1]
  (row-1-|col-1)
  \foreach \i in {2,...,\n}{ |- (row-\i-|col-\i) }
  \foreach \i in {\n,...,2}{ |- (row-\i-|col-\i) }
  -- (row-1-|col-2)
  -- cycle
  ;
}
\newcommand{\hlUpper}[1][]{
  \tikz
  \pgfmathtruncatemacro{\m}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\n}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\k}{min(\m, \n)}
  \draw[btrmt-hl, #1]
  (row-1-|col-1)
  \foreach \i in {2,...,\k}{ |- (row-\i-|col-\i) }
  -- (row-\k-|col-\n)
  -- (row-1-|col-\n)
  -- cycle
  ;
}
\newcommand{\hlLower}[1][]{
  \tikz
  \pgfmathtruncatemacro{\m}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\n}{\arabic{jCol}+1}
  \pgfmathtruncatemacro{\k}{min(\m, \n)}
  \draw[btrmt-hl, #1]
  (row-1-|col-1)
  \foreach \i in {2,...,\k}{ -| (row-\i-|col-\i) }
  -- (row-\m-|col-\k)
  -- (row-\m-|col-1)
  -- cycle
  ;
}
\newcommand{\hlPortion}[5][]{
  \tikz
  \pgfmathtruncatemacro{\nextRow}{#4+1}
  \pgfmathtruncatemacro{\nextCol}{#5+1}
  \draw[btrmt-hl, #1]
  (row-#2-|col-#3) rectangle (row-\nextRow-|col-\nextCol);
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% FINDING IMPORTANT LOCATIONS %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\markBorder}{%
  \pgfmathtruncatemacro{\nRows}{\arabic{iRow}}
  \pgfmathtruncatemacro{\nCols}{\arabic{jCol}}
  \pgfmathtruncatemacro{\lastRow}{\arabic{iRow}+1}
  \pgfmathtruncatemacro{\lastCol}{\arabic{jCol}+1}
  \foreach \i in {1,...,\nCols} {%
    \pgfmathtruncatemacro{\colLeft}{\i}
    \pgfmathtruncatemacro{\colRight}{\i+1}
    \coordinate (topCol\i) at ($ (row-1-|col-\colLeft)!0.5!(row-1-|col-\colRight) $);
    \coordinate (bottomCol\i) at ($ (row-\lastRow-|col-\colLeft)!0.5!(row-\lastRow-|col-\colRight) $);
  }
  \foreach \i in {1,...,\nRows} {%
    \pgfmathtruncatemacro{\rowTop}{\i}
    \pgfmathtruncatemacro{\rowBottom}{\i+1}
    \coordinate (leftRow\i) at ($ (row-\rowTop-|col-1)!0.5!(row-\rowBottom-|col-1) $);
    \coordinate (rightRow\i) at ($ (row-\rowTop-|col-\lastCol)!0.5!(row-\rowBottom-|col-\lastCol) $);
  }
  \coordinate (matNorth) at ($ (row-1-|col-1)!0.5!(row-1-|col-\lastCol) $);
  \coordinate (matSouth) at ($ (row-\lastRow-|col-1)!0.5!(row-\lastRow-|col-\lastCol) $);
  \coordinate (matEast) at ($ (row-1-|col-1)!0.5!(row-\lastRow-|col-1) $);
  \coordinate (matWest) at ($ (row-1-|col-\lastCol)!0.5!(row-\lastRow-|col-\lastCol) $);
}

%%%%%%%%%%%%%%%%
% BLOCK MATRIX %
%%%%%%%%%%%%%%%%

% Define keys with default values
\pgfkeys{
  , /btrmtblk/.is family
  , /btrmtblk
  , height/.estore in = \btrmtBlockHeight
  , height/.default = 1.25em
  , width/.estore in = \btrmtBlockWidth
  , width/.default = 3em
}
\newcommand{\btrmtBlock}[2][]{
  \pgfkeys{/btrmtblk, height, width, #1} % Set user-provided keys with defaults
  \begin{bNiceMatrix}[
    , cell-space-top-limit=\btrmtBlockHeight
    , cell-space-bottom-limit=\btrmtBlockHeight
    , columns-width=\btrmtBlockWidth
    ]
    #2
  \end{bNiceMatrix}
}

% Symbolic labeled columns.
% Define keys with default values
\newcommand{\vPhantomSpace}[1]{\vphantom{\rule{0pt}{#1}}}
\newcommand{\hPhantomSpace}[1]{\hphantom{\rule{#1}{0pt}}}
\pgfkeys{
  , /btrmtcols/.is family
  , /btrmtcols
  , default/.style = {cols={}, pad=2em, inner shorten=0.125em, outer shorten=0.5em, nolines={}, btrmtcols=}
  , cols/.store in = \btrmtColsList
  , pad/.store in = \btrmtColsPad
  , inner shorten/.store in = \btrmtInnerShorten
  , outer shorten/.store in = \btrmtOuterShorten
  , nolines/.store in = \btrmtColsNoLines
  , btrmtcols/.store in = \btrmtCols
}
\newcommand{\btrmtColsAppendFirst}[1]{\gappto{\btrmtCols}{#1}}
\newcommand{\btrmtColsAppendRest}[1]{\gappto{\btrmtCols}{ & #1}}
\newcommand{\btrmtCol}[1][]{
  \pgfkeys{/btrmtcols, default, #1}
  \renewcommand{\btrmtCols}{} % Reset \btrmtCols before use
  \foreach \x [count=\xi] in \btrmtColsList{
    \edef\temp{
      \noexpand\ifnum\xi=1
        \noexpand\expandafter\noexpand\btrmtColsAppendFirst{\unexpanded\expandafter{\x}}
        \noexpand\else
        \noexpand\expandafter\noexpand\btrmtColsAppendRest{\unexpanded\expandafter{\x}}
        \noexpand\fi
    }\temp
  }
  % \texttt{\detokenize\expandafter{\btrmtCols}}
  \begin{bNiceMatrix}[
    , code-after = {
      \begin{tikzpicture}[
        , thick
        , line cap=round
        , shorten >=\btrmtInnerShorten
        , shorten <=\btrmtOuterShorten
        ]
        \pgfmathtruncatemacro{\nCols}{\arabic{jCol}}
        \foreach \Coli in {1,...,\nCols}{
          \pgfmathsetmacro{\blacklist}{0}
          \foreach \y in \btrmtColsNoLines{
            \pgfmathparse{int(\Coli==\y ? 1 : \blacklist)}
            \xdef\blacklist{\pgfmathresult}
          }
          \ifnum\blacklist=0
            \pgfmathtruncatemacro{\Colj}{\Coli+1}
            \draw
            ($ (row-1-|col-\Coli)!0.5!(row-1-|\Colj) $)
            --
            ($ (row-2-|col-\Coli)!0.5!(row-2-|\Colj) $);
            \draw
            ($ (row-4-|col-\Coli)!0.5!(row-4-|\Colj) $)
            --
            ($ (row-3-|col-\Coli)!0.5!(row-3-|\Colj) $);
          \fi
        }
      \end{tikzpicture}
    }
    ]
    \vPhantomSpace{\btrmtColsPad}\\
    \btrmtCols \\
    \vPhantomSpace{\btrmtColsPad}\\
  \end{bNiceMatrix}
}

\endinput
