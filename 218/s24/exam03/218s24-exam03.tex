\documentclass[12pt]{exm}

\usepackage{btrmt}

\renewcommand{\examtitle}{Exam III}
\renewcommand{\examtime}{50-minute}
\renewcommand{\examdate}{\DTMdisplaydate{2024}{4}{19}{-1}}

\begin{document}
\begin{questions}


  %%%%%%%%%%%%
  % PAGE ONE %
  %%%%%%%%%%%%

  \begin{sagesilent}
    A = matrix(ZZ, [[6, 21, -26], [5, -20, 20], [5, -24, 24]])
    var('t')
    charpoly = A.characteristic_polynomial(t)
  \end{sagesilent}

  \newcommand{\EigenVector}{
    \begin{bNiceMatrix}
      5\\ 3-4\,i\\ 3-4\,i
    \end{bNiceMatrix}
  }

  \newcommand{\AEigenVector}{
    \begin{bNiceMatrix}
      15+20\,i\\ 25\\ 25
    \end{bNiceMatrix}
  }

  \question The data below depicts the result of multiplying a real $3\times 3$
  matrix $A$ by one of its eigenvectors $\bv$ as well as the unfactored
  characteric polynomial of $A$.
  \begin{align*}
    \overset{A}{\sage{A}}\overset{\bv}{\EigenVector} &= \AEigenVector & \chi_A(t) &= c_3\,t^3+c_2\,t^2+49\,t-100
  \end{align*}
  Note that the coefficients of $t^3$ and $t^2$ in $\chi_A(t)$ are labeled as
  $c_3$ and $c_2$.

  \vspace{2em}

  \begin{parts}
    \part[5] $c_3=\fillin[$1$][0.5in]$ and $c_2=\fillin[$-10$][0.5in]$

    \vspace{2em}

    \part[4] The eigenvector $\bv$ of $A$ corresponds to the eigenvalue
    $\lambda=\fillin[$3+4\,i$]$ (note that this $\lambda$ must be nonreal
    because $A$ is real and $\bv$ is nonreal).

    \vspace{\stretch{1}}

    \part[4] The value of $\lambda$ from part (\emph{b}) of this problem only
    satisfies one of the following properties. Select this property.

    \begin{checkboxes}

      \vspace{1em}

      \choice $\gm_A(\lambda)=3$

      \vspace{1em}

      \choice $\am_A(\lambda)=3$

      \vspace{1em}

      \correctchoice $\chi_A\left(\overline{\lambda}\right)=0$

      \vspace{1em}

      \choice $\lambda\cdot I_3-A$ is invertible

      \vspace{1em}

      \choice $\chi_A(\lambda)=\trace(A)$
    \end{checkboxes}

    \vspace{\stretch{1}}

    \part[5] Only one of the following statements correctly explains why there
    is no invertible $X$ and no real-symmetric matrix $S$ satisfying the
    equation $A=XSX^{-1}$. Select this statement.

    \vspace{1em}

    \begin{checkboxes}

      \choice $A\neq XSX^{-1}$ because $A$ is not Hermitian.

      \vspace{1em}

      \choice $A\neq XSX^{-1}$ because $A$ is not diagonalizable.

      \vspace{1em}

      \choice $A\neq XSX^{-1}$ because $A$ is not real-symmetric.

      \vspace{1em}

      \choice $A\neq XSX^{-1}$ because $A$ has no real eigenvalues.

      \vspace{1em}
      
      \correctchoice $A\neq XSX^{-1}$ because $A$ has a nonreal eigenvalue.

    \end{checkboxes}
  \end{parts}

  \vspace{\stretch{1}}


  \skippage

  %%%%%%%%%%%%
  % PAGE TWO %
  %%%%%%%%%%%%

  \begin{sagesilent}
    X = matrix(ZZ, [[1, 2, 0, -1], [1, 0, -1, -2], [2, 1, -2, 1], [-1, -1, 0, -1]])
    D = diagonal_matrix([2, 2, 2, -2])
    adjX = X.adjugate()
    A = X*D*X.inverse()
    var('t')
    chi = factor(A.characteristic_polynomial(t))
    var('ast')
    adjXstar = adjX.change_ring(SR)
    adjXstar[0,2] = adjXstar[1,3] = adjXstar[2,1] = ast
  \end{sagesilent}

  \question The following equation is a digaonaliztion of a $4\times 4$ matrix
  $A$.
  \[
    \btrmtBlock[height=2em]{A}
    =
    \overset{X}{\sage{X}}
    \overset{D}{\sage{D}}
    \frac{1}{c}
    \overset{\adj(X)}{\sage{adjXstar}}
  \]
  Note that the last matrix in this factorization is
  $X^{-1}=\dfrac{1}{c}\adj(X)$ where $c$ is a scalar quantity. Also note that
  several entries in $\adj(X)$ are missing and marked as $\ast$.

  \vspace{2em}

  \begin{parts}
    \part[6] $\rank(A)=\fillin[$\sage{A.rank()}$][0.5in]$, $\trace(A)=\fillin[$\sage{A.trace()}$][0.5in]$, and $\det(A)=\fillin[$\sage{A.det()}$][0.5in]$
    \begin{solution}[\stretch{1}]
      Here, $\rank(A)=\rank(D)=\sage{A.rank()}$, $\trace(A)=\trace(D)=\sage{A.trace()}$, and $\det(A)=\det(D)=\sage{A.det()}$
    \end{solution}

    \part[8] $\chi_A(0)=\fillin[$\sage{expand(chi(t=0))}$][0.5in]$ and $\chi_A(3)=\fillin[$\sage{expand(chi(t=3))}$][0.5in]$
    \begin{solution}[\stretch{1}]
      The diagonalization tells us that $\chi_A(t)=(t-2)^3(t+2)$, so
      $\chi_A(0)=\sage{chi(t=0)}$ and $\chi_A(3)=\sage{chi(t=3)}$. Of course, we
      also know $\chi_A(0)=(-1)^4\det(A)=-16$.
    \end{solution}

    \part[4] Every value of $x$ such that $(x\cdot I_4-A)^{-1}$ does \emph{not}
    exist satisfies $\abs{x}=\fillin[2][0.5in]$.
    \begin{solution}[\stretch{1}]
      The nonexistance of $(x\cdot I_4-A)^{-1}$ is the same as $x\cdot I_4-A$
      being \emph{singular}, which is the same as $x$ being an eigenvalue of
      $A$. The diagonalization tells us that $A$ only has two eigenvalues $2$
      and $-2$, so $\abs{x}=2$.
    \end{solution}

    \part[8] Find the correct value of $c$. Clearly explain your reasoning to
    receive credit.
    \begin{solution}[\stretch{4}]
      The adjugate formula for inverses tells us that
      $X^{-1}=\dfrac{1}{c}\adj{X}$ where $c=\det(X)$. So, our job is to
      calculate the determinant of $X$. The simplest way to do this is to take
      advantage of the fact that $X\adj(X)=\det(X)\cdot I_4$. The $(1,1)$ entry
      of $X\adj(X)$ is our desired value of $c$, which is calculated with the inner product
      \[
        c
        = \inner{\sage{X[0]}, \sage{adjX.T[0]}}
        = \sage{X.det()}
      \]
    \end{solution}

    \part[8] Is $A$ a real symmetric matrix? Clearly explain why or why not
    using ideas from class.
    \begin{solution}[\stretch{4}]
      The diagonalization tells us that $A$ has two eigenspaces given by
      \begin{align*}
        \mathcal{E}_A(2) &= \Span\Set*{\sage{X.T[0].column()}, \sage{X.T[1].column()}, \sage{X.T[2].column()}} & \mathcal{E}_A(-2) &= \Span\Set*{\sage{X.T[3].column()}}
      \end{align*}
      The Spectral Theorem tells us that the question of whether or not $A$ is
      real-symmetric is identical to the question of whether or not
      $\mathcal{E}_A(2)\perp\mathcal{E}_A(-2)$, which is resolved by three inner
      products
      \begin{align*}
        \inner{\sage{X.T[-1]}, \sage{X.T[0]}} &= 0 \\
        \inner{\sage{X.T[-1]}, \sage{X.T[1]}} &= 0 \\
        \inner{\sage{X.T[-1]}, \sage{X.T[2]}} &= 0
      \end{align*}
      The basis vector of $\mathcal{E}_A(-2)$ is indeed orthogonal to all of the
      basis vectors of $\mathcal{E}_A(2)$. This means that $A$ is indeed
      real-symmetric!
    \end{solution}

  \end{parts}


  \skippage

  %%%%%%%%%%%%%%
  % PAGE THREE %
  %%%%%%%%%%%%%%


  \begin{sagesilent}
    A = matrix(ZZ, [[7, 6], [-3, -2]])
    l2, l1 = sorted(A.eigenvalues())
    chi = lambda x: x*identity_matrix(A.nrows())-A
    X = matrix.column([(-2, 1), (1, -1)])
    D = diagonal_matrix([l1, l2])
    var('a b t')
    u0 = matrix.column([a, b])
    u0 = matrix.column([-3, 2])
    t0 = ln(2)
  \end{sagesilent}
  \question[16] Let $\bu(t)$ be the solution to the initial value problem
  $\bu^\prime=A\bu$ with $\bu(0)=\bu_0$ where
  \begin{align*}
    A &= \sage{A} & \bu_0 &= \sage{u0}
  \end{align*}
  Calculate $\bu(\ln(2))$. Simplify your answer to a vector of integers.

  \emph{Hint.} The algebraic identities $a\ln(b)=\ln(b^a)$ and $e^{\ln(a)}=a$
  will be useful here.
  \begin{solution}[\stretch{1}]
    We need to diagonalize $A$. The characteristic polynomial is
    \[
      \chi_A(t)
      = t^2-\trace(A)t+\det(A)
      = \sage{A.characteristic_polynomial(t)}
      = (t-\sage{l1})(t-\sage{l2})
    \]
    This gives us two eigenvalues $\EVals(A)=\Set{\sage{l1}, \sage{l2}}$. The eigenspaces are
    \begin{align*}
      \mathcal{E}_A(\sage{l1}) &= \Null\overset{\sage{l1}\cdot I_2-A}{\sage{chi(l1)}} = \Span\Set*{\sage{X.T[0].column()}} & \mathcal{E}_A(\sage{l2}) &= \Null\overset{\sage{l2}\cdot I_2-A}{\sage{chi(l2)}} = \Span\Set*{\sage{X.T[1].column()}}
    \end{align*}
    This gives the diagonalization
    \[
      \overset{A}{\sage{A}}
      =
      \overset{X}{\sage{X}}
      \overset{D}{\sage{D}}
      \overset{X^{-1}}{\sage{X.inverse()}}
    \]
    The vector $\bu(t)$ is then
    \[
      \bu(t)
      =
      \overset{X}{\sage{X}}
      \overset{\exp(Dt)}{\nvc{e^{4\,t} 0; 0 e^t}}
      \overset{X^{-1}}{\sage{X.inverse()}}
      \overset{\bu_0}{\sage{u0}}
    \]
    It follows that
    \begin{align*}
      \bu(\ln(t)) &= \overset{X}{\sage{X}}\overset{\exp(Dt)}{\nvc{e^{4\,\ln(2)} 0; 0 e^{\ln(2)}}}\overset{X^{-1}}{\sage{X.inverse()}}\overset{\bu_0}{\sage{u0}} \\
                  &= \sage{X}\nvc{2^4 0; 0 2}\sage{X.inverse()*u0} \\
                  &= \sage{X}\nvc{2^4; -2} \\
                  &= \nvc{-2^5-2; 2^4+2} \\
                  &= \nvc{-34; 18}
    \end{align*}
  \end{solution}






  \skippage

  %%%%%%%%%%%%%
  % PAGE FOUR %
  %%%%%%%%%%%%%

  \newcommand{\myA}{
    \begin{bNiceMatrix}[r]
      -6 & 6 & 6 & -6 \\
      -1 & -3 & -3 & -5 \\
      -2 & 0 & 4 & -2 \\
      -2 & -6 & -4 & 0 \\
      -6 & 0 & -2 & -4
    \end{bNiceMatrix}
  }
  \newcommand{\myU}{
    \begin{bNiceMatrix}[r]
      \sfrac{\sqrt{3}}{2} & 0 & 0 & 0 \\
      0 & \sfrac{\sqrt{3}}{3} & \sfrac{\sqrt{3}}{3} & -\sfrac{\sqrt{3}}{3} \\
      \sfrac{\sqrt{3}}{6} & 0 & \sfrac{\sqrt{3}}{3} & \sfrac{\sqrt{3}}{3} \\
      -\sfrac{\sqrt{3}}{6} & \sfrac{\sqrt{3}}{3} & 0 & \sfrac{\sqrt{3}}{3} \\
      \sfrac{\sqrt{3}}{6} & \sfrac{\sqrt{3}}{3} & -\sfrac{\sqrt{3}}{3} & 0
    \end{bNiceMatrix}
  }
  \newcommand{\mySigma}{
    \begin{bNiceMatrix}[r]
      8\,\sqrt{3} & & & \\
      & 6\,\sqrt{3} & & \\
      & & 2\,\sqrt{3} & \\
      & & & 2\,\sqrt{3}
    \end{bNiceMatrix}
  }
  \newcommand{\myVT}{
    \begin{bNiceMatrix}[r]
      -\sfrac{1}{2} & \sfrac{1}{2} & \sfrac{1}{2} & -\sfrac{1}{2} \\
      -\sfrac{1}{2} & -\sfrac{1}{2} & -\sfrac{1}{2} & -\sfrac{1}{2} \\
      \sfrac{1}{2} & -\sfrac{1}{2} & \sfrac{1}{2} & -\sfrac{1}{2} \\
      -\sfrac{1}{2} & -\sfrac{1}{2} & \sfrac{1}{2} & \sfrac{1}{2}
    \end{bNiceMatrix}
  }
  \question Consider the following singular value decomposition
  $A=U\Sigma V^\intercal$ (note that $A$ is $5\times 4$).
  \[
    \overset{A}{\myA}
    =
    \overset{U}{\myU}
    \overset{\Sigma}{\mySigma}
    \overset{V^\intercal}{\myVT}
  \]
  Throughout this problem, let $S=A^\intercal A$ and let $q(\bx)$ be the
  quadratic form $q(\bx)=\inner{\bx, S\bx}$.

  \vspace{2em}

  \begin{parts}
    \part[6] $\rank(A)=\fillin[4][0.5in]$ and $\rank(S)=\fillin[4][0.5in]$
    \vspace{1em}
    \part[6] Which categories of definiteness apply to $S$? Select \textbf{all}
    that apply.

    \vspace{1em}

    \begin{oneparcheckboxes}
      \correctchoice positive definite
      \correctchoice positive semidefinite
      \choice negative definite
      \choice negative semidefinite
      \choice indefinite
    \end{oneparcheckboxes}

    \vspace{1em}
    \part[4] The smallest real number $x$ such that $\det(x\cdot I_4-S)=0$ is
    $x=\fillin[$12$][0.5in]$.
    \begin{solution}[\stretch{1}]
      This is the same thing as asking for the smallest eigenvalue of
      $S=A^\intercal A$, which is the same thing as the square of the smallest
      \emph{singular value} of $A$, which is
      $\sigma_4^2=(2\,\sqrt{3})^2=12$.
    \end{solution}
    \part[8] Let $A^+=V\Sigma^{-1} U^\intercal$. Calculate $AA^+A$. Your answer will
    be a $5\times 4$ matrix.
    \begin{solution}[\stretch{4}]
      Note that
      \[
        AA^+A
        = U\Sigma V^\intercal V\Sigma^{-1} U^\intercal U\Sigma V^\intercal
        = U\Sigma I_4\Sigma^{-1} I_4 \Sigma V^\intercal
        = U\Sigma\Sigma^{-1}\Sigma V^\intercal
        = U\Sigma V^\intercal
        = A
        = \myA
      \]
    \end{solution}
    \part[8] Let $\bv$ be any vector satisfying $V^\intercal\bv=\nv{0 0 1
      1}$. Calculate the value of $q(\bv)$. Simplify your answer to an integer
    value.
    \begin{solution}[\stretch{4}]
      We know from class that $S=A^\intercal A=V\Sigma^2 V^\intercal$ is a
      spectral factorization. Of course, we could also derive this fact from
      scratch with
      \[
        A^\intercal A
        = (U\Sigma V^\intercal)^\intercal(U\Sigma V^\intercal)
        = V\Sigma U^\intercal U\Sigma V^\intercal
        = V\Sigma^2 V^\intercal
      \]
      The technique of ``completing the square'' from class then simplifies the
      quadratic form to
      \[
        q(\bx)
        = \lambda_1\cdot y_1^2+\lambda_2\cdot y_2^2+\lambda_3\cdot y_3^2+\lambda_4\cdot y_4^2
        = \sigma^2_1\cdot y_1^2+\sigma^2_2\cdot y_2^2+\sigma^2_3\cdot y_3^2+\sigma^2_4\cdot y_4^2
      \]
      where the $y$'s are the coordinates of $V^\intercal\bx$. Our desired
      calculation is then
      \[
        q(\bv)
        = \sigma^2_1\cdot 0^2+\sigma^2_2\cdot 0^2+\sigma^2_3\cdot 1^2+\sigma^2_4\cdot 1^2
        = \sigma_3^2+\sigma_4^2
        = \left(2\,\sqrt{3}\right)^2+\left(2\,\sqrt{3}\right)^2
        = 24
      \]
    \end{solution}

  \end{parts}

\end{questions}
\end{document}
