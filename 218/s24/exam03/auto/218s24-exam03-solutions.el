(TeX-add-style-hook
 "218s24-exam03-solutions"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("exm" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "exm"
    "exm12"
    "btrmt")
   (TeX-add-symbols
    "EigenVector"
    "AEigenVector"
    "myA"
    "myU"
    "mySigma"
    "myVT"))
 :latex)

