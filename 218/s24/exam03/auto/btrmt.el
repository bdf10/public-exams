(TeX-add-style-hook
 "btrmt"
 (lambda ()
   (TeX-run-style-hooks
    "nicematrix"
    "etoolbox"
    "pgffor"
    "xstring"
    "tikz")
   (TeX-add-symbols
    '("btrmtCol" ["argument"] 0)
    '("btrmtBlock" ["argument"] 1)
    '("hlPortion" ["argument"] 4)
    '("hlLower" ["argument"] 0)
    '("hlUpper" ["argument"] 0)
    '("hlDiag" ["argument"] 0)
    '("hlEntry" ["argument"] 2)
    '("hlMat" ["argument"] 0)
    '("hlCol" ["argument"] 1)
    '("hlRow" ["argument"] 1)
    '("btrmtColsAppendRest" 1)
    '("btrmtColsAppendFirst" 1)
    '("hPhantomSpace" 1)
    '("vPhantomSpace" 1)
    "markBorder"))
 :latex)

