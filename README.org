#+TITLE: public-exams

A repository of old midterm exams written by me.

* Math 111L

| Semester  | Exam     | Solutions |
|-----------+----------+-----------|
| Fall 2014 | [[file:111/111f14/111f14-exam01/111f14-exam01.pdf][Exam I]]   | [[file:111/111f14/111f14-exam01/111f14-exam01-solutions.pdf][pdf]]       |
| Fall 2014 | [[file:111/111f14/111f14-exam02/111f14-exam02.pdf][Exam II]]  | [[file:111/111f14/111f14-exam02/111f14-exam02-solutions.pdf][pdf]]       |
| Fall 2014 | [[file:111/111f14/111f14-exam03/111f14-exam03.pdf][Exam III]] | [[file:111/111f14/111f14-exam03/111f14-exam03-solutions.pdf][pdf]]       |

* Math 122L

| Semester  | Exam     | Solutions |
|-----------+----------+-----------|
| Fall 2011 | [[file:122/122f11/122f11-exam01/122f11-exam01.pdf][Exam I]]   | [[file:122/122f11/122f11-exam01/122f11-exam01-solutions.pdf][pdf]]       |
| Fall 2011 | [[file:122/122f11/122f11-exam02/122f11-exam02.pdf][Exam II]]  | [[file:122/122f11/122f11-exam02/122f11-exam02-solutions.pdf][pdf]]       |
| Fall 2011 | [[file:122/122f11/122f11-exam03/122f11-exam03.pdf][Exam III]] | [[file:122/122f11/122f11-exam03/122f11-exam03-solutions.pdf][pdf]]       |
| Fall 2012 | [[file:122/122f12/122f12-exam01/122f12-exam01.pdf][Exam I]]   | [[file:122/122f12/122f12-exam01/122f12-exam01-solutions.pdf][pdf]]       |
| Fall 2012 | [[file:122/122f12/122f12-exam02/122f12-exam02.pdf][Exam II]]  | [[file:122/122f12/122f12-exam02/122f12-exam02-solutions.pdf][pdf]]       |
| Fall 2012 | [[file:122/122f12/122f12-exam03/122f12-exam03.pdf][Exam III]] | [[file:122/122f12/122f12-exam03/122f12-exam03-solutions.pdf][pdf]]       |
| Fall 2013 | [[file:122/122f13/122f13-exam01/122f13-exam01.pdf][Exam I]]   | [[file:122/122f13/122f13-exam01/122f13-exam01-solutions.pdf][pdf]]       |
| Fall 2013 | [[file:122/122f13/122f13-exam02/122f13-exam02.pdf][Exam II]]  | [[file:122/122f13/122f13-exam02/122f13-exam02-solutions.pdf][pdf]]       |
| Fall 2013 | [[file:122/122f13/122f13-exam03/122f13-exam03.pdf][Exam III]] | [[file:122/122f13/122f13-exam03/122f13-exam03-solutions.pdf][pdf]]       |

* Math 202

| Semester    | Exam     | Solutions |
|-------------+----------+-----------|
| Spring 2017 | [[file:202/202s17/202s17-exam01/202s17-exam01.pdf][Exam I]]   | [[file:202/202s17/202s17-exam01/202s17-exam01-solutions.pdf][pdf]]       |
| Spring 2017 | [[file:202/202s17/202s17-exam02/202s17-exam02.pdf][Exam II]]  | [[file:202/202s17/202s17-exam02/202s17-exam02-solutions.pdf][pdf]]       |
| Spring 2017 | [[file:202/202s17/202s17-exam03/202s17-exam03.pdf][Exam III]] | [[file:202/202s17/202s17-exam03/202s17-exam03-solutions.pdf][pdf]]       |
| Fall 2017   | [[file:202/202f17/202f17-exam01/202f17-exam01.pdf][Exam I]]   | [[file:202/202f17/202f17-exam01/202f17-exam01-solutions.pdf][pdf]]       |
| Fall 2017   | [[file:202/202f17/202f17-exam02/202f17-exam02.pdf][Exam II]]  | [[file:202/202f17/202f17-exam02/202f17-exam02-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:202/202s18/202s18-exam01/202s18-exam01.pdf][Exam I]]   | [[file:202/202s18/202s18-exam01/202s18-exam01-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:202/202s18/202s18-exam02/202s18-exam02.pdf][Exam II]]  | [[file:202/202s18/202s18-exam02/202s18-exam02-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:202/202s18/202s18-exam03/202s18-exam03.pdf][Exam III]] | [[file:202/202s18/202s18-exam03/202s18-exam03-solutions.pdf][pdf]]       |
| Fall 2018   | [[file:202/202f18/202f18-exam01/202f18-exam01.pdf][Exam I]]   | [[file:202/202f18/202f18-exam01/202f18-exam01-solutions.pdf][pdf]]       |
| Fall 2018   | [[file:202/202f18/202f18-exam02/202f18-exam02.pdf][Exam II]]  | [[file:202/202f18/202f18-exam02/202f18-exam02-solutions.pdf][pdf]]       |
| Fall 2018   | [[file:202/202f18/202f18-exam03/202f18-exam03.pdf][Exam III]] | [[file:202/202f18/202f18-exam03/202f18-exam03-solutions.pdf][pdf]]       |

* Math 212

| Semester    | Exam     | Solutions |
|-------------+----------+-----------|
| Fall 2015   | [[file:212/212f15/212f15-exam01/212f15-exam01.pdf][Exam I]]   | [[file:212/212f15/212f15-exam01/212f15-exam01-solutions.pdf][pdf]]       |
| Fall 2015   | [[file:212/212f15/212f15-exam02/212f15-exam02.pdf][Exam II]]  | [[file:212/212f15/212f15-exam02/212f15-exam02-solutions.pdf][pdf]]       |
| Spring 2016 | [[file:212/212s16/212s16-exam01/212s16-exam01.pdf][Exam I]]   | [[file:212/212s16/212s16-exam01/212s16-exam01-solutions.pdf][pdf]]       |
| Spring 2016 | [[file:212/212s16/212s16-exam02/212s16-exam02.pdf][Exam II]]  | [[file:212/212s16/212s16-exam02/212s16-exam02-solutions.pdf][pdf]]       |
| Fall 2016   | [[file:212/212f16/212f16-exam01/212f16-exam01.pdf][Exam I]]   | [[file:212/212f16/212f16-exam01/212f16-exam01-solutions.pdf][pdf]]       |
| Fall 2016   | [[file:212/212f16/212f16-exam02/212f16-exam02.pdf][Exam II]]  | [[file:212/212f16/212f16-exam02/212f16-exam02-solutions.pdf][pdf]]       |
| Fall 2016   | [[file:212/212f16/212f16-exam03/212f16-exam03.pdf][Exam III]] | [[file:212/212f16/212f16-exam03/212f16-exam03-solutions.pdf][pdf]]       |
| Spring 2020 | [[file:212/212s20/212s20-exam01/212s20-exam01.pdf][Exam I]]   | [[file:212/212s20/212s20-exam01/212s20-exam01-solutions.pdf][pdf]] |
| Spring 2020 | [[file:212/212s20/212s20-exam02/212s20-exam02.pdf][Exam II]] | [[file:212/212s20/212s20-exam02/212s20-exam02-solutions.pdf][pdf]]       |

* Math 216

| Semester    | Exam     | Solutions |
|-------------+----------+-----------|
| Summer 2012 | [[file:216/216su12/216su12-exam01/216su12-exam01.pdf][Exam I]]   | [[file:216/216su12/216su12-exam01/216su12-exam01-solutions.pdf][pdf]]       |
| Summer 2012 | [[file:216/216su12/216su12-exam02/216su12-exam02.pdf][Exam II]]  | [[file:216/216su12/216su12-exam02/216su12-exam02-solutions.pdf][pdf]]       |
| Summer 2016 | [[file:216/216su16/216su16-exam01/216su16-exam01.pdf][Exam I]]   | [[file:216/216su16/216su16-exam01/216su16-exam01-solutions.pdf][pdf]]       |
| Summer 2016 | [[file:216/216su16/216su16-exam02/216su16-exam02.pdf][Exam II]]  | lost      |
| Summer 2016 | [[file:216/216su16/216su16-exam03/216su16-exam03.pdf][Exam III]] | lost      |
| Summer 2017 | [[file:216/216su17/216su17-exam01/216su17-exam01.pdf][Exam I]]   | [[file:216/216su17/216su17-exam01/216su17-exam01-solutions.pdf][pdf]]       |
| Summer 2017 | [[file:216/216su17/216su17-exam02/216su17-exam02.pdf][Exam II]]  | [[file:216/216su17/216su17-exam02/216su17-exam02-solutions.pdf][pdf]]       |
| Summer 2017 | [[file:216/216su17/216su17-exam03/216su17-exam03.pdf][Exam III]] | [[file:216/216su17/216su17-exam03/216su17-exam03-solutions.pdf][pdf]]       |

* Math 218

| Semester    | Exam     | Solutions |
|-------------+----------+-----------|
| Spring 2018 | [[file:218/218s18/218s18-exam01/218s18-exam01.pdf][Exam I]]   | [[file:218/218s18/218s18-exam01/218s18-exam01-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:218/218s18/218s18-exam02/218s18-exam02.pdf][Exam II]]  | [[file:218/218s18/218s18-exam02/218s18-exam02-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:218/218s18/218s18-exam03/218s18-exam03.pdf][Exam III]] | [[file:218/218s18/218s18-exam03/218s18-exam03-solutions.pdf][pdf]]       |
| Fall 2018   | [[file:218/218f18/218f18-exam01/218f18-exam01.pdf][Exam I]]   | [[file:218/218f18/218f18-exam01/218f18-exam01-solutions.pdf][pdf]]       |
| Fall 2018   | [[file:218/218f18/218f18-exam02/218f18-exam02.pdf][Exam II]]  | [[file:218/218f18/218f18-exam02/218f18-exam02-solutions.pdf][pdf]]       |
| Spring 2019 | [[file:218/218s19/218s19-exam01/218s19-exam01.pdf][Exam I]]   | [[file:218/218s19/218s19-exam01/218s19-exam01-solutions.pdf][pdf]]       |
| Spring 2019 | [[file:218/218s19/218s19-exam02/218s19-exam02.pdf][Exam II]]  | [[file:218/218s19/218s19-exam02/218s19-exam02-solutions.pdf][pdf]]       |
| Spring 2019 | [[file:218/218s19/218s19-exam03/218s19-exam03.pdf][Exam III]] | [[file:218/218s19/218s19-exam03/218s19-exam03-solutions.pdf][pdf]]       |
| Summer 2019 | [[file:218/218su19/218su19-exam01/218su19-exam01.pdf][Exam I]]   | [[file:218/218su19/218su19-exam01/218su19-exam01-solutions.pdf][pdf]]       |
| Summer 2019 | [[file:218/218su19/218su19-exam02/218su19-exam02.pdf][Exam II]]  | [[file:218/218su19/218su19-exam02/218su19-exam02-solutions.pdf][pdf]]       |
| Summer 2019 | [[file:218/218su19/218su19-exam03/218su19-exam03.pdf][Exam III]] | [[file:218/218su19/218su19-exam03/218su19-exam03-solutions.pdf][pdf]]       |
| Fall 2019   | [[file:218/218f19/218f19-exam01/218f19-exam01.pdf][Exam I]]   | [[file:218/218f19/218f19-exam01/218f19-exam01-solutions.pdf][pdf]]       |
| Fall 2019   | [[file:218/218f19/218f19-exam02/218f19-exam02.pdf][Exam II]]  | [[file:218/218f19/218f19-exam02/218f19-exam02-solutions.pdf][pdf]]       |
| Fall 2019   | [[file:218/218f19/218f19-exam03/218f19-exam03.pdf][Exam III]] | [[file:218/218f19/218f19-exam03/218f19-exam03-solutions.pdf][pdf]]       |
| Spring 2020 | [[file:218/218s20/218s20-exam01/218s20-exam01.pdf][Exam I]]   | [[file:218/218s20/218s20-exam01/218s20-exam01-solutions.pdf][pdf]]       |
| Spring 2020 | [[file:218/218s20/218s20-exam02/218s20-exam02.pdf][Exam II]]  | [[file:218/218s20/218s20-exam02/218s20-exam02-solutions.pdf][pdf]]       |

* Math 222
| Semester    | Exam    | Solutions |
|-------------+---------+-----------|
| Spring 2018 | [[file:222/222s18/222s18-exam01/222s18-exam01.pdf][Exam I]]  | [[file:222/222s18/222s18-exam01/222s18-exam01-solutions.pdf][pdf]]       |
| Spring 2018 | [[file:222/222s18/222s18-exam02/222s18-exam02.pdf][Exam II]] | [[file:222/222s18/222s18-exam02/222s18-exam02-solutions.pdf][pdf]]       |
